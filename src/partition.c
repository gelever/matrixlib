/* partition.c
 * Author: Stephan Gelever
 * Date: April, 2016
 *

 *
 */

#include <omp.h>
//#include<mtmetis.h>
#include "partition.h"

#define START_FLAGS 3

/*
 * Driver for aggregation
 *
 * Input:
 *  A - Matrix
 *  n - number in each aggregate
 *
 *  Flags:
 *	-v - Verbose output
 *	-p - Print the input matrix
 *	-f - Set input file format : entries (default), adj, adji, hb
 *		entries - list of pairs of matrix entries 
 *		adj - adjacency list 
 *		adji - adjacency list with matrix information in first two lines.
 *		       matrix size in first line, nnz in second line
 *		hb - Harwell Boeing format
 *	-o - Output file name
 *
 */

//int main(int argc, char *argv[]) {
int test_partition(int argc, char *argv[]) {
	Matrix * mat;	// Matrix to work with.
	//Matrix * aggs;	// Matrix to work with.
	//Matrix * p;	// Matrix to work with.

	int * options; // Holds input flags

	if (argc < START_FLAGS || argc > 11)
		show_help_partition();

	options = parse_cmd(argc, argv, START_FLAGS);

	mat = parse_file(argv[1], options[FORMAT]);

	if (options[PRINT]) {
		if (mat->nnz < 30) {
			pretty(mat);
		}

	}

	// REPLACE W/ Current restriciton function
	//p = create_restriction(mat, atoi(argv[2]), 1);
	//printf("PT:\n");
	//pretty(p);

	free(options);
	destroy_matrix(mat);
	exit(0);
}



/*
 * Partitions the matrix into nparts parts.
 * Input:
 *  mat - Matrix
 *  nparts - number of parts to partition into
 *
 * Output:
 *  int array of size nvtxs that holds the partition information.
 *
 */

int * partition(Matrix * mat, int nparts) 
{
	int * part;

	if (mat->nnz == 1) {
		part = malloc(sizeof(int));
		part[0] = 1;
		return part;
	}
	/*

	double * op = mtmetis_init_options();
	op[MTMETIS_OPTION_NTHREADS] = 4;
	mtmetis_vtx_type nvtxs = mat->rows;
	mtmetis_vtx_type ncon  = 1;

	mtmetis_wgt_type objval;

	part = malloc(sizeof(mtmetis_vtx_type) * nvtxs);
	int status;

	status = MTMETIS_PartGraphKway(&nvtxs,
			&ncon, mat->row_ptr, mat->col_index,
			NULL, NULL, NULL, &nparts, NULL,
			NULL, op, &objval, part);

	if (status != MTMETIS_SUCCESS) {
		printf("Partitioning failed!\n");
		exit(1);
	}



	*/



	idx_t options[METIS_NOPTIONS];
	//Force contigious partition
	METIS_SetDefaultOptions(options);
	options[METIS_OPTION_CONTIG] = 1;

	idx_t nvtxs = mat->rows;
	idx_t ncon  = 1;

	idx_t objval;
	part = malloc(sizeof(idx_t) * nvtxs);
	int status;

	status = METIS_PartGraphKway(&nvtxs,
			&ncon, mat->row_ptr, mat->col_index,
			NULL, NULL, NULL, &nparts, NULL,
			NULL, options, &objval, part);

	if (status != METIS_OK) {
		printf("Partitioning failed!\n");
		exit(1);
	}

	return part;
}



/*
 * Creates an agglomeration-edge relationship matrix
 * Input:
 *  aggs - integer array holding the partition
 *  num_aggs - number of agglomerations
 *  num_edges - number of edges
 *
 * Output:
 *  Matrix containing the agglomeration-edge relationship.
 *
 */
Matrix * make_agg_edge(int * aggs, int num_aggs, int num_edges) {
	//part, size, mat->rows
	
	int total = 0;
	int last = 0;
	int * agg_count = calloc(num_edges, sizeof(int));

	// Counts how many aggregates 
	for (int i = 0; i < num_edges; ++i) {
		agg_count[aggs[i]]++;
	}
	for (int i = 0; i < num_edges; ++i) {
		if(agg_count[i] > 0) {
			++total;
			last = i+1;
		}
	}
	printf("total: %d vs num_aggs: %d\n", total, num_aggs);

	int * count = calloc(num_edges, sizeof(int));
	int * rows = calloc(num_edges, sizeof(int));

	Matrix * result = create_matrix(total, num_edges, num_edges);

	for (int i = 0; i < result->nnz; ++i)
		result->values[i] = 1;

	result->row_ptr[0] = 0;
	result->row_ptr[result->rows] = result->nnz;

	if (total > 1) {
		int temp = 0;

		// Sets adjusted row pointers
		for (int i = 0; i < last; ++i) {
			if (agg_count[i] > 0 ) {
				result->row_ptr[temp+1] = result->row_ptr[temp] + agg_count[i];
				rows[i] = temp;
				++temp;
			}
		}

		// Sets the column values of each edge
		for (int i = 0; i < num_edges; ++i) { 
			int col = rows[aggs[i]];
			//int col = aggs[i];
			int k = result->row_ptr[col];
			int offset = count[col];

			result->col_index[k + offset] = i;
			count[col]++;
		}
	}
	else {
		for (int i = 0; i < num_edges; ++i)
			result->col_index[i] = i;

	}

	free(agg_count);
	free(count);
	free(rows);

	return result;
}

/*
 * Creates an edge-vertex relationship matrix
 * Input:
 *  mat - matrix to create from
 *
 * Output:
 *  Matrix containing the edge-vertex relationship.
 *
 */
Matrix * edge_vertex(Matrix * mat) {
	Matrix * result;
	int count= 0;
	int k = 0;

	result = create_matrix( mat->nnz/2, mat->rows, mat->nnz);

	result->row_ptr[0] = count;

	for (int i = 0; i < mat->rows; ++i) {
		k = mat->row_ptr[i];
		while(k < mat->row_ptr[i+1]) {
			if (mat->col_index[k] <= i) {
				++k;
				continue;
			}

			//
			// if/else split checks to make sure in numerical order.
			// TODO: MAKE THIS BETTER
			//
			if (i < mat->col_index[k]) {
				result->values[count] = 1;
				result->col_index[count] = i;
				count++;

				result->values[count] = 1;
				result->col_index[count] = mat->col_index[k];
				count++;

				result->row_ptr[count/2] = count;
			}
			else{

				result->values[count] = 1;
				result->col_index[count] = mat->col_index[k];
				count++;

				result->values[count] = 1;
				result->col_index[count] = i;
				count++;

				result->row_ptr[count/2] = count;

			}
			++k;

		}
	}

	return result;
}


Matrix * aggregate_edge(Matrix * mat, Matrix * agg_vert, int agg_size) {
	Matrix * adj = get_adj(mat);
	int size = mat->rows;
	size /= agg_size;

	int * part = partition(adj, size);
	Matrix * ev = edge_vertex(adj);
	Matrix * evt = transpose(ev);
	//Matrix * agg_edge = sparse_mult_sparse(agg_vert, evt);
	Matrix * agg_edge = sparse_mult_sparse(agg_vert, evt);

	free(part);
	free(agg_vert);
	free(ev);
	free(evt);
	free(adj);

	return agg_edge;
}

/*
 * Creates a restriction matrix
 *  - Agglomerates the input matrix
 *  - Solves the local eigenproblems on each agglomerate
 *  - Aggregate the dofs
 *  - SVD each aggregate
 *
 *  Input: 
 *   mat - input matrix
 *   agg_size - size of each agglomerate
 *   num_eig - number of eigenvectors per agglomerate
 */

Matrix * create_restriction_agg_edge(Matrix * mat,  int agg_size, double theta) {

	int * part;
	int size = mat->rows/agg_size;
	//size /= agg_size;
	if (size <= 1)
		return NULL;
	//part = calloc(mat->rows, sizeof(int));
	struct timeval start_time, end_time;
	struct timeval s, e;
	gettimeofday(&start_time, NULL);


	gettimeofday(&s, NULL);
	Matrix * adj = get_adj(mat);
	part = partition(adj, size);
	gettimeofday(&e, NULL);

	double total_time = (e.tv_sec - s.tv_sec) + 
		(e.tv_usec -s.tv_usec)/1000000.0f;
	//printf("Partition %d in:\t%g seconds\n", mat->rows, total_time);


	gettimeofday(&s, NULL);
	Matrix * agg_vert = make_agg_edge(part, size, mat->rows);
	//Matrix * expanded = sparse_mult_sparse(agg_vert, adj);
	Matrix * pattern = get_pattern_bool(mat);
	Matrix * expanded = sparse_mult_sparse(agg_vert, pattern);

	int * row_count = calloc(agg_vert->rows, sizeof(int));
	Matrix * P = create_p_agg_edge_parallel(mat, expanded, theta, row_count);

	gettimeofday(&e, NULL);

	total_time = (e.tv_sec - s.tv_sec) + 
		(e.tv_usec -s.tv_usec)/1000000.0f;
	//printf("P %d in:\t%g seconds\n", mat->rows, total_time);

	gettimeofday(&s, NULL);
	aggregate_with_op(P, agg_vert, row_count);
	svd_agg(P, agg_vert->rows, row_count);

	gettimeofday(&e, NULL);

	total_time = (e.tv_sec - s.tv_sec) + 
		(e.tv_usec -s.tv_usec)/1000000.0f;
	//printf("aggregate/svd %d in:\t%g seconds\n", mat->rows, total_time);

	free(row_count);
	free(part);

	destroy_matrix(agg_vert);
	destroy_matrix(pattern);
	destroy_matrix(expanded);
	destroy_matrix(adj);

	gettimeofday(&end_time, NULL);
	total_time = (end_time.tv_sec - start_time.tv_sec) + 
		(end_time.tv_usec - start_time.tv_usec)/1000000.0f;
	//printf("Setup size %d in:\t%g seconds\n", P->rows, total_time);


	return P;
}

/*
 * SVDs all aggregates so that they are linearly independent
 * after aggregation
 *
 * Input:
 *  P - Interpolation matrix
 *  rows - number of aggregates
 *  row_count - array holding how many eigenvectors in each aggregate
 *
 */
void svd_agg(Matrix * P, int num_aggs, int * row_count) {
	int count = 0;
	int current_row = 0;
	for (int i = 0; i < num_aggs; ++i) {
		int size = P->row_ptr[current_row+1] - P->row_ptr[current_row];

		int n = size, m = row_count[i], lda = n, ldu, ldvt = n, info;
		ldu = m;

		// Modified from Intel Example:
		int min = n < m ? n : m;
		double * superb = calloc(min-1, sizeof(double));
		double * s = calloc(n, sizeof(double));
		double * u = calloc(ldu*m, sizeof(double));
		double * vt = calloc(ldvt*n, sizeof(double));

		/* Compute SVD */
		info = LAPACKE_dgesvd( LAPACK_ROW_MAJOR, 'A', 'A', m, n, &P->values[count], lda,
				s, u, ldu, vt, ldvt, superb );

		/* Check for convergence */
		if( info > 0 ) {
			printf( "The algorithm computing SVD failed to converge.\n" );
			exit( 1 );
		}

		memmove(&P->values[count], vt, sizeof(double)*n*m);
		count += size*row_count[i];
		current_row += row_count[i];
		free(superb);
		free(s);
		free(u);
		free(vt);
	}
}
/* Creates P from matrix and agglomerates.
 *  - Initially copies sparsity pattern of agglomerates
 *  - For each agglomerate:
 *	- Create and solve local eigenproblem
 *	- Set the values of each local P to be all eigenvectors within theta of the largest eigenvector
 *  Input:
 *   A - original matrix w/ values
 *   aggs - agglomerates-dof relationship
 *   theta - spectral tolerance
 *
 * Return:
 *  P - Interpolation Matrix
 *  row_count - number of eigenvectors in each agglomerate
 *
 *  TODO: Convert this to use tolerance instead of static number of eigenvectors.
 *
 */

Matrix * create_p_agg_edge(Matrix * A, Matrix * aggs, double theta, int * row_count) {

	int scale_for_now = 100;
	//Matrix * result = create_matrix(aggs->rows,
	//aggs->cols, aggs->nnz);
	Matrix * result = create_matrix(aggs->rows * scale_for_now,
			aggs->cols, aggs->nnz * scale_for_now);
	double * coarse;

	int size;
	int count = 0;
	int rows = 0;
	double * d_inv;

	for (int i = 0; i < aggs->rows; ++i) {
		size = aggs->row_ptr[i + 1] - aggs->row_ptr[i];
		int info_c;
		if (size <= 1) {
			printf("Size 0 agglomerate!\n");
			continue;

		}

		double * solution_c = calloc(size, sizeof(double));

		coarse = coarse_dense(A,
				&aggs->col_index[aggs->row_ptr[i]], size); 
		d_inv = malloc(size * sizeof(double));

		//printf("Coarse before scale\n");
		//print_dense(coarse, size,size);

		for (int j = 0; j < size; ++j)
			d_inv[j] = 1/sqrt(coarse[j*size+j]);

		double * d = calloc(size*size, sizeof(double));

		for (int j = 0; j < size; ++j)
			d[j*size+j] = coarse[j*size+j];
		info_c = LAPACKE_dpotrf(LAPACK_ROW_MAJOR, 'U', size,
				d, size);

		if (info_c > 0)  {
			printf("cantcholfactor\n");

		}
		info_c = LAPACKE_dsygst(LAPACK_ROW_MAJOR, 1, 'U', size,
				coarse, size, d, size);
		if (info_c > 0) 
			printf("cantgeenraliz\n");


		//d_inv[j] = 1;
		/*

		   for(int j = 0; j < size; ++j) {
		   for (int k = 0; k < size; ++k) {
		   coarse[j*size +k] *= d_inv[k]*d_inv[j];
		   }
		   }

		   for (int j = 0; j < size; ++j) {
		   for (int k = 0; k<size; ++k) {
		   coarse[k*size+j] *= d_inv[j];
		   }
		   }
		   for (int j = 0; j < size; ++j) {
		   for (int k = 0; k<size; ++k) {
		   coarse[j*size+k] *= d_inv[j];
		   }
		   }


*/

		//printf("Coarse after scale %d size %d\n", i, size);
		//print_dense(coarse, size,size);

		info_c = LAPACKE_dsyev( LAPACK_ROW_MAJOR, 'V', 'U', 
				size, coarse, size, solution_c );
		if( info_c > 0 ) {
			printf( "The algorithm failed to compute eigenvalues.\n" );
			exit( 1 );
		}

		//printf("Eigenvectors at %d size %d\n", i, size);
		//print_dense(coarse, size,size);
		row_count[i] = 1;
		int j = size -2;
		while(j > 0 && solution_c[size-1] - solution_c[j] < theta) {
			row_count[i]++;
			j--;
#ifdef DEBUG
			if (solution_c[j] <= 0){
				printf("Negative eigenvalue!\n");
				exit(0);
			}
#endif

		}

		for(int j = 1; j <= row_count[i]; ++j) {
			result->row_ptr[rows+j] = result->row_ptr[rows] + (j)*size;
		}

		for (int j = 0; j < row_count[i]; ++j) {
			int l = 0;
			int m = aggs->row_ptr[i];
			int k = result->row_ptr[rows+j];
			while (k < result->row_ptr[rows+j+1]) {
				result->values[k] = d_inv[l] * coarse[l*size+size-1-j];
				result->col_index[k] = aggs->col_index[m+l];
				//printf("%g\n", result->values[k]);
				++k;
				++l;
			}
			//printf("\n");
		}
		rows += row_count[i];
		count += size*row_count[i];

		free(solution_c);
		free(coarse);
		free(d_inv);
		free(d);
	}
	result->rows = rows;
	result->nnz = count;
	return result;
}

/* Creates PT from matrix and agglomerates.
 *  - For each agglomerate:
 *	- Create and solve local eigenproblem
 *	- Set the values of each local PT to be all eigenvectors within theta of the largest eigenvector
 *
 *  Input:
 *   A - original matrix w/ values
 *   aggs - agglomerates-dof relationship
 *   theta - spectral tolerance
 *
 * Return:
 *  PT - Restrction Matrix
 *  row_count - number of eigenvectors in each agglomerate
 */

Matrix * create_p_agg_edge_parallel(Matrix * A, Matrix * aggs, double theta, int * row_count) {
	int total = 0;
	int rows = 0;
	double ** vectors = calloc(aggs->rows, sizeof(double*));

	int max_threads = omp_get_max_threads();

#ifdef DEBUG
	max_threads = 1;
#endif

#pragma omp parallel for schedule(auto) num_threads(max_threads)
	for (int i = 0; i < aggs->rows; ++i) {
		int size = aggs->row_ptr[i + 1] - aggs->row_ptr[i];
		int info_c;

		if (size < 1) {
			printf("Size 0 agglomerate!\n");
			continue;
		} 
		double * solution_c = calloc(size, sizeof(double));

		double * coarse = coarse_dense(A,
				&aggs->col_index[aggs->row_ptr[i]], size); 
		double * d_inv = malloc(size * sizeof(double));

		for (int j = 0; j < size; ++j)
			d_inv[j] = 1/sqrt(coarse[j*size+j]);

		double * d = calloc(size*size, sizeof(double));

		for (int j = 0; j < size; ++j)
			d[j*size+j] = coarse[j*size+j];
		info_c = LAPACKE_dpotrf(LAPACK_ROW_MAJOR, 'U', size,
				d, size);

		if (info_c > 0)  {
			printf("cantcholfactor\n");
			/*
			   double * d2 = calloc(size*size, sizeof(double));
			   for (int j = 0; j < size; ++j)
			   d2[j*size+j] = coarse[j*size+j];
			   for (int j = 0; j < size; ++j)
			   printf("%d\n", aggs->col_index[aggs->row_ptr[i]+j]);
			   printf("D\n");
			   print_dense(d2, size,size);
			   printf("A\n");
			   print_dense(coarse, size,size);
			   */

		}
		info_c = LAPACKE_dsygst(LAPACK_ROW_MAJOR, 1, 'U', size,
				coarse, size, d, size);
		if (info_c > 0) 
			printf("cantgeenraliz\n");

		info_c = LAPACKE_dsyev( LAPACK_ROW_MAJOR, 'V', 'U', 
				size, coarse, size, solution_c );
		if( info_c > 0 ) {
			printf( "The algorithm failed to compute eigenvalues.\n" );
			exit( 1 );
		}

		int j = size -2;
		int row_temp = 1;
		while(j > 0 && solution_c[size-1] - solution_c[j] < theta) {

			row_temp++;

			j--;
#ifdef DEBUG
			if (solution_c[j] <= 0){
				printf("Negative eigenvalue!\n");
				exit(0);
			}
#endif

		}

		double * row_vector = calloc(row_temp * size, sizeof(double));

		for (int j = 0; j < row_temp; ++j) {
			for ( int k = 0; k < size; ++k) {
				row_vector[k+size*j] = d_inv[k] * coarse[k*size+size-1-j];

			}
		}

		// Set global counters

#pragma omp critical(row_count) 
		row_count[i] = row_temp;

#pragma omp critical(total)
		total += row_temp * size;

#pragma omp critical(vectors)
		vectors[i] = row_vector;

#pragma omp critical(rows)
		rows += row_temp;

		free(solution_c);
		free(d);
		free(coarse);
		free(d_inv);
	}


	Matrix * result = create_matrix(rows, aggs->cols, total);
	int count = 0;
	int row = 0;
	int l = 0;
	for (int i = 0; i < aggs->rows; ++i) {
		int size = aggs->row_ptr[i+1] - aggs->row_ptr[i];
		for (int j = 0; j < row_count[i]; ++j) {
			result->row_ptr[row] = count;
			l = aggs->row_ptr[i];
			for( int k = 0; k < size; ++k) {
				result->values[count] = vectors[i][k+size*j];
				result->col_index[count] = aggs->col_index[l+k];
				count++;
			}
			row++;
		}
		free(vectors[i]);
	}
	result->row_ptr[rows] = total;
	free(vectors);

	return result;
}

void aggregate_with_op(Matrix * mat, Matrix * aggop, int * rows) {
	//Matrix * result;

	int count = 0;
	int * totals = calloc(aggop->cols, sizeof(int));


	/*
	   for (int i = 0; i < aggop->rows; ++i) {
	   for(int j = 0; j < rows[i]; ++j) {
	//printf("mat row :%d\n", count);
	int k = mat->row_ptr[count];
	while (k < mat->row_ptr[count+1] ) {
	int set = 0;
	int size = aggop->row_ptr[i+1] - aggop->row_ptr[i];
	for (int l = 0; l < size; ++l) {
	//printf("mat col: %d\nagg_col:%d\n\n", mat->col_index[k], aggop->col_index[aggop->row_ptr[i]+l]);
	if (mat->col_index[k] == aggop->col_index[aggop->row_ptr[i]+l]){
	set = 1;
	break;
	}
	}
	if (!set) {
	mat->values[k] = 0;
	}
	++k;

	}
	++count;

	}


	}
	*/

	/*
	// old
	for (int i = 0; i < aggop->rows; ++i) {
	for(int j = 0; j < rows[i]; ++j) {
	int set = 0;
	int k = mat->row_ptr[count];
	while (k < mat->row_ptr[count+1] ) {
	set = 1;
	if (totals[mat->col_index[k]] < rows[i]) {
	totals[mat->col_index[k]]++;

	}
	else {
	mat->values[k] = 0;
	}
	++k;

	}
	if (set)
	count++;
	}
	}
	*/
	//older
	for (int i = 0; i < aggop->rows; ++i) {
		for(int j = 0; j < rows[i]; ++j) {
			int k = mat->row_ptr[count];
			int l = aggop->row_ptr[i];

			while (k < mat->row_ptr[count+1] ) {
				if (l >= aggop->row_ptr[i+1]) {
					mat->values[k] = 0;
					++k;
				}
				else if (mat->col_index[k] != 
						aggop->col_index[l]) {
					mat->values[k] = 0;
					++k;
				}
				else {
					++l;
					++k;
				}
			}
			count++;


		}


	}
	free(totals);

}
Matrix * aggregate(Matrix * agglom) {
	Matrix * result = create_matrix(agglom->rows, agglom->cols, agglom->cols);
	int k;
	int * count = calloc(agglom->cols, sizeof(int));

	int row = 0;
	int total = 0;
	for (int i = 0; i < agglom->rows; ++i) {
		k = agglom->row_ptr[i];
		while (k < agglom->row_ptr[i+1] && total < agglom->cols) {
			if(count[agglom->col_index[k]] == 0) {
				result->values[total] = 1;
				result->col_index[total] = agglom->col_index[k];
				count[agglom->col_index[k]]++;
				total++;
			}
			++k;
		}
		row++;
		result->row_ptr[row] = total;
	}
	result->row_ptr[agglom->rows] = total;
	return result;

}


double * dense_diag(double * dense, int size) {
	double * result = calloc(size*size, sizeof(double));

	for(int i = 0; i <size; ++i) {
		result[i*size+i] = dense[i*size+i];
	}
	return result;
}


double * coarse_dense(Matrix * mat, int * indices, int size) {
	int k;
	double * result;
	double * count;
	if (size < 1) {
		printf("no size local graph lapalacian!\n");
		exit(0);
	}
	else if (size == 1) {
		result = malloc(sizeof(double));
		result[0] = 1;
		return result;
	}
	double epsilon;
	//double tau;

	int dense_size = size +1;

	//result = calloc(size*size, sizeof(double));
	result = calloc((dense_size)*(dense_size), sizeof(double));
	count = calloc(size+1, sizeof(double));


	//TODO: Fix this, like n^3 lol
	for (int i = 0; i < size; ++i) {
		k = mat->row_ptr[indices[i]];
		while (k < mat->row_ptr[indices[i] + 1 ] ) {
			if(mat->col_index[k] > indices[i]) {
				for (int j = 0; j < size; ++j) {
					if (mat->col_index[k] == indices[j]) {
						epsilon = (-mat->values[k])/fabs(mat->values[k]);
						//DANGER!!!! Remove!
						//epsilon = 1;
						//tau = 1;
						//printf("epsilon: %f\n", epsilon);
						/*

						// Off Diag
						result[i * dense_size + j] += epsilon*fabs(mat->values[k]);
						result[j * dense_size + i] += epsilon*fabs(mat->values[k]);
						// Diag
						result[i * dense_size + i] += fabs(mat->values[k]);
						result[j * dense_size + j] += fabs(mat->values[k]);
						*/
						// Off Diag
						result[i * size + j] += epsilon*fabs(mat->values[k]);
						result[j * size + i] += epsilon*fabs(mat->values[k]);
						// Diag
						result[i * size + i] += fabs(mat->values[k]);
						result[j * size + j] += fabs(mat->values[k]);
					}
				}
			}
			++k;
		}
		/*
		   if (set == 0){ 
		   fails = 1;
		   printf("index %d col num: %d not set!\n",i, indices[i]);
		   for (int j = 0; j < mat->row_ptr[indices[i]+1]-mat->row_ptr[indices[i]]; ++j) {
		   printf("");

		   }
		   }
		   */
	}
	/*
	   if (fails) {
	   print_dense(result, size,size);

	   }
	   */
	free(count);
	return result;
}






/*
 * Displays usage information to the user.
 *
 */

void show_help_partition(void) {
	printf("Valid Input: matrix rhs e m [-g] [-v] [-p] [-f option] [-o output file]\n");
	printf("             matrix\t- Input Matrix. File in matrices/ \n");
	printf("             rhs\t- Right hand side vector. File in matrices/ \n");
	printf("             e\t\t- Residual tolerance. \n");
	printf("             m\t\t- Number of iterations before restart \n");
	printf("             [-v]\t- Verbose output, displays more information \n");
	printf("             [-g]\t- Generate the rhs matrix and save into rhs file\n");
	printf("             [-p]\t- Print the input matrix\n");
	printf("             [-f]\t- File format: entries(default), adj, adji, hb\n");
	printf("             [-o]\t- Output file name\n");
	exit(1);
}

