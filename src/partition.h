#ifndef __PARTITION_H
#define __PARTITION_H

#include <metis.h>
//#include <mtmetis.h>
#include <stdio.h>
#include <sys/time.h>
#include <time.h>
#include "matrix.h"
#include "parser.h"
#include "precondition.h"


void show_help_partition();
int test_partition(int argc, char *argv[]);



Matrix * edge_vertex(Matrix *);

Matrix * make_agg_edge(int*, int, int);

Matrix * get_agglomerate(Matrix *, int);
Matrix * aggregate_multi(Matrix *, int);
Matrix * aggregate(Matrix *);
int * partition(Matrix *, int );
void svd_aggs_multi(Matrix *, int);

double * local_laplacian(Matrix *, int *, int);
double * local_dense(Matrix *, int *, int);
double * coarse_dense(Matrix *, int *, int);
double * dense_diag(double *, int);


Matrix * create_restriction(Matrix *, int, int);

//Newer
Matrix * create_restriction_agg_edge(Matrix *, int, double);
Matrix * aggregate_edge(Matrix *, Matrix *,int);
Matrix * create_p_agg_edge(Matrix *, Matrix *,double, int *);
void aggregate_with_op(Matrix *, Matrix *, int *);
void svd_agg(Matrix *, int, int *);

// New stuff
Matrix * create_p_agg_edge_parallel(Matrix *, Matrix *,double, int *);
Matrix * create_restriction2(Matrix *, Matrix *,int, double);
Matrix * create_p(Matrix *, Matrix *, Matrix *, Matrix *, double);

double * coarse_m(Matrix *, int *,  int);

Matrix * create_p_multi(Matrix *, Matrix *, int);

#endif
