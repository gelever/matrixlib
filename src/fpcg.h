#ifndef __FPCG_H
#define __FPCG_H

#include <stdio.h>
#include <sys/time.h>
#include <time.h>
#include "matrix.h"
#include "parser.h"
#include "precondition.h"
#include "amge.h"


void show_help();

Result * pcg(
    Matrix * A, Matrix * B_inv, 
    double * b, double * x0, double e, int maxit, 
    int verbose);

Result * fpcg(
    Matrix * A, Matrix * B_inv, 
    double * b, double e, int maxit, 
    int verbose);

#endif
