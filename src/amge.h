#ifndef __AMGE_H
#define __AMGE_H

#include <stdio.h>
#include <sys/time.h>
#include "matrix.h"
#include "parser.h"
#include "fpcg.h"
#include "precondition.h"
#include "partition.h"
#include <lapacke.h>


void show_help();

Result * amge(
    Matrix * A, Matrix * B_inv, 
    double * b, double e, int maxit, 
    int verbose);

Result * mgpcg(
    Matrix * A, Solver * solver,
    double * b, double * x0, double e, int maxit, 
    int verbose);

Solver * setup_solver(Matrix *,int, int, int, double, int);
Result * run_solver(Solver *,  double *, double tol, int maxit, int, int);
Result * run_solver_recursive(Solver *,  double *, double tol, int maxit, int, int);
void solve_level(Level * level, int, int);

void smooth_prolongator(Matrix *, Matrix *, Matrix **, Matrix **, int);

void print_solver(Solver *);
void print_input(double, int, double, int ,int ,int, int);

void destroy_level(Level *);
void destroy_solver(Solver *);



//TODO: this doesnt belong here!
double calc_avg(double *, int);
int solve(Matrix *, double *,double *);
int solve_dense(double *, double *,double *, int);
int solve_factored(double *, double *,double *, int, int *);
int solve_coarse_level(Level *);
int solve_pinv(Level *);
#endif
