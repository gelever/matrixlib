#ifndef __PRECONDITION_H
#define __PRECONDITION_H

#include "parser.h"
#include "matrix.h"

void level_smooth(Level *, double, int);
void weighted_jacobi_smooth(Matrix *, Matrix *, double *, double *, double, int);
void invert_diag(Matrix *);
void sqrt_diag(Matrix *);
void precondition(Matrix *, Matrix *);
Matrix * create_m_inv(Matrix *);

#endif 

