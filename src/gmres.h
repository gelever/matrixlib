#ifndef __GMRES_H
#define __GMRES_H

#include <stdio.h>
#include <sys/time.h>
#include <string.h>
#include <time.h>
#include "matrix.h"
#include "gmres.h"
#include "parser.h"

void show_help();

Result * gmres(Matrix * A, double * b, double e, int maxit, double * x0, int verbose);

#endif
