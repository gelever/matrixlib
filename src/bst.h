#ifndef __BST_H
#define __BST_H

#include <stdlib.h>
#include <stdio.h>

typedef struct BST BST;
typedef struct Node Node;

struct BST {
	Node * head;
	int count;
};

struct Node {
	int value;
	Node * left;
	Node * right;
};

BST * bst_create();
void bst_destroy(BST *);
void bst_insert(BST *, int);
void bst_print(BST *);

Node * node_create(int);
void node_destroy(Node **);
int node_insert(Node **, int);
void node_print(Node *);

#endif

