/*
 * Runs taskone one.  Read in a matrix, transpose it, compute A^t * A,
 *	determine if it is symmetric.
 *
 */

#include "taskone.h"
#define START_FLAGS  2

int main(int argc, char *argv[]) {
	Matrix * mat;
	Matrix * tmat;
	Matrix * product;
	int * options;
	int result;


	if (argc < START_FLAGS || argc > 5) 
		show_help();

	options = parse_cmd(argc, argv, START_FLAGS);

	mat = parse_file(argv[1], options[FORMAT]);
	tmat = transpose(mat);

	product = sparse_mult_sparse(tmat, mat);

	if(options[PRINT]){
		pretty(tmat);
		printf("\n");
		pretty(mat);
		printf("\n");
		pretty(product);
	}

	result = is_symmetric(product);

	printf("Symmetric?\n");
	printf("A^t   :\t%s\n", 
			is_symmetric(tmat) == 1 ? "True" : "False");
	printf("A     :\t%s\n", 
			is_symmetric(mat) == 1 ? "True" : "False");

	printf("A^t*A :\t%s\n", 
			result == 1 ? "True" : "False");


	destroy_matrix(mat);
	destroy_matrix(tmat);
	destroy_matrix(product);
	free(options);
	exit(0);
}

void show_help() {
	printf("Valid Input: matrix  [-g] [-v] [-p] [-f option]\n");
	printf("             matrix\t- Input Matrix. File in matrices/ \n");
	printf("             [-f]\t- File format: entries(default), adj, hb\n");
	printf("             [-p]\t- Print the input matrix\n");
	exit(1);
}
