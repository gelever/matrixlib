/* spec.c
 * Author: Stephan Gelever
 * Date: April, 2016
 *
 * Provides driver for testing spectral radius estimate.
 *
 */

#include "spec.h"
#include "precondition.h"

#define START_FLAGS 2

/*
 * Driver for running spec radius estimate algorithm. 
 *
 * Input:
 *  A - Matrix
 *  b - rhs vector
 *  e - residual tolerance
 *  maxit - maximum number of iterations before restart.
 *  x0 - Initial solution guess.
 *
 *  Flags:
 *	-v - Verbose output
 *	-g - Generate a rhs matrix and save into rhs file
 *	-p - Print the input matrix
 *	-f - Set input file format : entries (default), adj, adji, hb
 *		entries - list of pairs of matrix entries 
 *		adj - adjacency list 
 *		adji - adjacency list with matrix information in first two lines.
 *		       matrix size in first line, nnz in second line
 *		hb - Harwell Boeing format
 *	-o - Output file name
 *
 */

int main(int argc, char *argv[]) {
	Matrix * mat;	// Matrix to work with.
	double result = 0; // Stores result of computation.
	int * options;	// Stores input flags
	Matrix * M;

	if (argc < START_FLAGS || argc > 12) {
		show_help();
		exit(1);
	}

	options = parse_cmd(argc, argv, START_FLAGS);
	mat = parse_file(argv[1], options[FORMAT]);
	M = create_m_inv(mat);
	//Matrix * I = i_minus_spd(mat, 1);
	//pretty(mat);
	//pretty(I);
	
	if (options[PRINT]) 
		pretty(mat);


	result = estimate_spectral_radius(M, 3);
	pretty(M);

	printf("Spectral radius estimate: %g\n", result);

	destroy_matrix(mat);
	free(options);
}



/*
 * Displays usage information to the user.
 *
 */

void show_help(void) {
	printf("Valid Input: matrix rhs x e m [-g] [-v] [-p] [-f option] [-o output file]\n");
	printf("             matrix\t- Input Matrix. File in matrices/ \n");
	printf("             rhs\t- Right hand side vector. File in matrices/ \n");
	printf("             x\t\t- Initial X.  0 for zero vector, anything else is random vector \n");
	printf("             e\t\t- Residual tolerance. \n");
	printf("             m\t\t- Number of iterations before restart \n");
	printf("             [-v]\t- Verbose output, displays more information \n");
	printf("             [-g]\t- Generate the rhs matrix and save into rhs file\n");
	printf("             [-p]\t- Print the input matrix\n");
	printf("             [-f]\t- File format: entries(default), adj, adji, hb\n");
	printf("             [-o]\t- Output file name\n");
}

