#ifndef __PARSER_H
#define __PARSER_H

#include "matrix.h"

// Matrix i/o
Matrix * parse_file(char *, int);

Matrix * entry_read_file(char * fname);
Matrix * adj_read_file(char * fname);
Matrix * adj_with_info_read_file(char * fname);
Matrix * hb_read_file(char * fname);

void write_adj(Matrix *, char *, int);
void write_aggop(Matrix *, char *, int);

// Result i/o
void write_result(Result *, char * fname, int size);

// Vector i/o
int vector_read_file(char * fname, double * b1, int size);
int vector_write_file(char * fname, double * b1, int size);
void read_rhs(double *, char * filename, int size, int * options);

// Generation
void generate_rhs(double *, int, int, char *);

// Parse command line flags
int * parse_cmd(int, char * argv[], int);


#endif /* PARSER_H_ */
