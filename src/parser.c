#include <stdlib.h>
#include <math.h>
#include <stdio.h>
#include <string.h>
#include "parser.h"
#include "matrix.h"

Matrix * entry_read_file(char * fname) {
	Matrix * mat;
	int count = 0;
	int nnz = 0;
	int row_count = 0;
	int col_count = 0;
	int row;
	int col;
	int row_temp = 0;
	double val;
	char buffer[1024];
	char filename[80] = "matrices/";

	strcat(filename, fname);

	FILE * f = fopen(filename, "r");
	if (!f) {
		return 0;
	}


	while (fgets(buffer, 1024, f) != NULL) {
		sscanf(buffer,"%d %d %lf", &row, &col, &val);
		++nnz;
		row_count = fmax(row, row_count);
		col_count = fmax(col, col_count);
	}

	++row_count;
	++col_count;
	rewind(f);

	mat = create_matrix(row_count, col_count, nnz);


	mat->row_ptr[0] = 0;
	while (fgets(buffer, 80, f)) {
		sscanf(buffer,"%d %d %lf", &row, &col, &val);
		if (row > row_temp) {
			row_temp = row;
			mat->row_ptr[row] = count;
		}
		mat->values[count] = val;
		mat->col_index[count] = col;
		++count;
	}

	printf("%s\n", fname);
	printf("nnz: %d\t rows: %d\t cols: %d\n", mat->nnz, mat->rows, mat->cols);

	fclose(f);
	return mat;

}

Matrix * adj_with_info_read_file(char * fname) {
	Matrix * mat;
	int nnz = 0;
	int row_count = 0;
	int row;
	int col;
	char buffer[1024];
	char filename[80] = "matrices/";


	strcat(filename, fname);


	FILE * f = fopen(filename, "r");
	if (!f)
		return 0;


	if( fgets(buffer, 80, f))
		sscanf(buffer, "%d", &row_count);
	else
		return 0;

	if(fgets(buffer, 80, f))
		sscanf(buffer, "%d", &nnz);
	else
		return 0;

	if (row_count <= 0 || nnz <= 0){
		printf("Invalid Size when Parsing!\n");
		exit(1);
	}


	mat = create_matrix(row_count, row_count, nnz+row_count);

	int diag_set = 0;
	int diag_index = 0;
	int diag_total = 0;
	row_count = 0;
	nnz = 0;


	mat->row_ptr[0] = 0;
	while (fgets(buffer, 80, f)) {
		sscanf(buffer,"%d %d", &row, &col);
		if (row != row_count) {
			mat->values[diag_index] = diag_total;
			if (diag_set == 0) {
				mat->col_index[diag_index] = row_count;
				++nnz;
			}
			diag_total = 0;
			diag_index = 0;
			diag_set = 0;
			//row_count += row - row_count;
			row_count++;
			mat->row_ptr[row_count] = nnz;
			//++rp_count;
		}

		if (col > row_count && diag_set == 0) {
			mat->col_index[nnz] = row_count;
			diag_index = nnz;
			++nnz;
			diag_set = 1;
		}
		else if (col < row_count) {
			diag_index = nnz + 1;
		}



		if (row != col) {
			++diag_total;
			mat->values[nnz] = -1.0;
			mat->col_index[nnz] = col;
			++nnz;
		}
		else {
			//diag_total += 2;

			diag_set = 1;
			diag_index =nnz;
			mat->col_index[nnz] = col;
			++nnz;
		}


	}

	mat->values[diag_index] = diag_total;
	if (diag_set == 0) {
		mat->col_index[diag_index] = row_count;
		++nnz;
	}

	mat->row_ptr[mat->rows] = nnz;

	printf("%s\n", fname);
	printf("nnz: %d\t rows: %d\t cols: %d\n", mat->nnz, mat->rows, mat->cols);


	if (f)
		fclose(f);
	return mat;

}
Matrix * adj_read_file(char * fname) {
	Matrix * mat;
	int nnz = 0;
	int row_count = 0;
	int col_count = 0;
	int row;
	int col;
	char buffer[1024];
	char filename[80] = "matrices/";


	strcat(filename, fname);

	FILE * f = fopen(filename, "r");
	if (!f) {
		return 0;
	}


	while (fgets(buffer, 1024, f) != NULL) {
		sscanf(buffer,"%d %d", &row, &col);
		++nnz;
		row_count = fmax(row, row_count);
		col_count = fmax(col, col_count);
	}

	++row_count;
	++col_count;
	rewind(f);

	mat = create_matrix(row_count, col_count, nnz + row_count);

	int diag_set = 0;
	int diag_index = 0;
	int diag_total = 0;
	row_count = 0;
	col_count = 0;
	nnz = 0;


	mat->row_ptr[0] = 0;
	while (fgets(buffer, 80, f)) {
		sscanf(buffer,"%d %d", &row, &col);
		if (row != row_count) {
			mat->values[diag_index] = diag_total;
			if (diag_set == 0) {
				mat->col_index[diag_index] = row_count;
				++nnz;
			}
			diag_total = 0;
			diag_index = 0;
			diag_set = 0;
			//row_count += row - row_count;
			row_count++;
			mat->row_ptr[row_count] = nnz;
			//++rp_count;
		}

		if (col > row_count && diag_set == 0) {
			mat->col_index[nnz] = row_count;
			diag_index = nnz;
			++nnz;
			diag_set = 1;
		}
		else if (col < row_count) {
			diag_index = nnz + 1;
		}



		if (row != col) {
			++diag_total;
			mat->values[nnz] = -1.0;
			mat->col_index[nnz] = col;
			++nnz;
		}
		else {
			//diag_total += 2;

			diag_set = 1;
			diag_index =nnz;
			mat->col_index[nnz] = col;
			++nnz;
		}


	}

	mat->values[diag_index] = diag_total;
	if (diag_set == 0) {
		mat->col_index[diag_index] = row_count;
		++nnz;
	}

	//mat->row_ptr[mat->rows] = nnz;

	printf("%s\n", fname);
	printf("nnz: %d\t rows: %d\t cols: %d\n", mat->nnz, mat->rows, mat->cols);


	fclose(f);
	return mat;

}

Matrix * hb_read_file(char * fname) {
	Matrix * mat;
	int count = 0;
	int nnz = 0;
	int row_count = 0;
	int col_count = 0;
	int row;
	int col;
	int row_temp = 0;
	double val;
	char buffer[1024];
	char filename[80] = "matrices/";

	strcat(filename, fname);

	FILE * f = fopen(filename, "r");
	if (!f) {
		return 0;
	}


	while (fgets(buffer, 1024, f) != NULL) {
		sscanf(buffer,"%d %d %lf", &row, &col, &val);
		++nnz;
		row_count = fmax(row, row_count);
		col_count = fmax(col, col_count);
	}

	++row_count;
	++col_count;
	rewind(f);

	mat = create_matrix(row_count, col_count, nnz);

	//mat->nnz = nnz;
	//mat->rows = row_count;
	//mat->cols = col_count;


	printf("nnz, rows, cols: %d\t%d\t%d\n", nnz, row_count, col_count);
	//mat->values = malloc(sizeof(double)*mat->nnz);
	//mat->col_index = malloc(sizeof(int)*mat->nnz);
	//mat->row_ptr = malloc(sizeof(int)*(mat->rows+1));


	while (fgets(buffer, 80, f)) {
		sscanf(buffer,"%d %d %lf", &row, &col, &val);
		if (row > row_temp) {
			row_temp = row;
			mat->row_ptr[row] = count;
		}
		mat->values[count] = val;
		mat->col_index[count] = col;
		++count;
	}
	//mat->row_ptr[mat->rows] = mat->nnz;


	fclose(f);
	return mat;

}

int vector_read_file(char * fname, double * b1, int size) {
	int count = 0;
	double val;
	char buffer[1024];
	char filename[80] = "matrices/";

	if (!b1 || !fname) {
		return 0;
	}

	strcat(filename, fname);

	FILE * f = fopen(filename, "r");
	if (!f) {
		return 0;
	}


	while (fgets(buffer, 80, f)) {
		sscanf(buffer,"%lf", &val);
		if (count >= size ) {
			return 0;
		}
		b1[count] = val;
		++count;
	}


	fclose(f);
	return 1;

}

int vector_write_file(char * fname, double * b1, int size) {
	if (!b1 || !fname || size < 1)
		return 0;
	char buffer[1024];
	char filename[80] = "matrices/";


	strcat(filename, fname);

	FILE * f = fopen(filename, "w");
	if (!f) {
		return 0;
	}

	for (int i = 0; i < size; ++i) {
		sprintf(buffer, "%g\n", b1[i]);
		fputs(buffer,f);
	}

	fclose(f);
	return 1;

}


void read_rhs(double * rhs, char * filename, int size, int * options) {
	if (options[GENERATE]) 
		generate_rhs(rhs, size, options[FORMAT], filename);
	else 
		vector_read_file(filename, rhs, size);
}

/*
 * Generates a right hand side vector
 * Input:
 * b1 - vector to generate
 * size - size of vector
 * f_flag - type of vector to generate
 * name - file to write vector to
 *
 */
void generate_rhs(double * b1, int size, int f_flag, char * name) {
	int count = 0;
	int temp = 0;

	// Generate orthoganal to constant vector.
	if (f_flag == 1 || f_flag == 2) {
		for (int i = 0; i < size - 1; ++i) {
			temp = rand() % 101 - 50;
			b1[i] = temp;
			count += temp;
		}

		b1[size-1] = -count;
	}
	// Generate all ones.
	else {
		for (int i = 0; i < size; ++i) {
			b1[i] = 1;
		}
	}
	if (!vector_write_file(name, b1, size)) {
		printf("Error writing rhs file!\n");
		exit(1);
	}
}


/*
 * Parses command line arguments and saves to options array.
 * options array:
 *	options[0] : verbose
 *	options[1] : generate rhs
 *	options[2] : print
 *	options[3] : format
 *	options[4] : output to file
 */

int * parse_cmd(int argc, char * argv[], int start_flags) {
	int * options;
	options = malloc(sizeof(int) * NUM_OPTIONS);
	memset(options, 0, sizeof(int) * NUM_OPTIONS);

	for (int i = start_flags; i < argc; ++i) {
		if (strcmp(argv[i],"-g") == 0) {
			options[GENERATE] = 1;
		}
		else if (strcmp(argv[i],"-p") == 0) {
			options[PRINT] = 1;
		}
		else if(strcmp(argv[i],"-v") == 0) {
			options[VERBOSE] = 1;
		}
		else if(strcmp(argv[i],"-f") == 0) {
			if (i + 1 < argc) {
				if (strcmp(argv[i+1], "entries") == 0) {
					options[FORMAT] = 0;
				}
				else if (strcmp(argv[i+1], "adj") == 0) {
					options[FORMAT] = 1;
				}
				else if (strcmp(argv[i+1], "adji") == 0) {
					options[FORMAT] = 2;
				}
				else if (strcmp(argv[i+1], "hb") == 0) {
					options[FORMAT] = 3;
				}
				else {
					printf("Invalid file format specified.");
					exit(1);
				}
			}
		}
		else if(strcmp(argv[i],"-o") == 0) {
			if (i + 1 < argc) {
				options[OUTPUT] = i + 1;
			}
			else {
				printf("-o flag requires file name!");
				exit(1);
			}
		}
	}

	return options;
}

/*
 * Reads a file in using the specified format.
 * Input:
 *  mat - matrix to save information into.
 *  file_name - file to read from
 *  f_flag - format flag
 *
 */
Matrix * parse_file(char * file_name, int f_flag) {
	if (!file_name) {
		printf("Invalid Parse Paramaters!\n");
		exit(1);
	}
	Matrix * mat;

	switch(f_flag) {
		case 0: {
				mat = entry_read_file(file_name);
			}break;
		case 1: {
				mat = adj_read_file(file_name);
			}break;
		case 2: {
				mat = adj_with_info_read_file(file_name);
			}break;
		case 3: {
				mat = hb_read_file(file_name);
			}break;
		default: {
				 printf("Invalid format flag!\n");
				 exit(1);
			 }
	}
	if (!mat) {
		printf("Error reading matrix!\n");
		exit(1);
	}

	return mat;
}

void print_vector(double * vect, int size) {
	if (!vect) {
		printf("Invalid vector to print!\n");
		exit(1);
	}
	for (int i = 0; i < size; ++i) {
		printf("%g\n", vect[i]);
	}

}

void write_aggop(Matrix * mat, char * fname, int base) {
	if (!mat) {
		printf("Invalid matrix to print!\n");
		exit(1);
	}

	int k;
	char buffer[1024];
	char filename[80] = "matrices/";


	strcat(filename, fname);

	FILE * f = fopen(filename, "w");
	if (!f) {
		exit(1);
	}
	sprintf(buffer, "%%%%MatrixMarket matrix coordinate real general\n\%\n%d %d %d\n", mat->rows, mat->cols, mat->nnz);
	fputs(buffer,f);

	for (int i = 0; i < mat->rows; ++i) {
		k = mat->row_ptr[i];
		while (k < mat->row_ptr[i+1]) {
			sprintf(buffer, "%d %d %d\n",
					i + base, 
					mat->col_index[k] + base,
					1);

			fputs(buffer,f);
			++k;
		}
	}
	fclose(f);

}
//void write_adj(Matrix * mat, char * fname) {
void write_adj(Matrix * mat, char * fname, int base) {
	if (!mat) {
		printf("Invalid matrix to print!\n");
		exit(1);
	}

	int k;
	char buffer[1024];
	char filename[80] = "matrices/";


	strcat(filename, fname);

	FILE * f = fopen(filename, "w");
	if (!f) {
		exit(1);
	}

	for (int i = 0; i < mat->rows; ++i) {
		k = mat->row_ptr[i];
		while (k < mat->row_ptr[i+1]) {
			sprintf(buffer, "%d %d %lf\n",
					i + base, 
					mat->col_index[k] + base,
					mat->values[k]);

			fputs(buffer,f);
			++k;
		}
	}
	fclose(f);
}

void write_result(Result * result, char * fname, int size) {
	if (!result || !fname) {
		printf("Invalid Result to write!\n");
		return;
	}

	char x_file[1024];
	char res_file[1024];

	strcpy(x_file, fname);
	strcpy(res_file, fname);

	strcat(x_file, ".sol");
	strcat(res_file, ".res");

	vector_write_file(x_file, result->x, size);
	if (result->residuals) 
		vector_write_file(res_file, result->residuals, result->iter);
}
