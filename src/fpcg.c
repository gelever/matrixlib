/* GMRes.c
 * Author: Stephan Gelever
 * Date: April, 2016
 *
 *
 */

#include "fpcg.h"


/*
 * Approximates the solution to the system by my minimizing the residual ||b-Ax||
 * Input:
 *  A - Matrix
 *  b - rhs vector
 *  e - residual tolerance
 *  maxit - maximum number of iterations before restart.
 *  x0 - Initial solution guess.
 *  verbose - Display more information.
 *
 * Output:
 *  Result  struct holding solution, iteration count, and flags.
 *
 */

Result * fpcg(Matrix * A, Matrix * B_inv, 
		double * b, double e, 
		int maxit, int verbose) 
{

	if (!A || !B_inv || !b || A->rows != A->cols) {
		printf("Invalid Input to FPCG!");
		return NULL;
	}

	int found = 0;
	int count = 0;
	int flag = 0;
	double delta_0 = 0;
	double delta = 0;
	double delta_old = 0;

	double alpha = 0;
	double beta = 0;
	int m;

	struct timeval start_time, end_time;

	e = e*e;

	double *gammas = malloc(sizeof(double) * maxit);
	double *g = malloc(sizeof(double) * A->rows);
	double *x = malloc(sizeof(double) * A->rows);
	double *r = malloc(sizeof(double) * A->rows);
	double *pre_r = malloc(sizeof(double) * A->rows);
	double *P = malloc(sizeof(double) * (maxit+1) * A->rows);
	double *buffer = malloc(sizeof(double) * A->rows);
	double *buffer2 = malloc(sizeof(double) * A->rows);

	if (!r || !x || !pre_r || !P || !buffer || !buffer2) {
		printf("Error allocating memory!");
		flag = 1;
	}


	gettimeofday(&start_time, NULL);

	//Compute initial approximation of x
	sparse_mult_vector(B_inv, b, x);
	//memset(x, 0, sizeof(double) * A->rows);

	// r0 = b-Ax
	sparse_mult_vector(A, x, buffer);
	sub_vector(b, buffer, r, A->rows);

	// Compute initial preconditioned residual
	sparse_mult_vector(B_inv, r, pre_r);

	delta_0 = inner_product(r, pre_r, A->rows);

	// Set P_0
	memmove(P, pre_r, A->rows*sizeof(double));

	// Update tracking variables
	m = 0;
	delta_old = delta_0;

	while (m < maxit && m < A->rows && !flag && !found) {
		if (verbose) {
			//printf("res:     %f \n", sqrt(delta/delta_0));
		}

		sparse_mult_vector(A, P + m * A->rows, g);

		gammas[m] = inner_product(P + m * A->rows, g, A->rows);

		//if (gammas[m] < 0){
		//	printf("\n Gamma Negative!\n");
		//}

		alpha = delta_old/gammas[m];

		// Update iterate x:= x + ap_j
		scalar_mult_vector(P + m * A->rows, buffer, A->rows, alpha);
		add_vector(x, buffer, x, A->rows);


		// Update residual r:= r - ag
		scalar_mult_vector(g, buffer, A->rows, alpha);
		sub_vector(r, buffer, r, A->rows);

		// Compute preconditioned residual
		sparse_mult_vector(B_inv, r, pre_r);

		delta = inner_product(r, pre_r, A->rows);


		// Check stop condition
		if (delta < e * delta_0)
			found = 1;
		else {
			// Set h = Ap_j
			sparse_mult_vector(A, pre_r, buffer);

			// Set p_m+1 = pre_r
			memmove(P + (m+1) * A->rows, pre_r, A->rows*sizeof(double));

			for (int i = 0; i < m + 1; ++i) {
				beta = inner_product(P + i * A->rows, buffer,
						     A->rows)/gammas[i];

				scalar_mult_vector(P + i * A->rows, buffer2,
						A->rows, beta);

				sub_vector(P + (m+1) * A->rows, buffer2,
						P + (m+1) * A->rows, A->rows);
			}
			delta_old = delta;
		}

		m++;
		count++;
	}

	gettimeofday(&end_time, NULL);


	// Setup return data.
	Result  * result = malloc(sizeof(Result));

	result->x = malloc(sizeof(double)*A->rows);
	memmove(result->x, x, A->rows*sizeof(double));

	result->iter = count;
	result->flag = flag;

	result->relres = sqrt(delta/delta_0);

	result->total_time = (end_time.tv_sec - start_time.tv_sec) + 
			     (end_time.tv_usec - start_time.tv_usec)/1000000.0f;

	free(gammas);
	free(g);
	free(x);
	free(r);
	free(pre_r);
	free(P);
	free(buffer);
	free(buffer2);

	return result;
}


// pcg
Result * pcg(Matrix * A, Matrix * B_inv, 
		double * b,double * x, double e, 
		int maxit, int verbose) 
{

	if (!A || !x|| !B_inv || !b || A->rows != A->cols) {
		printf("Invalid Input to PCG!");
		return NULL;
	}

	int found = 0;
	int count = 0;
	int flag = 0;
	double delta_0 = 0;
	double delta = 0;
	double delta_old = 0;

	double alpha = 0;
	int m;

	struct timeval start_time, end_time;

	e = e*e;

	double gamma;
	double *g = malloc(sizeof(double) * A->rows);
	//double *x = malloc(sizeof(double) * A->rows);
	double *r = malloc(sizeof(double) * A->rows);
	double *pre_r = malloc(sizeof(double) * A->rows);
	double *p = malloc(sizeof(double) * A->rows);
	double *buffer = malloc(sizeof(double) * A->rows);
	double *buffer2 = malloc(sizeof(double) * A->rows);

	if (!r || !x || !pre_r || !p || !buffer || !buffer2) {
		printf("Error allocating memory!");
		flag = 1;
	}


	gettimeofday(&start_time, NULL);

	//Compute initial approximation of x
	//sparse_mult_vector(B_inv, b, x);
	//memset(x, 0, sizeof(double) * A->rows);

	// r0 = b-Ax
	sparse_mult_vector(A, x, buffer);
	sub_vector(b, buffer, r, A->rows);

	// Compute initial preconditioned residual
	sparse_mult_vector(B_inv, r, pre_r);

	delta_0 = inner_product(r, pre_r, A->rows);

	// Set p
	memmove(p, pre_r, A->rows*sizeof(double));

	// Update tracking variables
	m = 0;
	delta_old = delta_0;

	while (m < maxit && m < A->rows && !flag && !found) {
		if (verbose) {
			printf("res:     %f \n", sqrt(delta/delta_0));
		}

		sparse_mult_vector(A, p, g);

		gamma = inner_product(p, g, A->rows);

		//if (gammas[m] < 0){
		//	printf("\n Gamma Negative!\n");
		//}

		alpha = delta_old/gamma;

		// Update iterate x:= x + ap_j
		scalar_mult_vector(p, buffer, A->rows, alpha);
		add_vector(x, buffer, x, A->rows);


		// Update residual r:= r - ag
		scalar_mult_vector(g, buffer, A->rows, alpha);
		sub_vector(r, buffer, r, A->rows);

		// Compute preconditioned residual
		sparse_mult_vector(B_inv, r, pre_r);

		delta = inner_product(r, pre_r, A->rows);


		// Check stop condition
		if (delta < e * delta_0)
			found = 1;
		else {
			// Set p_m+1 = pre_r
			scalar_mult_vector(p, buffer, A->rows, delta/delta_old);
			add_vector(pre_r, buffer, p, A->rows);

			delta_old = delta;
		}
		m++;
		count++;
	}

	gettimeofday(&end_time, NULL);


	// Setup return data.
	Result  * result = malloc(sizeof(Result));

	result->x = malloc(sizeof(double)*A->rows);
	memmove(result->x, x, A->rows*sizeof(double));

	result->iter = count;
	result->flag = flag;

	result->relres = sqrt(delta/delta_0);

	result->total_time = (end_time.tv_sec - start_time.tv_sec) + 
			     (end_time.tv_usec - start_time.tv_usec)/1000000.0f;

	free(g);
	//free(x);
	free(r);
	free(pre_r);
	free(p);
	free(buffer);
	free(buffer2);

	return result;
}






