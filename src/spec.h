#ifndef __SPEC_H
#define __SPEC_H

#include <stdio.h>
#include <sys/time.h>
#include <string.h>
#include <time.h>
#include "matrix.h"
#include "gmres.h"
#include "parser.h"

void show_help();

#endif
