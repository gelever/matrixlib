#include "precondition.h"


void test(int argc, char * argv[]) {
	Matrix * mat;
	Matrix * precond;
	double * rhs;
	double * x0;

	mat = entry_read_file(argv[1]);

	precond = malloc(sizeof(Matrix));
	precond = copy_info(mat, mat->rows);
	init_matrix(precond);

	rhs = malloc(sizeof(double) * mat->rows);
	x0 = malloc(sizeof(double) * mat->rows);

	for (int i = 0; i < mat->rows; ++i)
		x0[i] = 0;

	precondition(mat, precond);
	invert_diag(precond);
	weighted_jacobi_smooth(mat, precond, rhs, x0, 2/3, atoi(argv[2]));

	for (int i = 0; i < mat->rows; ++i) {
		printf("x%d: %f\n", i, x0[i]);
	}



	destroy_matrix(mat);
	destroy_matrix(precond);
	free(rhs);
	free(x0);

	exit(0);

}

void level_smooth(Level * level, double weight, int rounds) {
		weighted_jacobi_smooth(level->A, level->M, 
				level->b, level->x, 
				weight, rounds);
}

void weighted_jacobi_smooth(Matrix * mat, 
	    Matrix * m_inv, 
	    double * rhs, 
	    double * x0, 
	    double weight,
	    int rounds
	    ) 
{
	if (!mat || !m_inv || weight == 0.0) {
		printf("Invalid Smooth input!\n");
		exit(1);
	}
	// Hold temporary info
	double * buffer = calloc(mat->rows, sizeof(double));
	double * buffer2 = calloc(mat->rows, sizeof(double));

	// Smoothing
	//
	// Compute:
	// x_k+1 = x_k + M^-1 (b - Ax_k)
	//
	for (int i = 0; i < rounds; ++i) {
		memset(buffer, 0, sizeof(double) * mat->rows);
		memset(buffer2, 0, sizeof(double) * mat->rows);
		sparse_mult_vector(mat, x0, buffer2);
		sub_vector(rhs, buffer2, buffer2, mat->rows);
		sparse_mult_vector(m_inv, buffer2, buffer);
		scalar_mult_vector(buffer, buffer, mat->rows, weight);
		add_vector(x0, buffer, x0, mat->rows);
	}

	

	free(buffer);
	free(buffer2);
}

void sqrt_diag(Matrix * mat) {
	if (!mat) {
		printf("Invalid matrix to sqrt diag!\n");
		exit(1);
	}

	for (int i = 0; i < mat->nnz; ++i) 
			mat->values[i] = sqrt(mat->values[i]);
}

void invert_diag(Matrix * mat) {
	if (!mat) {
		printf("Invalid matrix to invert diag!\n");
		exit(1);
	}

	for (int i = 0; i < mat->nnz; ++i) { 
		if (mat->values[i] == 0) {
			printf("DIV BY ZERO in invert!!\n");
			mat->values[i] = 0;

		}
		else {
			mat->values[i] = 1.0/mat->values[i];
		}

	}
}


//TODO: Make this return a Matrix instead.
void precondition(Matrix * mat, Matrix * result) {

	if (!mat || !result || !is_symmetric(mat)) {
		printf("Invalid Precondition input!\n");
		exit(1);
	}



	int k = 0;

	for (int i = 0; i < result->rows; ++i) {
		result->row_ptr[i] = i;
		result->col_index[i] = i;
	}

	// No check if diag even exists!
	// Copy diagonal
	for (int i = 0; i < mat->rows; ++i) {
		k = mat->row_ptr[i];
		while (k < mat->row_ptr[i+1]) {
			//printf("Info: i:%d k:%d val:%f \n", i, k, mat->values[k]);
			if (mat->col_index[k] == i){
				// *2 so that it mathes pyamg
				result->values[i] = mat->values[k];
				break;
			}
			++k;
		}
	}

	result->row_ptr[result->rows] = result->nnz;

	double tau = 0;
	double epsilon = 0;

	// Fold in off diagonal values
	for (int i = 0; i < mat->rows; ++i) {
		k = mat->row_ptr[i];
		while (k < mat->row_ptr[i+1]) {
			//if (mat->col_index[k] != i && mat->col_index[k] > i){
			if (mat->col_index[k] < i){
				if (result->values[mat->col_index[k]] == 0) {
					printf("DIV BY ZERO!");
					continue;
					tau = 1;
				}
				else {
					tau = 1;
					//tau = sqrt(result->values[i]/result->values[mat->col_index[k]]);
				}
				//epsilon = (-mat->values[k])/fabs(mat->values[k]);
				epsilon = 1;
				// Scaled
				//result->values[i] += epsilon*fabs(mat->values[k])*tau;
				result->values[i] += epsilon*fabs(mat->values[k])*tau;
				//result->values[mat->col_index[k]] += epsilon*fabs(mat->values[k])/tau;
				result->values[mat->col_index[k]] += epsilon*fabs(mat->values[k])/tau;
				//
				// Unscaled
				//result->values[i] += fabs(mat->values[k]);
				//result->values[mat->col_index[k]] += fabs(mat->values[k]);
				//result->values[i] += abs(mat->values[k]);
				//result->values[mat->col_index[k]] += abs(mat->values[k]);
				/*
				result->values[i] += abs(mat->values[k])*tau;
				result->values[i] += abs(mat->values[k]);
				result->values[mat->col_index[k]] += abs(mat->values[k])/tau;
				result->values[mat->col_index[k]] += abs(mat->values[k]);
				*/

				//printf("Info: i:%d j:%d k:%d val:%.2f tau:%.2f\n", i, mat->col_index[k],k, mat->values[k], tau);

			}
			++k;
		}
	}


}


Matrix * create_m_inv(Matrix * mat) 
{
	Matrix * result;

	result = create_matrix(mat->rows, mat->rows, mat->rows);
	precondition(mat, result);
	invert_diag(result);


	return result;
}



