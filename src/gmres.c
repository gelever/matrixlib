/* GMRes.c
 * Author: Stephan Gelever
 * Date: April, 2016
 *
 * Provides implementation and driver for Generalized Minimal Residual algorithm.
 *
 */

#include "gmres.h"

#define START_FLAGS 6

/*
 * Driver for running GMRes algorithm. 
 *
 * Input:
 *  A - Matrix
 *  b - rhs vector
 *  e - residual tolerance
 *  maxit - maximum number of iterations before restart.
 *  x0 - Initial solution guess.
 *
 *  Flags:
 *	-v - Verbose output
 *	-g - Generate a rhs matrix and save into rhs file
 *	-p - Print the input matrix
 *	-f - Set input file format : entries (default), adj, adji, hb
 *		entries - list of pairs of matrix entries 
 *		adj - adjacency list 
 *		adji - adjacency list with matrix information in first two lines.
 *		       matrix size in first line, nnz in second line
 *		hb - Harwell Boeing format
 *	-o - Output file name
 *
 */

int main(int argc, char *argv[]) {
	Matrix * mat;	// Matrix to work with.
	Result * result; // Stores result of computation.
	int * options;	// Stores input flags
	double * x;		// Initial x.
	double * rhs;		// Right hand side


	if (argc < START_FLAGS || argc > 12) {
		show_help();
		exit(1);
	}

	options = parse_cmd(argc, argv, START_FLAGS);
	mat = parse_file(argv[1], options[FORMAT]);



	rhs = malloc(sizeof(double)*mat->cols);
	x = malloc(sizeof(double)*mat->cols);

	memset(x, 0, sizeof(double) * mat->rows);

	read_rhs(rhs, argv[2], mat->cols, options);

	
	if (options[PRINT]) 
		pretty(mat);

	// Not sure, this changes the answer.  TODO: Fix or figure out if 
	// this is intended behaviour
	//if (strcmp(argv[3], "0") == 0)
	//fill_rand(x, mat->cols);

	result = gmres(
			mat,		// Input Matrix
			rhs,		// Right hand side
			atof(argv[4]),	// epsilon tolerance
			atoi(argv[5]),	// max iterations
			x,		// Initial x
			options[VERBOSE]); //Verbose output

	print_result(result, options, mat->rows, argv[options[OUTPUT]]);



	destroy_result(result);
	destroy_matrix(mat);
	free(rhs);
	free(x);
	free(options);
}



/*
 * Approximates the solution to the system by my minimizing the residual ||b-Ax||
 * Input:
 *  A - Matrix
 *  b - rhs vector
 *  e - residual tolerance
 *  maxit - maximum number of iterations before restart.
 *  x0 - Initial solution guess.
 *  verbose - Display more information.
 *
 * Output:
 *  Result struct holding solution, iteration count, and flags.
 *
 */

Result * gmres(Matrix * A, double * b, double e, int maxit, double * x0, int verbose) 
{
	if (!A || !b || !x0 || A->rows != A->cols) {
		printf("Invalid Input to GMRes!");
		return NULL;
	}

	Result * result;
	double total_time;

	int found = 0;
	int count = 0;
	int flag = 0;
	double old_r_norm = 0;
	double r_norm = 0;
	int m = 0;
	struct timeval start_time, end_time;

	double *betas = malloc(sizeof(double) * A->rows);
	double *r0 = malloc(sizeof(double) * A->rows);
	double *P = malloc(sizeof(double) * maxit * A->rows);
	double *B = malloc(sizeof(double) * maxit * A->rows);
	double *Q = malloc(sizeof(double) * maxit * A->rows);
	double *R = malloc(sizeof(double) * maxit * A->rows);
	double *buffer = malloc(sizeof(double) * A->rows);
	double *buffer2 = malloc(sizeof(double) * A->rows);

	if (!betas || !r0 || !P || !B || !Q
			|| !R || !buffer 
			|| !buffer2) {
		printf("Error allocating memory!");
		flag = 1;
	}

	gettimeofday(&start_time, NULL);

	while (!found && !flag) {
		// r0 = b-Ax
		sparse_mult_vector(A, x0, buffer);
		sub_vector(b, buffer, r0, A->rows);

		// Find r0 norm
		r_norm = norm(r0, A->rows);
		if (!r_norm) {
			printf("r_norm: %g\n", r_norm);
			flag = 1;
		}


		// Calculate p1 = (1/||r0||)*r0
		for (int i = 0; i < A->rows; ++i) {
			*(P + i) = r0[i]/r_norm;
		}

		// Calculate b1 = A*p1
		sparse_mult_vector(A, P, B);

		// Calculate q1 and first vector in R
		double b1norm = norm(B,A->rows);
		scalar_mult_vector(B, Q, A->rows, 1/b1norm);
		*R = inner_product(B, Q, A->rows);


		// Calculate t1
		double b1tb1 = inner_product(B, B, A->rows);
		double b1tr = inner_product(B, r0, A->rows);
		double t = 0;

		if (b1tb1 == 0) {
			printf("b1t1 = 0\n");
			flag = 1;
		}
		else {
			t = b1tr/b1tb1;

			// Calculate next iterations x, r
			scalar_mult_vector(P, buffer, A->rows, t);
			add_vector(x0, buffer, x0, A->rows);

			scalar_mult_vector(B, buffer, A->rows, t);
			sub_vector(r0, buffer, r0, A->rows);
			r_norm = norm(r0, A->rows);
			old_r_norm = r_norm;
		}

		// Update tracking variables
		++count;
		m = 1;

		while (m < maxit && r_norm > e && !flag) {
			if (verbose)
				printf("m: %d\trnorm:\t%.20g\n", m, r_norm);

			// Calculate betas
			for (int i = 0; i < m; ++i) {
				betas[i] = inner_product(P + i * A->rows,r0,A->rows);
			}

			memmove(buffer, r0,A->rows*sizeof(double));

			// Calculate p hat
			for (int i = 0; i < m; ++i) {
				scalar_mult_vector(P + i * A->rows, buffer2, A->rows, betas[i]);
				sub_vector(buffer, buffer2, buffer, A->rows);
			}

			// Calculate P from p hat/phatnorm
			double p_hat_norm = norm(buffer, A->rows);
			scalar_mult_vector(buffer, P + m * A->rows, A->rows, 1/p_hat_norm);

			// Calculate b_m
			sparse_mult_vector(A, P + m * A->rows, B + m * A->rows);

			memmove(buffer, B + m * A->rows,A->rows * sizeof(double));

			// Find next q_hat_m
			double t = 0;
			for (int i = 0; i < m; ++i) {
				t = inner_product(Q + i * A->rows, B + m * A->rows, A->rows);
				*(R + i * A->rows + m) = t;
				scalar_mult_vector(Q + i * A->rows, buffer2, A->rows, t);
				sub_vector(buffer, buffer2, buffer, A->rows);
			}

			// Calculate q_m
			double q_hat_norm = norm(buffer, A->rows);
			for (int i = 0; i<A->rows; ++i) 
				*(Q + m * A->rows + i ) = *(buffer + i)/q_hat_norm;

			if (verbose) {
				printf("\t\tphatnorm: %g\n", p_hat_norm);
				printf("\t\tqhatnorm: %g\n", q_hat_norm);
			}

			// Update R for q_m
			*(R + m * A->rows + m) = inner_product(Q + m * A->rows, B + m * A->rows, A->rows);

			// Calculate qtb
			dense_mult_vector(Q, r0, buffer, m+1, A->rows);

			// Find ti by solving Rx=Q^t * b
			solve_upper(R, buffer2, buffer, m+1, A->rows);

			// Update x
			dense_trans_mult_vector(P, buffer2, buffer, m+1, A->rows);
			add_vector(x0, buffer, x0, A->rows);

			// Update r
			dense_trans_mult_vector(B, buffer2, buffer, m+1, A->rows);
			sub_vector(r0, buffer, r0, A->rows);

			// Update tracking variables
			old_r_norm = r_norm;
			r_norm = norm(r0, A->rows);

			if(r_norm > old_r_norm) {
				printf("r_norm increased at count: %d\n", count);
				flag = 2;
			}

			++count;
			++m;

		}

		// Check if NaN.
		if (r_norm != r_norm) {
			printf("Hit under or overflow! lower m\n");
			flag = 2;
		}

		// Check stopping criteria.
		if (r_norm < e){ 
			found = 1;
		}


	}

	gettimeofday(&end_time, NULL);


	// Setup return data.
	total_time = (end_time.tv_sec - start_time.tv_sec) 
		+ (end_time.tv_usec - start_time.tv_usec)/1000000.0f;
	result = create_result(x0, NULL, A->rows, count, flag, r_norm,
			total_time);

	free(betas);
	free(r0);
	free(P);
	free(B);
	free(Q);
	free(R);
	free(buffer);
	free(buffer2);

	return result;
}


/*
 * Displays usage information to the user.
 *
 */

void show_help(void) {
	printf("Valid Input: matrix rhs x e m [-g] [-v] [-p] [-f option] [-o output file]\n");
	printf("             matrix\t- Input Matrix. File in matrices/ \n");
	printf("             rhs\t- Right hand side vector. File in matrices/ \n");
	printf("             x\t\t- Initial X.  0 for zero vector, anything else is random vector \n");
	printf("             e\t\t- Residual tolerance. \n");
	printf("             m\t\t- Number of iterations before restart \n");
	printf("             [-v]\t- Verbose output, displays more information \n");
	printf("             [-g]\t- Generate the rhs matrix and save into rhs file\n");
	printf("             [-p]\t- Print the input matrix\n");
	printf("             [-f]\t- File format: entries(default), adj, adji, hb\n");
	printf("             [-o]\t- Output file name\n");
}

