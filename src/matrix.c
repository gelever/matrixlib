/* matrix.c
 * Author: Stephan Gelever
 * Date: April, 2016
 *
 * Provides a toolkit to work with matrices.
 *
 */

#include "matrix.h"

/* 
 * Checks if matrix symmetric by calculating v^t*A*w and w^t*A*v
 * where v and w are random row vectors.
 *
 * Input:
 *   mat - matrix to check
 *
 * Output:
 *   0 if not symmetric
 *   1 if symmetric
 */
int is_symmetric(Matrix * mat) 
{
	if(!mat || mat->rows != mat->cols)
		return 0;
	srand((unsigned) time(NULL));

	double * vector1;
	double * vector2;
	double * buffer;
	vector1 = malloc(sizeof(double)*mat->cols);
	vector2 = malloc(sizeof(double)*mat->cols);
	buffer = malloc(sizeof(double)*mat->cols);


	for (int i = 0; i < mat->rows; ++i) {
		vector1[i] = rand() % 100;
		vector2[i] = rand() % 100;
	}

	sparse_mult_vector(mat, vector1, buffer);
	double total1 = inner_product(buffer, vector2, mat->rows);

	sparse_mult_vector(mat, vector2, buffer);
	double total2 = inner_product(buffer, vector1, mat->rows);

	free(vector1);
	free(vector2);
	free(buffer);

	return abs(total1 - total2) < .000001;
}


/*
 * Calculates A^t * x, given just A,x.  Multiplies with the transpose in mind.
 *
 * Input:
 *   mat - Dense matrix which is to multiply the input vector
 *   vector - Dense vector, x
 *   result - Dense vector that holds the result of A^t * x
 *   rows - number of rows in A
 *   cols - number of coloumns in A
 */
void dense_trans_mult_vector(double * mat, double * vector, double * result, int rows, int cols) 
{
	double total;
	for (int i = 0; i < cols; ++i) {
		total = 0;
		for (int j = 0; j < rows; ++j) {
			total += *((double*)mat + j*cols + i) * vector[j];
		}
		result[i] = total;
	}

}

/*
 * Transposes the given Matrix in CSR format.
 *
 * Input:
 *   input - matrix to transpose.
 *   output - result of transpose.
 * Output:
 *   int indicating success: 0 is failure.
 */
Matrix * transpose(Matrix * input) 
{
	if (!input) {
		printf("Invalid tranpose input!\n");
		exit(1);
	}

	Matrix * result = create_matrix(input->cols, input->rows, input->nnz);

	int * col_count = calloc(input->cols, sizeof(int));
	if(!col_count) {
		printf("Error allocating space in transpose!");
		exit(0);
	}

	for (int i = 0; i < input->nnz; ++i) {
		col_count[input->col_index[i]]++;
	}

	int count = 0;
	for(int i = 0; i < input->cols; ++i) {
		result->row_ptr[i+1] = count + col_count[i];
		count += col_count[i];
	}

	int * col_position = malloc((input->cols + 1) * sizeof(int));

	// Use col_position to keep track of current position in column pointers
	memmove(col_position, result->row_ptr, (input->cols + 1)*sizeof(int));

	for (int i = 0; i < input->rows; ++i) {
		int k = input->row_ptr[i];
		while (k < input->row_ptr[i+1]) {
			int col = input->col_index[k];
			int pos = col_position[col];

			result->col_index[pos] = i;
			result->values[pos] = input->values[k];
			++col_position[col];
			++k;
		}
	}




	result->row_ptr[result->rows] = result->nnz;

	free(col_count);
	free(col_position);
	return result;
}

/*
 * Multiplies a sparse matrix by a vector.
 *
 * Input:
 *   mat - Sparse matrix
 *   vector - vector being multiplied by
 *   result - holds the result of the multiplication, dense vector
 */
void sparse_mult_vector(Matrix * mat, double * vector, double * result) 
{
	int k = 0;
	double total;
	for (int i = 0; i < mat->rows; ++i) {
		k = mat->row_ptr[i];
		total = 0;
		while (k < mat->row_ptr[i+1]) {
			total += mat->values[k] * vector[mat->col_index[k]];
			++k;
		}
		result[i] = total;
	}
}

/*
 * Multiplies a dense matrix by a dense vector.
 *
 * Input:
 *   mat - 2d dense matrix
 *   vector - vector being multiplied by
 *   result - vector holding result of the multiplication
 *   rows - rows in input matrix
 *   cols - coloumns in input matrix
 */
void dense_mult_vector(double *mat, double * vector, double * result, int rows, int cols) 
{
	double total;
	for (int i = 0; i < rows; ++i) {
		total = 0;
		for (int j = 0; j < cols; ++j) {
			total += *((double*)mat + i*cols + j) * vector[j];
		}
		result[i] = total;
	}
}

/*
 * Calculates norm of a vector.
 *
 * Input:
 *   v1 - vector to norm
 *   size - size of vector
 */
double norm(double * v1, int size) 
{
	return sqrt(inner_product(v1,v1,size));
}

/*
 * Calculates the inner product of two vectors.
 *
 * Input:
 *   v1, v2 - two vectors to find inner product of
 *   size - size of both vectors
 *
 * Output:
 *   The calculated inner product
 */
double inner_product(double *v1, double *v2, int size) 
{
	double total = 0;
	for (int i = 0; i < size; ++i) {
		total += (v1[i] * v2[i]);
	}
	return total;
}

/*
 * Adds two vectors together.
 *
 * Input:
 *   lhs - left hand vector
 *   rhs - right hand vector
 *   result - holds the result of the the addition
 *   size - size of all input vectors
 */
void add_vector(double *lhs, double *rhs, double *result, int size) 
{
	for (int i = 0; i < size; ++i) {
		result[i] = lhs[i] + rhs[i];
	}
}

/*
 * Subtracts two vectors.
 *
 * Input:
 *   lhs - left hand vector
 *   rhs - right hand vector
 *   result - holds the result of the the addition
 *   size - size of all input vectors
 */
void sub_vector(double *lhs, double *rhs, double *result, int size) 
{
	for (int i = 0; i < size; ++i) {
		result[i] = lhs[i] - rhs[i];
	}
}

/*
 * Multiplies a scalar through a vector.
 *
 * Input:
 *   vector - vector that will be multiplied
 *   result - holds the result of the the multiplication
 *   size - size of all input vectors
 *   scalar - scalar that will be multiplied through the vector
 */
void scalar_mult_vector(double *vector, double *result, int size, double scalar) 
{
	for (int i = 0; i < size; ++i) 
		result[i] = vector[i] * scalar;
}

/*
 * Solves an upper right triangle system.
 *
 * Input: 
 *   A - 2d Array holding upper triangle matrix.  Dense.
 *   x - holds solution vector.
 *   b - right hand side vector
 *   size - size of upper right triangular matrix in A
 *   total_size - total size of A.  not all of A must be upper right triangular.
 *
 * Requires A be in a specific format:
 *
 * Assumes full square array A. Must be full rank upper triangular matrix.
 * If Input([[1],[2,1],[3,2,1]], [1,2,3]):
 *
 * Solves A[0] A[1] A[2]   [x]  =  [b]
 *
 *         [1]  [2]  [3]   [x1]    [b1]
 *         [-]  [1]  [2]   [x2]    [b2]
 *         [-]  [-]  [1]   [x3]    [b3]
 */
void solve_upper(double * A, double * x, double * b, int size, int total_size) 
{
	double total;
	double div;
	for (int i = size - 1; i >= 0; --i) {
		total = 0;
		for (int j = size - 1; j > i; --j) {
			total += *((double*)A + i*total_size + j) * x[j];
		}
		div = *((double*)A + i*total_size + i);
		if (div == 0) {
			printf("div by zero in upper solve!\n");
			return;
		}
		x[i] = (b[i]-total)/div;
	}
}

/*
 * Calculates (I-wA) where I is the identity, A is SPD, and w is real
 *
 * Input:
 *   A - SPD Matrix
 *   scale - scale of A to subtract
 * Ouptput:
 *   Matrix holding the subtraction
 */
Matrix * i_minus_spd(Matrix * A, double scale) {
	Matrix * result = create_matrix(A->rows, A->cols, A->nnz);
	memcpy(result->row_ptr, A->row_ptr, A->rows * sizeof(int));
	memcpy(result->col_index, A->col_index, A->nnz * sizeof(int));

	for (int i = 0; i < A->rows; ++i) {
		int k = A->row_ptr[i];
		while(k < A->row_ptr[i+1]) {
			result->values[k] = (-scale) * A->values[k];
			if (i == A->col_index[k]) {
				result->values[k] += 1.0;
			}
			++k;
		}
	}
	return result;
}

/*
 * Estimates the spectral radius of A using Lanczos algorithm
 *
 * Input:
 *   A - Matrix to estimate spectral radius of
 *   m - number of iterations of Lanczos to perform
 *
 * Output:
 *   Estimated spectral radius
 */
double estimate_spectral_radius(Matrix * A, int m) {
	double v_norm = 0;
	double alpha = 0;
	double beta = 0;
	int size = A->rows;

	double * temp;

	//double ** V = malloc(sizeof(double*) * 2);
	double * v0 = calloc(size, sizeof(double));
	double * v1 = malloc(size * sizeof(double));
	double * buffer = calloc(size, sizeof(double));
	double * buffer2 = calloc(size, sizeof(double));
	double * H = calloc(m*m, sizeof(double));

	// Set intial random vector
	for (int i = 0; i < size; ++i ) 
		v1[i] = 1.0 + (double)rand()/(double)(RAND_MAX);

	// Normalize it
	v_norm = norm(v1, size);
	scalar_mult_vector(v1, v1, size, 1/v_norm);

	// Do Lanczos algorithm to construct H
	for( int i = 0; i < m; ++i) {

		sparse_mult_vector(A, v1, buffer);
		alpha = inner_product(buffer, v1, size);

		memset(buffer2, 0, size * sizeof(double));
		scalar_mult_vector(v1, buffer2, size, alpha);
		sub_vector(buffer, buffer2, buffer, size);

		memset(buffer2, 0, size * sizeof(double));
		scalar_mult_vector(v0, buffer2, size, beta);
		sub_vector(buffer, buffer2, buffer, size);

		beta = norm(buffer, size);
		if(i > 0){
			H[(i-1) * m + i] = beta;
			H[i * m + i - 1 ] = beta;
		}
		H[i*m + i] = alpha;

		temp = v0;
		v0 = v1;
		scalar_mult_vector(buffer, temp, size, beta);
		v1 = temp;
	}

	// Calculate the Eigenvalues of H, that should approximate 
	// Eigenvalues of A
	double solution[m];
	int info = LAPACKE_dsyev( LAPACK_ROW_MAJOR, 'V', 'U', 
			m, H, m, solution );

	if( info > 0 ) {
		printf( "The algorithm failed to compute eigenvalues.\n" );
		exit( 1 );
	}

	free(v0);
	free(v1);
	free(buffer);
	free(buffer2);
	free(H);

	return solution[m-1];
}

/* 
 * Solves C = AB 
 * where A and B are sparse matrices
 * Does not keep coloumns in order.
 *
 * Follows algorithm by Bank and Douglas
 * "Sparse Matrix Multiplication Package (SMMP)"
 * http://www.i2m.univ-amu.fr/~bradji/multp_sparse.pdf 
 *
 * Input:
 *   A - lhs sparse matrix
 *   B - rhs sparse matrix
 *
 * Output:
 *   C - result of A*B
 */
Matrix * sparse_mult_sparse_unorder(Matrix * A, Matrix * B) {
	int maxab = A->rows > B->rows ? A->rows : B->rows;
	int max = maxab > A->cols ? maxab : A->cols;

	int * index = malloc(max * sizeof(int));
	for (int i = 0; i < max; ++i) {
		index[i] = -1;
	}
	int * c_row = calloc(A->rows + 1, sizeof(int));

	// Symbolic pass
	for (int i = 0; i < A->rows; ++i) {
		int length = 0;
		int k = A->row_ptr[i];
		while (k < A->row_ptr[i+1]) {
			int col = A->col_index[k];
			int j = B->row_ptr[col];
			while (j < B->row_ptr[col+1]) {
				int B_col = B->col_index[j];
				if (index[B_col] != i){
					index[B_col] = i;
					++length;
				}
				++j;
			}
			++k;
		}
		c_row[i+1] = c_row[i] + length;
	}

	Matrix * result = create_matrix(A->rows, B->cols, c_row[A->rows]);

	double * temp = calloc(B->cols, sizeof(double));
	for (int i = 0; i < max; ++i) {
		index[i] = -1;
	}

	int total = 0;

	// Numerical Pass
	for (int i = 0; i < A->rows; ++i) {
		int start = -2;
		int length = 0;
		int k = A->row_ptr[i];
		while (k < A->row_ptr[i+1]) {
			int A_col = A->col_index[k];
			int j = B->row_ptr[A_col];
			while (j < B->row_ptr[A_col+1]) {
				int B_col = B->col_index[j];
				temp[B_col] = temp[B_col] + A->values[k]*B->values[j];
				if (index[B_col] == -1) {
					index[B_col] = start;
					start = B_col;
					++length;
				}

				++j;
			}
			++k;
		}
		for (int j = 0; j < length; ++j) {
			if (temp[start] != 0) {
				result->col_index[total] = start;
				int col = result->col_index[total];
				result->values[total] = temp[col];
				temp[col] = 0;
				++total;
			}

			int t = start;
			start = index[start];

			index[t] = -1;
		}
		result->row_ptr[i+1] = total;
	}
	result->nnz = total;

	free(c_row);
	free(index);
	free(temp);

	return result;
}

/*
 * Multiplies two sparse matrices in CSR format. 
 * Inefficient but correct. Keeps coloumns in order.
 *
 * Input:
 *   lhs - left hand side matrix 
 *   rhs - right hand side matrix
 *   result - holds the result of the multiplication
 *
 * Output:
 *   Matrix holding the solution to A*B
 */
Matrix * sparse_mult_sparse_slow(Matrix * lhs, Matrix * rhs) 
{
	if (!lhs || !rhs || lhs->cols != rhs->rows)
		return 0;
	Matrix * result;
	int count = 0;
	int lhs_col_idx = 0;
	int rhs_col_idx = 0;
	double total = 0;
	int lhs_col = 0;
	int rhs_col = 0;

	// First pass to find how many nnz of result matrix
	for (int i = 0; i < lhs->rows; ++i) {
		for (int k = 0; k < rhs->cols; ++k) {
			lhs_col_idx = lhs->row_ptr[i];
			total = 0;
			while (lhs_col_idx < lhs->row_ptr[i+1]) {
				lhs_col = lhs->col_index[lhs_col_idx];
				rhs_col_idx = rhs->row_ptr[lhs_col];
				if (rhs->row_ptr[lhs_col] == rhs->row_ptr[lhs_col+1] ) {
					++lhs_col_idx;
					continue;

				}
				rhs_col = rhs->col_index[rhs_col_idx];
				while (rhs_col_idx < rhs->row_ptr[lhs_col + 1] && rhs_col < k && rhs_col_idx +1< rhs->nnz) {
					++rhs_col_idx;
					rhs_col = rhs->col_index[rhs_col_idx];
				}
				if (rhs_col == k && rhs_col_idx < rhs->row_ptr[lhs_col+1]) {
					total += lhs->values[lhs_col_idx]*rhs->values[rhs_col_idx];
				}
				++lhs_col_idx;
			}
			if (total != 0) {

				++count;
			}
		}
	}

	result = create_matrix(lhs->rows, rhs->cols, count);

	// Second pass to do multiplication
	count = 0;
	for (int i = 0; i < lhs->rows; ++i) {
		result->row_ptr[i] = count;
		for (int k = 0; k < rhs->cols; ++k) {
			lhs_col_idx = lhs->row_ptr[i];
			total = 0;
			while (lhs_col_idx < lhs->row_ptr[i+1]) {
				lhs_col = lhs->col_index[lhs_col_idx];
				rhs_col_idx = rhs->row_ptr[lhs_col];
				if (rhs->row_ptr[lhs_col] == rhs->row_ptr[lhs_col+1] ) {
					++lhs_col_idx;
					continue;

				}
				rhs_col = rhs->col_index[rhs_col_idx];
				while (rhs_col_idx < rhs->row_ptr[lhs_col + 1] && rhs_col < k && rhs_col_idx +1< rhs->nnz) {
					++rhs_col_idx;
					rhs_col = rhs->col_index[rhs_col_idx];
				}
				if (rhs_col == k && rhs_col_idx < rhs->row_ptr[lhs_col+1]) {
					total += lhs->values[lhs_col_idx]*rhs->values[rhs_col_idx];
				}
				++lhs_col_idx;
			}
			if (total != 0) {
				result->values[count] = total;
				result->col_index[count] = k;

				++count;
			}
		}
	}
	result->nnz = count;
	result->row_ptr[result->rows] = count;

	++count;
	double * dtemp;
	int * itemp;
	dtemp = realloc(result->values, count * sizeof(double));
	itemp = realloc(result->col_index, count * sizeof(int));

	if (itemp == NULL || dtemp == NULL){
		printf("Error reallocing!\n");
		//exit(1);
	}
	else { 

		result->values = dtemp;
		result->col_index = itemp;
	}
	return result;
}

/* 
 * Solves C = AB 
 * where A and B are sparse matrices
 * Keeps coloumns in order.
 *
 * Modifies algorithm by Bank and Douglas.
 * Uses a BST to keep coloumns in order.
 * "Sparse Matrix Multiplication Package (SMMP)"
 * http://www.i2m.univ-amu.fr/~bradji/multp_sparse.pdf 
 *
 * Input:
 *   A - lhs sparse matrix
 *   B - rhs sparse matrix
 * Output:
 *   C - result of A*B
 */
Matrix * sparse_mult_sparse(Matrix * A, Matrix * B) {
	int maxab = A->rows > B->rows ? A->rows : B->rows;
	int max = maxab > A->cols ? maxab : A->cols;

	int * index = malloc(max * sizeof(int));
	for (int i = 0; i < max; ++i) {
		index[i] = -1;
	}
	int * c_row = calloc(A->rows + 1, sizeof(int));

	// Symbolic pass
	for (int i = 0; i < A->rows; ++i) {
		int length = 0;
		int k = A->row_ptr[i];
		while (k < A->row_ptr[i+1]) {
			int col = A->col_index[k];
			int j = B->row_ptr[col];
			while (j < B->row_ptr[col+1]) {
				int B_col = B->col_index[j];
				if (index[B_col] != i){
					index[B_col] = i;
					++length;
				}
				++j;
			}
			++k;
		}
		c_row[i+1] = c_row[i] + length;
	}

	Matrix * result = create_matrix(A->rows, B->cols, c_row[A->rows]);

	double * temp = calloc(B->cols, sizeof(double));
	for (int i = 0; i < max; ++i) {
		index[i] = -1;
	}

	int total = 0;

	// Numerical Pass
	for (int i = 0; i < A->rows; ++i) {
		BST * bst = bst_create();
		int k = A->row_ptr[i];
		while (k < A->row_ptr[i+1]) {
			int A_col = A->col_index[k];
			int j = B->row_ptr[A_col];
			while (j < B->row_ptr[A_col+1]) {
				int B_col = B->col_index[j];
				temp[B_col] = temp[B_col] + A->values[k]*B->values[j];
				bst_insert(bst, B_col);

				++j;
			}
			++k;
		}

		int num = bst_set_row(bst, result, temp, i);
		total += num;
		result->row_ptr[i+1] = total;
		bst_destroy(bst);
	}
	result->nnz = total;

	free(c_row);
	free(index);
	free(temp);

	return result;
}


/*
 * Utility function to the values and columns of a row from A 
 *
 * Input:
 *   bst - BST keeping track of valid indices in values
 *   A - Matrix to set values/columns of
 *   values - array of calculated values from sparse_mult_sparse
 *   row - which row to set in A
 * Output:
 *   number of nnz in the row
 */
int bst_set_row(BST * bst, Matrix * A, double * values, int row) {
	return node_set_row(bst->head, A, values, row, 0);
}

/*
 * Utility function to the values and columns of a row from A 
 * Input:
 *   Node - Current node in recursive call
 *   A - Matrix to set values/columns of
 *   values - array of calculated values from sparse_mult_sparse
 *   row - which row to set in A
 *   count - total nnz so far in the calculation
 * Output:
 *   number of nnz after calculating this node
 */
int node_set_row(Node * node, Matrix * A, double * values, int row, int count) {
	if (!node)
		return count;

	count = node_set_row(node->left, A, values, row, count);

	if (values[node->value] != 0) {
		int k = A->row_ptr[row] + count;
		A->col_index[k] = node->value;
		A->values[k] = values[node->value];
		values[node->value] = 0;
		++count;
	}

	count = node_set_row(node->right, A, values, row, count);

	return count;
}

/*
 * Creates an adjacency matrix
 *
 * Input:
 *  mat - matrix to create from
 *
 * Output:
 *  Matrix containing the adjacency information
 */
Matrix * get_adj(Matrix * mat) {
	Matrix * result;
	int count = 0;
	int k;


	result = create_matrix(mat->rows, mat->cols, mat->nnz);
	if(result->rows == 1)
		return result;

	// Walks through all nnz and checks if off the diagonal.
	for (int i = 0; i < mat->rows; ++i) {
		result->row_ptr[i] = count;
		k = mat->row_ptr[i];
		while(k < mat->row_ptr[i+1]) {
			if (mat->col_index[k] != i && mat->values[k] !=0) {
				//result->values[count] = 1;
				result->values[count] = mat->values[k];
				result->col_index[count] = mat->col_index[k];
				count++;
			}
			++k;
		}
	}

	result->nnz = count;
	result->row_ptr[result->rows] = count;

	return result;
}

/*
 * Creates an boolean sparsity pattern matrix
 *
 * Input:
 *  mat - matrix to create from
 *
 * Output:
 *  Matrix containing the pattern information
 */
Matrix * get_pattern_bool(Matrix * mat) {
	Matrix * result;
	int count = 0;
	int k;


	result = create_matrix(mat->rows, mat->cols, mat->nnz);
	if(result->rows == 1)
		return result;

	// Walks through all nnz and checks if off the diagonal.
	for (int i = 0; i < mat->rows; ++i) {
		result->row_ptr[i] = count;
		k = mat->row_ptr[i];
		while(k < mat->row_ptr[i+1]) {
			if (mat->values[k] !=0) {
				result->values[count] = 1;
				result->col_index[count] = mat->col_index[k];
				count++;
			}
			++k;
		}
	}

	result->nnz = count;
	result->row_ptr[result->rows] = count;

	return result;
}

/*
 * Prints information about the input matrix.
 *
 * Input:
 *  mat - matrix from which to display information about.
 */
void print_data(Matrix * mat)
{
	for (int i = 0; i < mat->rows; ++i) {
		int k = mat->row_ptr[i];
		while ( k < mat->row_ptr[i+1]){
			printf("%d\t%d\t%f\n", i, mat->col_index[k], mat->values[k]);
			++k;
		}
	}
}

/* 
 * Prints Dense Matrix
 *
 * Input: 
 *   matrix - dense matrix in single array
 *   rows - rows in matrix
 *   cols - columns in matrix
 */
void print_dense(double * matrix, int rows, int cols) {
	for (int i = 0; i < rows; ++i) {
		for (int j = 0; j < cols; ++j) 
			printf("%.2f\t", matrix[i*cols+j]);
		printf("\n");
	}
	printf("\n");

}

/*
 * Prints the full dense, 2d representation of the matrix.
 *
 * Input:
 *  mat - matrix to display
 */
void pretty(Matrix * mat) 
{
	double * dense = create_full_dense(mat);
	print_dense(dense, mat->rows, mat->cols);
	/*
	   int count = 0;
	   for (int i = 0; i < mat->rows; ++i) {
	   for (int j = 0; j < mat->cols; ++j) {
	   if (count < mat->row_ptr[i+1] && 
	   j == mat->col_index[count]) {
	//printf("%.3f\t", mat->values[count]);
	if (mat->values[count] == 0)
	//printf("0.\t");
	printf("0.  ");
	else 
	printf("%.1f ", mat->values[count]);
	++count;
	}
	else {
	//printf("0\t");
	printf("0  ");
	}
	}
	printf("\n");
	}
	*/
}

/*
 * Prints results in a pleasant manner.
 * Input:
 *  result - struct holding result information
 *  options - holds options array that specifies what to display
 *
 */
void print_result(Result  * result, int * options, int size, char * filename) {
	if (!result || !options) 
		return;

	if (result->flag == 0) {
		printf("Time:\t\t%.9f seconds \n", result->total_time);
		printf("Count:\t\t%d iterations\n", result->iter);
		printf("Residual:\t%g\n", result->relres);
	}
	else if (result->flag == 1) {
		printf("System Inconsistent.\n");
	}

	if (options[VERBOSE])  {
		if (result->residuals) {
			printf("residuals:\n");
			print_vector(result->residuals, result->iter);
		}
		if (result->x) {
			printf("x:\n");
			print_vector(result->x, size);
		}

	}
}

/*
 * Fills vector with random digits
 *
 * Input:
 *   vect - vector to fill
 *   size - size of vector
 */
void fill_rand(double * vect, int size) 
{
	srand(time(NULL));
	for (int i = 0; i < size; ++i) {
		vect[i] = rand() % 10;
	}
}

/* Copies another matrix's size and creates new matrix
 *
 * Input:
 *   mat - original matrix
 *   nnz - nnz of new matrix
 */
Matrix * copy_info(Matrix * mat, int nnz) 
{
	if (!mat ) {
		printf("Copying NULL matrix!");
		exit(0);
	}

	Matrix * other;

	other = malloc(sizeof(Matrix));
	other->rows = mat->rows;
	other->cols = mat->cols;
	other->nnz = nnz;

	return other;
}

/*
 * Creates a new matrix
 *
 * Input:
 *   rows - rows in new matrix
 *   cols - columns in new matrix
 *   nnz - nnz of new matrix
 *
 * Output:
 *   The newly created matrix
 */
Matrix * create_matrix(int rows, int cols, int nnz) 
{
	Matrix * mat;

	mat = malloc(sizeof(Matrix));

	mat->rows = rows; 
	mat->cols = cols;
	mat->nnz = nnz;

	init_matrix(mat);

	mat->row_ptr[mat->rows] = nnz;

	return mat;
}

/*
 * Creates an identity matrix
 *
 * Input:
 *   rows - number of rows in new matrix
 *
 * Output:
 *   Identity matrix of size rows
 */
Matrix * create_identity(int rows) 
{
	Matrix * mat;

	mat = create_matrix(rows, rows, rows);

	for (int i = 0; i < mat->rows; ++i) {
		mat->values[i] = 1;
		mat->col_index[i] = i;
		mat->row_ptr[i] = i;
	}

	return mat;
}

/*
 * Creates a Result
 *
 * Input:
 *   x - solution vector
 *   residuals - array of residuals at each iteration
 *   size - length of x
 *   count - number of iterations performed
 *   flag - flags set during iterations
 *   relres - residual at the end of iterations
 *   time - total time to perform iterations
 *
 * Output:
 *   Result holding all the information 
 */
Result * create_result(double * x, double * residuals, int size, int count, int flag, 
		double relres, double time) 
{
	Result * result = malloc(sizeof(Result));

	result->x = malloc(sizeof(double) * size);
	memmove(result->x, x, size * sizeof(double));

	if (residuals) {
		result->residuals = malloc(sizeof(double) * count);
		memmove(result->residuals, residuals, count * sizeof(double));
	}
	else 
		result->residuals = NULL;

	result->iter = count;
	result->flag = flag;
	result->relres = relres;
	result->total_time = time;

	return result;
}

/* 
 * Initializes matrix.
 *
 * Input:
 *   mat - Matrix to initialize
 */
void init_matrix(Matrix * mat) 
{
	mat->values = calloc(mat->nnz, sizeof(double));
	mat->col_index = calloc(mat->nnz, sizeof(int));
	mat->row_ptr = calloc((mat->rows + 1), sizeof(int));

	mat->row_ptr[mat->rows] = mat->nnz;
	if (!mat->values || !mat->col_index || !mat->row_ptr) {
		printf("Error creating matrix of size %d x %d\n", mat->rows, mat->cols);
		exit(0);
	}
}

/* 
 * Destroys matrix.
 *
 * Input:
 *   mat - Matrix to destroy
 */
void destroy_matrix(Matrix *mat) 
{
	if (!mat)
		return;
	if (mat->values) free(mat->values);
	if (mat->col_index) free(mat->col_index);
	if (mat->row_ptr) free(mat->row_ptr);
	free(mat);
}

/* 
 * Destroys result.
 *
 * Input:
 *   result - Result to destroy
 */
void destroy_result(Result * result) 
{
	if (!result)
		return;
	if (result->x)
		free(result->x);
	if (result->residuals)
		free(result->residuals);

	result->x = NULL;
	result->residuals = NULL;

	free(result);
}

/*
 * Compares two vectors upto a specified tolerance.
 *
 * Input:
 *   vect1 - vector to compare
 *   vect2 - vector to compare
 *   size - length to compare
 *   tol - tolerance of comparison
 *
 * Output:
 *   number of differences
 */
int compare_vector(double * vect1, double * vect2, int size, double tol) {
	int count = 0;
	for (int i = 0; i < size; ++i) {
		if (!(fabs(vect1[i] - vect2[i] < tol))) {
			++count;
		}
	}

	return count;
}

/*
 * Creates a dense matrix out of an entire sparse matrix
 *
 * Input:
 *   mat - Sparse matrix
 *
 * Output:
 *   dense matrix representation of the input matrix
 */
double * create_full_dense(Matrix * mat) {
	int cols = mat->cols;
	int rows = mat->rows;
	int k;

	double * result = calloc(rows*cols, sizeof(double));

	for (int i = 0; i < rows; ++i) {
		k = mat->row_ptr[i];
		while(k < mat->row_ptr[i + 1]) {
			result[i*cols + mat->col_index[k]] = mat->values[k];
			++k;
		}
	}

	return result;
}

/*
 * Creates a dense submatrix out of a sparse matrix
 *
 * Input:
 *   mat - Sparse matrix
 *   start - first row of submatrix
 *   end - last row of submatrix
 *
 * Output:
 *   dense matrix representation of the input submatrix
 */
double * create_dense(Matrix * mat, int start, int end) { 
	double * result;
	int size;
	size = end - start + 1;
	int k;

	result = calloc(size*size, sizeof(double));

	for (int i = 0; i < size; ++i) {
		k = mat->row_ptr[i + start];
		while(k < mat->row_ptr[i+start + 1]) {
			if (mat->col_index[k] >= start && mat->col_index[k] <= end) {
				result[i*size + mat->col_index[k] - start] = mat->values[k];
			}
			++k;
		}
	}

	return result;
}

/*
 * Verifies a solution
 * Checks if Ax = b
 *
 * Input:
 *   mat - matrix A
 *   result - holds solution x
 *   rhs - right hand side/ b
 */
void verify_solution(Matrix * mat, Result * result, double * rhs) {
	double * x = calloc(mat->cols, sizeof(double));

	sparse_mult_vector(mat, result->x, x);

	double tolerance = 0.001;
	int comp = compare_vector(x, rhs, mat->rows, tolerance);

	printf("Comp result: %d different within %g tolerance\n", comp, tolerance);

	free(x);
}
