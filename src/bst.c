#include "bst.h"

BST * bst_create() {
	BST * bst = malloc(sizeof(BST));
	bst->head = NULL;
	bst->count = 0;
	return bst;
}

void bst_destroy(BST * bst) {
	node_destroy(&(bst->head));
	free(bst);
	bst = NULL;
}

void bst_insert(BST * bst, int num) {
	bst->count += node_insert(&(bst->head), num);
}

void bst_print(BST * bst) {
	printf("%d nodes:\n", bst->count);
	node_print(bst->head);
}


void node_destroy(Node ** node) {
	if (!*node)
		return;
	node_destroy(&(*node)->left);
	node_destroy(&(*node)->right);
	free(*node);
	*node = NULL;
}

Node * node_create(int num) {
	Node * node = malloc(sizeof(Node));
	node->value = num;
	node->left = NULL;
	node->right = NULL;
	return node;
}

int node_insert(Node ** node, int num) {
	if (!*node) {
		*node = node_create(num);
		return 1;
	}
	if (num < (*node)->value) {
		if (!(*node)->left) {
			(*node)->left = node_create(num);
			return 1;
		}
		else 
			return node_insert(&(*node)->left, num);
	}
	else if(num > (*node)->value) {
		if (!(*node)->right) {
			(*node)->right = node_create(num);
			return 1;
		}
		else 
			return node_insert(&(*node)->right, num);
	}
	return 0;
}

void node_print(Node * node) {
	if (!node)
		return;
	node_print(node->left);
	printf("%d\n",node->value);
	node_print(node->right);
}
