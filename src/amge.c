/* AMG.c
 * Author: Stephan Gelever
 * Date: April, 2016
 *
 */

#include "amge.h"
#include<cblas.h>

#include "bst.h"

#define START_FLAGS 10

#define SMOOTH_WEIGHT 4.0/3.0
#define COARSE_SIZE 1
#define FIRST_COARSE_FACTOR_SCALE 1

// v-cycle = 1, w-cycle = 2, etc
#define CYCLE_TYPE 2

/*
 * Driver for running AMGe algorithm. 
 *
 * Input:
 *  A - Matrix
 *  b - rhs vector
 *  e - residual tolerance
 *  maxit - maximum number of iterations before restart.
 *
 *  Flags:
 *	-v - Verbose output
 *	-g - Generate a rhs matrix and save into rhs file
 *	-p - Print the input matrix
 *	-f - Set input file format : entries (default), adj, adji, hb
 *		entries - list of pairs of matrix entries 
 *		adj - adjacency list 
 *		adji - adjacency list with matrix information in first two lines.
 *		       matrix size in first line, nnz in second line
 *		hb - Harwell Boeing format
 *	-o - Output file name
 */
int main(int argc, char *argv[]) {
	if (argc < START_FLAGS || argc > 16)
		show_help();

	int * options = parse_cmd(argc, argv, START_FLAGS);

	Matrix * mat = parse_file(argv[1], options[FORMAT]);
	
	double e = atof(argv[3]);
	int maxit = atoi(argv[4]);
	double theta = atof(argv[5]);
	int agg_size = atoi(argv[6]);
	int levels = atoi(argv[7]);
	int smooth_steps = atoi(argv[8]);
	int p_smooth_degrees = atoi(argv[9]);


	/*
	 * Matrix multiplicatio testing timing
	struct timeval start_time, end_time;
	gettimeofday(&start_time, NULL);
	Matrix * A = sparse_mult_sparse(mat, mat);
	gettimeofday(&end_time, NULL);

	double total_time = (end_time.tv_sec - start_time.tv_sec) + 
		(end_time.tv_usec - start_time.tv_usec)/1000000.0f;
	printf("bst in:\t%g seconds\n", total_time);

	gettimeofday(&start_time, NULL);
	Matrix * B = sparse_mult_sparse_order(mat, mat);
	gettimeofday(&end_time, NULL);
	total_time = (end_time.tv_sec - start_time.tv_sec) + 
		(end_time.tv_usec - start_time.tv_usec)/1000000.0f;
	printf("not bst in:\t%g seconds\n", total_time);

	gettimeofday(&start_time, NULL);
	Matrix * C = sparse_mult_sparse_new(mat, mat);
	gettimeofday(&end_time, NULL);
	total_time = (end_time.tv_sec - start_time.tv_sec) + 
		(end_time.tv_usec - start_time.tv_usec)/1000000.0f;
	printf("right in:\t%g seconds\n", total_time);

	double * a = create_full_dense(A);
	double * b = create_full_dense(B);
	double * c = create_full_dense(C);
	int comps = compare_vector(a, b, mat->rows * mat->cols, .001);
	int compslow = compare_vector(a, c, mat->rows * mat->cols, .001);
	printf("comps:%d\n", comps);
	printf("comps:%d\n", compslow);
	*/

	

	print_input(e, maxit, theta, agg_size, levels, smooth_steps, p_smooth_degrees);

	if (options[PRINT]) 
		pretty(mat);

	double * rhs = malloc(mat->cols * sizeof(double));
	read_rhs(rhs, argv[2], mat->cols, options);

	/*
	// Scale
	Matrix * m_half_inv = create_m_inv(mat);
	invert_diag(m_half_inv);
	sqrt_diag(m_half_inv);

	Matrix * temp = sparse_mult_sparse(mat, m_half_inv);
	Matrix * A_hat = sparse_mult_sparse(m_half_inv, temp);

	double * b_hat = calloc(A_hat->rows, sizeof(double));
	sparse_mult_vector(m_half_inv, rhs, b_hat);

	Solver * solver = setup_solver(A_hat, levels, COARSE_SIZE, agg_size, theta, p_smooth_degrees);
	print_solver(solver);
	result = run_solver_recursive(solver, b_hat, e, maxit, options[VERBOSE], smooth_steps);
	double * x = calloc(mat->cols, sizeof(double));

	sparse_mult_vector(m_half_inv, result->x, result->x);
	print_result(result, options, 
			mat->rows, 
			argv[options[OUTPUT]]);

	// Check if solution is correct
	// Calculate Ax, compare to input rhs
	memset(x, 0, mat->cols*sizeof(double));
	sparse_mult_vector(mat, result->x, x);

	double tol = .001;
	options[GENERATE] = 0;
	read_rhs(rhs, argv[2], mat->cols, options);
	int comp = compare_vector(x, rhs, mat->rows, tol);
	printf("Comp result: %d different within %g tolerance\n", comp, tol);


	free(x);
	destroy_solver(solver);
	destroy_result(result);
	free(options);

	//destroy_matrix(mat);
	//free(rhs);
	//free(b_hat);
	destroy_matrix(temp);
	//destroy_matrix(m_half);
	destroy_matrix(m_half_inv);
	*/
	

	//Unscaled:
	Solver * solver = setup_solver(mat, levels, COARSE_SIZE, agg_size, theta, p_smooth_degrees);
	
	print_solver(solver);

	//Unscaled:
	double * x = calloc(mat->cols, sizeof(double));
	//Result * result2 = mgpcg(mat, solver, rhs, x, e, maxit, options[VERBOSE]);

	Result * result = run_solver_recursive(solver, rhs, e, maxit, options[VERBOSE], smooth_steps);
	

	print_result(result, options, 
			mat->rows, 
			argv[options[OUTPUT]]);

	/*
	print_result(result2, options, 
			mat->rows, 
			argv[options[OUTPUT]]);
			*/

	verify_solution(mat, result, rhs);
	

	if (options[OUTPUT])
		write_result(result, argv[options[OUTPUT]], mat->rows);

	free(x);
	free(rhs);
	destroy_solver(solver);
	destroy_result(result);
	free(options);


	exit(0);
}


/*
 * Sets up solver hierarchy
 *
 * Input:
 *  A - Matrix
 *  num_levels - maximum number of levels of hierarchy
 *  coarse_size - minimum size of coarsest level
 *  agg_size - size of each agglomerate
 *  num_eig - number of eigenvectors to include in P
 *  p_smooth - number of degrees to smooth P
 *
 * Output:
 *  Solver - the complete solver hierarchy.
 *
 *  TODO: Convert to do-while loop?
 */
Solver * setup_solver(Matrix * A, int num_levels, int coarse_size, int agg_size, double theta, int p_smooth) {
	Solver * solver = malloc(sizeof(Solver));
	Level * current = malloc(sizeof(Level));
	Level * next;
	Matrix * temp;

	//Timing
	struct timeval start_time, end_time;
	gettimeofday(&start_time, NULL);

	solver->levels = current;
	solver->num_levels = 0;

	// Set first level
	current->A = A;
	current->M = create_m_inv(A);
	current->PT = create_restriction_agg_edge(A, agg_size*FIRST_COARSE_FACTOR_SCALE , theta);


	if (current->PT == NULL) {
		printf("Invalid Restriction Matrix, probably too many eigenvectors set!\n");
		exit(1);
	}


	current->P = transpose(current->PT);

	if (p_smooth) 
		smooth_prolongator(A, current->M, &current->P, &current->PT, p_smooth);

	current->b = calloc(A->rows, sizeof(double));
	current->x = calloc(A->rows, sizeof(double));
	current->next_level = NULL;
	current->prev_level = NULL;
	current->Adense = NULL;
	current->ipiv = NULL;

	solver->num_levels++;

	int p_size = current->PT == NULL ? 1 : 0;

	// Set coarse levels
	while (solver->num_levels < num_levels && !p_size &&
			current->P->cols >= coarse_size && current->P->cols < current->A->rows) {
		next = current->next_level;
		next = malloc(sizeof(Level));

		temp = sparse_mult_sparse(current->A, current->P);
		next->A = sparse_mult_sparse(current->PT, temp);

		next->M = create_m_inv(next->A);
		next->PT = create_restriction_agg_edge(next->A, agg_size, theta);
		if (next->PT == NULL || next->PT->rows <= coarse_size || next->PT->rows >= next->A->rows) {
			p_size = 1;
			next->P = NULL;
		}
		else {
			next->P = transpose(next->PT);
			if (p_smooth) 
				smooth_prolongator(next->A, next->M, &next->P, &next->PT, p_smooth);
		}

		next->x = calloc(next->A->rows, sizeof(double));
		next->b = malloc(next->A->rows * sizeof(double));
		next->prev_level = current;
		next->next_level = NULL;
		next->Adense = NULL;
		next->ipiv = NULL;

		current->next_level = next;
		current = next;

		solver->num_levels++;

		destroy_matrix(temp);
	}

	if (solver->num_levels < 2) { 
		printf("Only 1 level!\n");
		exit(0);
	}

	current->Adense = create_full_dense(current->A);
	current->ipiv = calloc(current->A->rows, sizeof(int));


	/*
	//Calculate pinv(A)
	int size = current->A->rows;
	double * adense = malloc(size*size*sizeof(double));
	int n = size, m = size, lda = n, ldu = m, ldvt = n, infosvd;
	double superb[n-1];
	double s[n], u[ldu*m], vt[ldvt*n];



	// Compute SVD
	infosvd = LAPACKE_dgesvd( LAPACK_ROW_MAJOR, 'A', 'A', m, n, adense, lda,
	s, u, ldu, vt, ldvt, superb );

	if( infosvd > 0 ) {
	printf( "The algorithm computing SVD failed to converge.\n" );
	exit( 1 );
	}
	double cutoff = s[0] * .00000001;

	// Calc A+ = VEU*
	double * A_pinv = malloc(size*size* sizeof(double));
	double * c = malloc(size*size* sizeof(double));
	double * svalues = calloc(size*size, sizeof(double));
	for (int i = 0; i < size; ++i) {
	if (s[i] > cutoff)
	svalues[i*size + i ] = 1/s[i];

	//s[i] = 1/s[i];
	}
	cblas_dgemm(CblasRowMajor, CblasNoTrans, CblasTrans,m,n,size,1.0,
	svalues, size, u, size, 0, c, size);

	cblas_dgemm(CblasRowMajor, CblasTrans, CblasNoTrans,m,n,size,1.0,
	vt, size, c, size, 0, A_pinv, size);
	print_dense(A_pinv, size,size);
	print_dense(current->Adense, size,size);

	//TODO: Memory manage this mess
	current->Adense=A_pinv;
*/
	// Factor the coarsest level to make it faster to solve
	int info = LAPACKE_dgetrf(LAPACK_ROW_MAJOR, 
	current->A->rows, 
	current->A->cols, 
	current->Adense, 
	current->A->rows, 
	current->ipiv);
	if (info > 0)
		printf("Error factoring A Dense\n");

	//Timing
	gettimeofday(&end_time, NULL);

	double total_time = (end_time.tv_sec - start_time.tv_sec) + 
		(end_time.tv_usec - start_time.tv_usec)/1000000.0f;
	printf("Setup in:\t%g seconds\n", total_time);

	return solver;
}



/*
 * Runs AMGe with the supplied solver hierarchy.
 *
 * Input:
 *  solver - solver hierarchy
 *  rhs - right hand side to solve for
 *  tol - tolerance of residual
 *  maxit - maximum number of iterations
 *  verbose - display additional information
 *  smooth_steps - number of smoothing steps 
 *
 * Output:
 *  result - the result struct holding result information
 */
Result * run_solver(Solver * solver, double * rhs, double tol, int maxit, int verbose, int smooth_steps) {
	if (!solver) {
		printf("Invalid solver to run!\n");
		exit(0);
	}
	double * buffer = malloc(solver->levels->A->cols * sizeof(double));
	double * residuals = malloc(maxit * sizeof(double));

	struct timeval start_time, end_time;
	double total_time = 0;


	// Just a huge number to make start w/ TODO: make better start bound
	double res = 999999999999;
	int iteration = 0;

	Level * current;
	memmove(solver->levels->b, rhs, solver->levels->A->rows * sizeof(double));

	gettimeofday(&start_time, NULL);

	// Start of v-cycle
	while(iteration < maxit && res > tol && !isinf(res)) {
		current = solver->levels;

		//TODO: make this do-while loop?
		// Move down v-cycle
		for(int i = 0; i < solver->num_levels; ++i) {
			memset(buffer, 0, sizeof(double)*current->A->cols);

			// Presmooth
			level_smooth(current, SMOOTH_WEIGHT, smooth_steps);

			// Calculate b coarse
			if ( i != solver->num_levels-1) {
				sparse_mult_vector(current->A, current->x, buffer);
				sub_vector(current->b, buffer, buffer, current->A->rows);
				sparse_mult_vector(current->PT, buffer, current->next_level->b);
				current = current->next_level;
			}

		}

		//Solve coarse level:
		solve_factored(current->Adense, current->x, current->b, current->A->rows, current->ipiv);

		current = current->prev_level;


		// Move up v-cycle
		for(int i = 0; i < solver->num_levels - 1; ++i) {
			memset(buffer, 0, sizeof(double)*current->A->cols);

			// Coarse grid correction
			sparse_mult_vector(current->P, current->next_level->x, buffer);
			add_vector(current->x, buffer, current->x, current->A->rows);

			// Post smooth
			level_smooth(current, SMOOTH_WEIGHT, smooth_steps);

			current = current->prev_level;
		}

		memset(buffer, 0, sizeof(double)*solver->levels->A->cols);

		// Residual
		sparse_mult_vector(solver->levels->A, solver->levels->x, buffer);
		sub_vector(solver->levels->b,buffer, buffer, solver->levels->A->rows);
		res = norm(buffer, solver->levels->A->rows);
		residuals[iteration] = res;
#ifdef DEBUG
		printf("res: %f\n",res);
#endif

		/*
		   double avg = calc_avg(solver->levels->x, solver->levels->A->rows);
		   memset(buffer, avg, solver->levels->A->rows * sizeof(double));

		   add_vector(solver->levels->x, buffer, solver->levels->x, solver->levels->A->rows);
		   */

		++iteration;

	}
	gettimeofday(&end_time, NULL);

	total_time = (end_time.tv_sec - start_time.tv_sec) + (end_time.tv_usec - start_time.tv_usec)/1000000.0f;

	// Create return result
	Result * result = create_result(solver->levels->x, residuals,
			solver->levels->A->rows, 
			iteration, 0 , res, total_time);

	free(buffer);
	free(residuals);

	return result;
}

/*
 * Runs AMGe with the supplied solver hierarchy.
 *
 * Input:
 *  solver - solver hierarchy
 *  rhs - right hand side to solve for
 *  tol - tolerance of residual
 *  maxit - maximum number of iterations
 *  verbose - display additional information
 *  smooth_steps - number of smoothing steps 
 *
 * Output:
 *  result - the result struct holding result information
 */
Result * run_solver_recursive(Solver * solver, double * rhs, double tol, int maxit, int verbose, int smooth_steps) {
	if (!solver) {
		printf("Invalid solver to run!\n");
		exit(0);
	}
	double * buffer = malloc(solver->levels->A->cols * sizeof(double));
	double * buffer2 = malloc(solver->levels->A->cols * sizeof(double));
	double * residuals = malloc(maxit * sizeof(double));

	struct timeval start_time, end_time;
	double total_time = 0;


	// Just a huge number to make start w/ TODO: make better start bound
	double res = 999999999999;
	int iteration = 0;

	memmove(solver->levels->b, rhs, solver->levels->A->rows * sizeof(double));

	int cycle = CYCLE_TYPE;

	gettimeofday(&start_time, NULL);

	// Start of cycle
	while(iteration < maxit && res > tol && !isinf(res)) {
		// Solve Level
		solve_level(solver->levels, smooth_steps, cycle);

		// Calculate Residual:  b-Ax
		memset(buffer, 0, sizeof(double)*solver->levels->A->cols);

		sparse_mult_vector(solver->levels->A, solver->levels->x, buffer);
		sub_vector(solver->levels->b,buffer, buffer, solver->levels->A->rows);

		res = norm(buffer, solver->levels->A->rows);
		residuals[iteration] = res;

#ifdef DEBUG
		//printf("res %d: %f\n", iteration, res);
#endif
		   double avg = calc_avg(solver->levels->x, solver->levels->A->rows);
		   for (int i = 0; i < solver->levels->A->rows; ++i) 
		   buffer2[i] = avg;
		   sub_vector(solver->levels->x, buffer2, solver->levels->x, solver->levels->A->rows);

		++iteration;
	}
	gettimeofday(&end_time, NULL);

	total_time = (end_time.tv_sec - start_time.tv_sec) + (end_time.tv_usec - start_time.tv_usec)/1000000.0f;

	// Create return result
	Result * result = create_result(solver->levels->x, residuals,
			solver->levels->A->rows, 
			iteration, 0 , res, total_time);

	free(buffer);
	free(buffer2);
	free(residuals);

	return result;
}

void solve_level(Level * level, int smooth_steps, int cycle) {
	if (level->Adense != NULL) {
		solve_factored(level->Adense, level->x, level->b, level->A->rows, level->ipiv);
		//solve_coarse_level(level);
		//solve_pinv(level);
		return;
	}

	double * buffer = calloc(level->A->cols, sizeof(double));

	for (int i = 0; i < cycle; ++i) {
		// Presmooth
		level_smooth(level, SMOOTH_WEIGHT, smooth_steps);

		sparse_mult_vector(level->A, level->x, buffer);
		sub_vector(level->b, buffer, buffer, level->A->rows);
		sparse_mult_vector(level->PT, buffer, level->next_level->b);

		//Solve coarse level:
		solve_level(level->next_level, smooth_steps, cycle);

		memset(buffer, 0, level->A->cols *sizeof(double));

		// Coarse grid correction
		sparse_mult_vector(level->P, level->next_level->x, buffer);
		add_vector(level->x, buffer, level->x, level->A->rows);

		// Post smooth
		level_smooth(level, SMOOTH_WEIGHT, smooth_steps);
	}

	free(buffer);
}

/*
 * Smooths the prolongation operator P
 * Calculates (I-MA)P for the input number of degrees
 *
 * Input:
 *  A - current level matrix
 *  M - preconditioner for A
 *  P - matrix P to transpose
 *  degrees - number of times to smooth
 *
 */

void smooth_prolongator(Matrix * A, Matrix * M, Matrix ** P, Matrix ** PT, int degrees) {
	Matrix * MA = sparse_mult_sparse(M, A);
	Matrix * op = i_minus_spd(MA, 1.0);
	Matrix * temp;
	Matrix * smooth;

	for (int i = 0; i < degrees; ++i) {
		smooth = sparse_mult_sparse(op, *P);

		temp = *P;
		*P = smooth;
		destroy_matrix(temp);

	}

	temp = *PT;
	*PT = transpose(*P);
	destroy_matrix(temp);

	destroy_matrix(op);
	destroy_matrix(MA);
}


/*
 * Converts the sparse matrix A to dense and solves for b
 *
 * Input: 
 *  A - current level matrix
 *  x - will hold solution
 *  b - right hand side to solve for
 * Output:
 *  1 on success
 */
int solve(Matrix * A, double * x, double * b) {
	double * dense;
	if (A->rows == 1) {
		x[0] = b[0]/A->values[0];
		return 1;
	}

	dense = create_dense(A, 0, A->cols-1);
	int value = solve_dense(dense, x, b, A->rows);
	free(dense);
	return value;
}

/*
 * Solves a dense matrix for b
 *
 * Input: 
 *  A - dense matrix 
 *  x - will hold solution
 *  b - right hand side to solve for
 *  size - size of A
 *
 * Output:
 *  1 on success
 */

int solve_dense(double * A, double * x, double * b, int size) {
	int info;
	int ipiv[size];

	info = LAPACKE_dgesv( LAPACK_ROW_MAJOR, 
			size, 1, A, size, ipiv,
			b, 1 );


	if(info != 0 ){
		printf("Error solving system! %d\n", info);
		return 0;
	}

	// ASUUME A SQUARE TODO: CHECK
	memmove(x, b, sizeof(double)*size);

	return 1;
}

/*
 * Solves a dense LU factored matrix for b
 *
 * Input: 
 *  A - dense matrix 
 *  x - will hold solution
 *  b - right hand side to solve for
 *  size - size of A
 *  ipiv - pivot indices
 *
 * Output:
 *  1 on success
 */

int solve_factored(double * A, double * x, double * b, int size, int * ipiv) {
	int info = LAPACKE_dgetrs(LAPACK_ROW_MAJOR, 'N', size, 1, A, size, ipiv, b, 1);


	if(info != 0 ){
		printf("Error solving system! %d\n", info);
		return 0;
	}

	// ASUUME A SQUARE TODO: CHECK
	memmove(x, b, sizeof(double)*size);

	return 1;
}

int solve_coarse_level(Level * level) {
	int size = level->A->rows;
	double * b_copy = malloc(size*size*sizeof(double));
	memmove(b_copy, level->b, sizeof(double)*size);

	int rank = 0;
	double singular_values[size];
	int info = LAPACKE_dgelsd(LAPACK_ROW_MAJOR, size, size, 1, level->Adense, size, b_copy, 1, singular_values, -1, &rank);


	if(info != 0 ){
		printf("Error solving coarse level! %d\n", info);
		return 0;
	}

	// ASUUME A SQUARE TODO: CHECK
	memmove(level->x, b_copy, sizeof(double)*size);

	free(b_copy);
	return 1;

}

int solve_pinv(Level * level) {
	int size = level->A->rows;
	double * b_copy = malloc(size*size*sizeof(double));
	memmove(b_copy, level->b, sizeof(double)*size);

	/*
	cblas_dgemv(CblasRowMajor, CblasNoTrans,size,size,1.0,
			level->Adense, size, level->b, 1, 0,
			level->x, 1);
			*/

	// ASUUME A SQUARE TODO: CHECK
	//memmove(level->x, b_copy, sizeof(double)*size);

	free(b_copy);
	return 1;

}


double calc_avg(double * x, int size) {
	double total = 0;
	for (int i = 0; i < size; ++i) {
		total += x[i];
	}
	return total/size;
}


/*
 * Prints information about the solver to the user
 *
 * Input: 
 *  solver - solver hierarchy
 */
void print_solver(Solver * solver) {
	printf("\nNumber of Levels: %d\n", solver->num_levels);
	Level * current = solver->levels;

	int count = 0;
	double opcomp = 0;
	double grid_comp = 0;
	double density = 0;

	printf("\tlevel\tdofs\tnnz\tdensity\t\n");
	while (current != NULL) {
		density = ((double)current->A->nnz) / (current->A->rows * current->A->cols);
		printf("\t%d\t%d\t%d\t%.3f%%\n", count, current->A->cols, current->A->nnz,
				density*100);
		opcomp += current->A->nnz;
		grid_comp += current->A->rows;
		count++;
		current = current->next_level;
	}

	opcomp = opcomp/solver->levels->A->nnz;
	grid_comp = grid_comp/solver->levels->A->rows;

	printf("Grid Complexity:\t%.3f\n", grid_comp);
	printf("Operator Complexity:\t%.3f\n", opcomp);
	printf("\n");
}


// mgpcg
Result * mgpcg(Matrix * A, Solver * solver, 
		double * b, double * x, double e, 
		int maxit, int verbose) 
{

	if (!A || !x|| !solver || !b || A->rows != A->cols) {
		printf("Invalid Input to PCG!");
		return NULL;
	}

	Result * resultmg;

	int found = 0;
	int count = 0;
	int flag = 0;
	double delta_0 = 0;
	double delta = 0;
	double delta_old = 0;

	double alpha = 0;
	int m;

	struct timeval start_time, end_time;

	e = e*e;

	double gamma;
	double *g = malloc(sizeof(double) * A->rows);
	//double *x = malloc(sizeof(double) * A->rows);
	double *r = malloc(sizeof(double) * A->rows);
	double *pre_r = malloc(sizeof(double) * A->rows);
	double *p = malloc(sizeof(double) * A->rows);
	double *buffer = malloc(sizeof(double) * A->rows);
	double *buffer2 = malloc(sizeof(double) * A->rows);

	if (!r || !x || !pre_r || !p || !buffer || !buffer2) {
		printf("Error allocating memory!");
		flag = 1;
	}


	gettimeofday(&start_time, NULL);

	//Compute initial approximation of x
	//sparse_mult_vector(B_inv, b, x);
	//memset(x, 0, sizeof(double) * A->rows);

	// r0 = b-Ax
	sparse_mult_vector(A, x, buffer);
	sub_vector(b, buffer, r, A->rows);

	// Compute initial preconditioned residual
	//sparse_mult_vector(B_inv, r, pre_r);
	resultmg = run_solver_recursive(solver, r, .0000001, 1, 0, 2);
	memmove(pre_r, resultmg->x, A->rows*sizeof(double));

	destroy_result(resultmg);

	delta_0 = inner_product(r, pre_r, A->rows);

	// Set p
	memmove(p, pre_r, A->rows*sizeof(double));

	// Update tracking variables
	m = 0;
	delta_old = delta_0;

	while (m < maxit && m < A->rows && !flag && !found) {
		if (verbose) {
			printf("res:     %f \n", sqrt(delta/delta_0));
		}

		sparse_mult_vector(A, p, g);

		gamma = inner_product(p, g, A->rows);

		//if (gammas[m] < 0){
		//	printf("\n Gamma Negative!\n");
		//}

		alpha = delta_old/gamma;

		// Update iterate x:= x + ap_j
		scalar_mult_vector(p, buffer, A->rows, alpha);
		add_vector(x, buffer, x, A->rows);


		// Update residual r:= r - ag
		scalar_mult_vector(g, buffer, A->rows, alpha);
		sub_vector(r, buffer, r, A->rows);

		// Compute preconditioned residual
		//sparse_mult_vector(B_inv, r, pre_r);
		resultmg = run_solver_recursive(solver, r, .0000001, 3, 0, 2);
		memmove(pre_r, resultmg->x, A->rows*sizeof(double));
		destroy_result(resultmg);


		delta = inner_product(r, pre_r, A->rows);


		// Check stop condition
		if (delta < e * delta_0)
			found = 1;
		else {
			// Set p_m+1 = pre_r
			scalar_mult_vector(p, buffer, A->rows, delta/delta_old);
			add_vector(pre_r, buffer, p, A->rows);

			delta_old = delta;
		}
		m++;
		count++;
	}

	gettimeofday(&end_time, NULL);

	double total_time = (end_time.tv_sec - start_time.tv_sec) + 
		(end_time.tv_usec - start_time.tv_usec)/1000000.0f;


	// Setup return data.
	Result * result = create_result(x, NULL, A->rows, count, 0, sqrt(delta/delta_0), total_time);

	free(g);
	free(r);
	free(pre_r);
	free(p);
	free(buffer);
	free(buffer2);

	return result;
}




/*
 * Prints back input information to the user
 * Useful when archiving results 
 *
 * Input: 
 *  tol - tolerance of error
 *  maxit - maximum number of iterations
 *  num_eig - number of eigenvectors in each agglomerate
 *  agg - size of each agglomerate
 *  levels - number of levels in hierarchy
 *  smooth - pre/post smooth steps
 *  smooth_p - degrees to smooth P
 *
 */
void print_input(double tol, int maxit, double theta, int agg, int levels, int smooth, int smooth_p) {
	printf("\n");
	printf("Tolerance:\t\t%.g\n", tol);
	printf("Max Iterations:\t\t%d\n", maxit);
	printf("Spectral Tolerance:\t%g\n", theta);
	printf("Agglomerate Size:\t%d\n", agg);
	printf("Max Levels:\t\t%d\n", levels);
	printf("Pre/Post Steps:\t\t%d\n", smooth);
	printf("P Smooth Degrees:\t%d\n", smooth_p);
	printf("First Coarse Factor:\t%d\n", FIRST_COARSE_FACTOR_SCALE);
	printf("\n");
}



/*
 * Frees memory used by solver hierarchy
 *
 * Input: 
 *  solver - solver hierarchy
 *
 */
void destroy_solver(Solver * solver) {
	Level * temp;
	while(solver->levels != NULL) {
		temp = solver->levels->next_level;
		destroy_level(solver->levels);
		solver->levels = temp;
	}
	free(solver);

}

/*
 * Frees memory used by a solver level
 *
 * Input: 
 *  level - solver level
 *
 */
void destroy_level(Level * level) {
	destroy_matrix(level->A);
	destroy_matrix(level->M);
	destroy_matrix(level->P);
	destroy_matrix(level->PT);
	if (level->b != NULL) free(level->b);
	if (level->x != NULL) free(level->x);
	if (level->ipiv != NULL) free(level->ipiv);
	if (level->Adense != NULL) free(level->Adense);
	level->next_level = NULL;
	level->prev_level = NULL;

	level->b = 0;
	level->x = 0;
	level->ipiv = 0;
	level->Adense = 0;
	free(level);
}

/*
 * Displays usage information to the user.
 *
 */

void show_help(void) {
	printf("Valid Input: matrix rhs e m eig agg levels num_smooth P_smooth [-g] [-v] [-p] [-f option] [-o output file]\n");
	printf("             matrix\t- Input Matrix. File in matrices/ \n");
	printf("             rhs\t- Right hand side vector. File in matrices/ \n");
	printf("             e\t\t- Residual tolerance. \n");
	printf("             m\t\t- Max number of iterations\n");
	printf("             eig\t- Number of eigenvectors\n");
	printf("             agg\t- Agglomerate Size\n");
	printf("             levels\t- Max number of solver levels\n");
	printf("             num smooth\t- Number of post/pre smooth steps\n");
	printf("             P smooth\t- Number of P smooth degrees\n");
	printf("             [-v]\t- Verbose output, displays more information \n");
	printf("             [-g]\t- Generate the rhs matrix and save into rhs file\n");
	printf("             [-p]\t- Print the input matrix\n");
	printf("             [-f]\t- File format: entries(default), adj, adji, hb\n");
	printf("             [-o]\t- Output file name\n");
	exit(1);
}
