
#include "fpcg_drive.h"
#include "amge.h"

#define START_FLAGS 5
#define END_FLAGS 11

/*
 * Driver for running FPGC algorithm. 
 *
 * Input:
 *  A - Matrix
 *  b - rhs vector
 *  e - residual tolerance
 *  maxit - maximum number of iterations before restart.
 *
 *  Flags:
 *	-v - Verbose output
 *	-g - Generate a rhs matrix and save into rhs file
 *	-p - Print the input matrix
 *	-f - Set input file format : entries (default), adj, adji, hb
 *		entries - list of pairs of matrix entries 
 *		adj - adjacency list 
 *		adji - adjacency list with matrix information in first two lines.
 *		       matrix size in first line, nnz in second line
 *		hb - Harwell Boeing format
 *	-o - Output file name
 *
 */

int main(int argc, char *argv[]) {
	Matrix * mat;	// Matrix to work with.
	Matrix * B_inv;	// Preconditioner
	Result * result2; // Stores result of computation.
	Result * result; // Stores result of computation.

	int * options;		// Holds input flags
	double * rhs;		// Right hand side
	double * x0;

	if (argc < START_FLAGS || argc > END_FLAGS)
		show_help();

	options = parse_cmd(argc, argv, START_FLAGS);
	mat = parse_file(argv[1], options[FORMAT]);

	if (options[PRINT]) 
		pretty(mat);

	rhs = malloc(sizeof(double)*mat->cols);
	x0 = calloc(mat->cols, sizeof(double));
	read_rhs(rhs, argv[2], mat->cols, options);

	B_inv = copy_info(mat, mat->rows);
	init_matrix(B_inv);

	precondition(mat, B_inv);
	invert_diag(B_inv);

	//MANUAL SETS THE DIAG!!!! DELETE M8
	//for(int i = 0; i < B_inv->nnz; ++i)
		//B_inv->values[i] = 1.0;

	result = fpcg(  mat,		// Matrix to solve
			B_inv,		// Preconditioner 
			rhs,		// Right hand side
			atof(argv[3]),	// epsilon tolerance
			atoi(argv[4]),	// Max iterations
			options[VERBOSE]); //Verbose output

	result2 = pcg(  mat,		// Matrix to solve
			B_inv,		// Preconditioner 
			rhs,		// Right hand side
			x0,		// initial guess
			atof(argv[3]),	// epsilon tolerance
			atoi(argv[4]),	// Max iterations
			options[VERBOSE]); //Verbose output

	printf("FPCG:\n");
	print_result(result, options, mat->rows, argv[options[OUTPUT]]);
	printf("PCG:\n");
	print_result(result2, options, mat->rows, argv[options[OUTPUT]]);

	double * x = malloc(sizeof(double)*mat->cols);
	sparse_mult_vector(mat, result2->x, x);

	double tol = 1.0;
	read_rhs(rhs, argv[2], mat->cols, options);
	int comp = compare_vector(x, rhs, mat->rows, tol);
	printf("Comp result: %d different within %g tolerance\n", comp, tol);
	free(x);


	destroy_result(result);
	destroy_result(result2);
	destroy_matrix(mat);
	destroy_matrix(B_inv);
	free(rhs);
	free(options);

	exit(0);
}

/*
 * Displays usage information to the user.
 *
 */

void show_help(void) {
	printf("Valid Input: matrix rhs e m [-g] [-v] [-p] [-f option] [-o output file]\n");
	printf("             matrix\t- Input Matrix. File in matrices/ \n");
	printf("             rhs\t- Right hand side vector. File in matrices/ \n");
	printf("             e\t\t- Residual tolerance. \n");
	printf("             m\t\t- Number of iterations before restart \n");
	printf("             [-v]\t- Verbose output, displays more information \n");
	printf("             [-g]\t- Generate the rhs matrix and save into rhs file\n");
	printf("             [-p]\t- Print the input matrix\n");
	printf("             [-f]\t- File format: entries(default), adj, adji, hb\n");
	printf("             [-o]\t- Output file name\n");
	exit(1);
}

