#ifndef __MATRIX_H
#define __MATRIX_H 

#include <stdlib.h>
#include <time.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <lapacke.h>

#include "bst.h"

// Options Indices
#define VERBOSE 0
#define GENERATE 1
#define PRINT 2
#define FORMAT 3
#define OUTPUT 4
#define NUM_OPTIONS 5

typedef struct Matrix Matrix;
typedef struct Result Result;
typedef struct Level Level;
typedef struct Solver Solver;

struct Level {
	Matrix * A;
	Matrix * M;
	Matrix * P;
	Matrix * PT;
	double * x;
	double * b;

	double * Adense;
	int * ipiv;

	Level * next_level;
	Level * prev_level;
};

struct Solver {
	int num_levels;
	Level * levels;
};

struct Result {
	double * x;
	int flag;
	double relres;
	int iter;
	float total_time;
	double * residuals;
};

struct Matrix {
	double * values;
	int * col_index;
	int * row_ptr;
	int nnz;
	int rows;
	int cols;
};


// Display information to user
void print_data(Matrix * mat);
void pretty(Matrix * mat);
void print_dense(double *, int, int);
void print_result(Result *, int *, int, char *);
void print_vector(double *, int );

// Matrix Management
Matrix * copy_info(Matrix *, int);
Matrix * create_matrix(int, int, int);
Matrix * create_identity(int);

void init_matrix(Matrix *);
void destroy_matrix(Matrix *);

// Result Management
void destroy_result(Result *);
Result * create_result(double *, double *, int, int, int, double, double);

// Matrix Operations
Matrix * transpose(Matrix *);
Matrix * sparse_mult_sparse(Matrix *, Matrix *);
Matrix * sparse_mult_sparse_unorder(Matrix *, Matrix *);
Matrix * sparse_mult_sparse_slow(Matrix *, Matrix *);
//Matrix * sparse_mult_sparse_old(Matrix *, Matrix *);
Matrix * get_adj(Matrix *);
Matrix * get_pattern_bool(Matrix *);
Matrix * i_minus_spd(Matrix *, double );

// Vector Operations
void sparse_mult_vector(Matrix * mat, double * v, double * result);
void scalar_mult_vector(double * vector, double * result, int size, double scalar);
void add_vector(double * lhs, double * rhs, double * result, int size);
void sub_vector(double * lhs, double * rhs, double * result, int size);
void dense_mult_vector(double * mat, double * vector, double * result, int rows, int cols);
void dense_trans_mult_vector(double * mat, double * vector, double * result, int rows, int cols);
void solve_upper(double * A, double * x, double * b, int size, int total_size);

// Utility
int compare_vector(double *, double *,  int size, double tol);
void fill_rand(double *, int);
void verify_solution(Matrix *, Result *, double *);

int is_symmetric(Matrix * mat);
double * create_dense(Matrix *, int, int); 
double * create_full_dense(Matrix *);

// Linear Algebra
double inner_product(double * v1, double * v2, int size);
double norm(double * v1, int size);
double estimate_spectral_radius(Matrix *, int);


// SPMMV Helpers
int bst_set_row(BST *, Matrix *, double *, int);
int node_set_row(Node *, Matrix *, double *, int, int);
#endif

