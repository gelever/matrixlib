
/*
 * Creates a restriction matrix
 *  - Agglomerates the input matrix
 *  - Solves the local eigenproblems on each agglomerate
 *  - Aggregate the dofs
 *  - SVD each aggregate
 *
 *  Input: 
 *   mat - input matrix
 *   agg_size - size of each agglomerate
 *   num_eig - number of eigenvectors per agglomerate
 */
Matrix * create_restriction(Matrix * mat,  int agg_size, int num_eig) {
	Matrix * agglomop;
	Matrix * multi_agg;

	agglomop = get_agglomerate(mat, agg_size);

	Matrix * multi = create_p_multi(mat, agglomop, num_eig);
	
	// Check if p will actually be restriction
	if (multi->rows >= mat->rows) {
		destroy_matrix(agglomop);
		destroy_matrix(multi);
		return NULL;
	}
	multi_agg = aggregate_multi(multi, num_eig);
	
	svd_aggs_multi(multi_agg, num_eig);

	destroy_matrix(multi);
	destroy_matrix(agglomop);

	return multi_agg;
}



/*
 * Creates a restriction matrix
 *  - Agglomerates the input matrix
 *  - Solves the local eigenproblems on each agglomerate
 *  - Aggregate the dofs
 *  - SVD each aggregate
 *
 *  Input: 
 *   mat - input matrix
 *   agg_size - size of each agglomerate
 *   num_eig - number of eigenvectors per agglomerate
 */
Matrix * create_restriction2(Matrix * A, Matrix * M, int agg_size, double theta) {
	Matrix * agglomop;
	//Matrix * multi_agg;

	agglomop = get_agglomerate(A, agg_size);
	Matrix * aggop = aggregate(agglomop);

	Matrix * P = create_p(A, M, agglomop, aggop, theta);
	/*

	   Matrix * multi = create_p_multi(A, agglomop, num_eig);

	// Check if p will actually be restriction
	if (multi->rows >= A->rows) {
	destroy_matrix(agglomop);
	destroy_matrix(multi);
	return NULL;
	}
	multi_agg = aggregate_multi(multi, num_eig);

	svd_aggs_multi(multi_agg, num_eig);

	destroy_matrix(multi);
	destroy_matrix(agglomop);
	*/

	return P;
}
double * coarse_m(Matrix *M, int * aggs,  int size) {
	double * result = calloc(size*size, sizeof(double));
	for (int i = 0; i < size; ++i) {
		if(aggs[i] >= 0) 
			result[i*size + i] = M->values[aggs[i]];
	}
	return result;

}

Matrix * create_p(Matrix * A, Matrix * M, Matrix * agglomop, Matrix * aggop, double theta) {

	Matrix * result = create_matrix(agglomop->rows * 2,
			agglomop->cols, agglomop->nnz * 2);
	//double * m_c;
	double * coarse;

	int size;
	int sizeaggop;
	int count = 0;

	for (int i = 0; i < agglomop->rows; ++i) {
		size = agglomop->row_ptr[i + 1] - agglomop->row_ptr[i];
		sizeaggop = aggop->row_ptr[i + 1] - aggop->row_ptr[i];
		if (sizeaggop < 1) {
			printf("Empty Agglom after aggregate%d!\n", i);
			continue;
		}
		if (size < 1) {
			printf("Error: empty agglomerate!\n");
			exit(1);
		}

		// Find which M diags to keep in M_A
		int * aggs = malloc(size * sizeof(int));
		count = 0;
		for (int j = 0; j < size; ++j) {
			aggs[count] = agglomop->col_index[agglomop->row_ptr[i]+j];
			++count;
		}
		count = 0;
		for (int j = 0; j < sizeaggop; ++j) {
			while(aggs[count] != aggop->col_index[aggop->row_ptr[i]+j]) {
				aggs[count] = -1;
				count++;
			}
			aggs[count] = aggop->col_index[j];
			count++;
		}
		count = 0;



		coarse = coarse_dense(A,
				&agglomop->col_index[agglomop->row_ptr[i]], size); 
		//m_c = coarse_m(M, aggs, size);

		/*
		   printf("Coarse\n");
		   print_dense(m_c, size, size);
		   printf("MC\n");
		   print_dense(m_c, size, size);
		   */

		int info_c;

		double solution_c[size];

		/*
		   printf("Coarse\n");
		   print_dense(coarse, size,size);
		   printf("lap\n");
		   print_dense(dense_sub_matrix, size,size);
		   */

		info_c = LAPACKE_dsyev( LAPACK_ROW_MAJOR, 'V', 'U', 
				size, coarse, size, solution_c );
		if( info_c > 0 ) {
			printf( "The algorithm failed to compute eigenvalues.\n" );
			exit( 1 );
		}

		/*
		   int info;
		   double solution[size];
		// Solve eigenproblem 
		info = LAPACKE_dsyev( LAPACK_ROW_MAJOR, 'V', 'U', 
		size, dense_sub_matrix, size, solution );
		// Check for convergence 
		if( info > 0 ) {
		printf( "The algorithm failed to compute eigenvalues.\n" );
		exit( 1 );
		}
		*/

		/*
		   int l;
		   for (int j = 0; j < num_eig; ++j) {
		   l = 0;
		   int k = result->row_ptr[i*num_eig + j];
		   while (k < result->row_ptr[i*num_eig + j+1]) {
		   result->values[k] = coarse[l*size+size-1-j];
		   ++k;
		   ++l;

		   }
		   }
		   */
		//free(dense_sub_matrix);
	}
	return result;



}
	/*
	   Matrix * result = create_matrix(agglom->rows, agglom->cols, agglom->cols);
	   int k;
	   int * count = calloc(agglom->cols, sizeof(int));

	   int row = 0;
	   int total = 0;
	   for (int i = 0; i < agglom->rows; ++i) {
	   k = agglom->row_ptr[i];
	   while (k < agglom->row_ptr[i+1] && total < agglom->cols) {
	   if(count[agglom->col_index[k]] == 0) {
	   result->values[total] = 1;
	   result->col_index[total] = agglom->col_index[k];
	   count[agglom->col_index[k]]++;
	   total++;
	   }
	   ++k;
	   }
	   row++;
	   result->row_ptr[row] = total;
	   }
	   result->row_ptr[agglom->rows] = total;
	   */
	//return result;
	//
	//



/* Creates P from matrix and agglomerates.
 *  - Initially copies sparsity pattern of agglomerates
 *  - For each agglomerate:
 *	- Create and solve local eigenproblem
 *	- Set the values of each local P to be the num_eig lowest eigenvectors
 *  Input:
 *   mat - original matrix w/ values
 *   agglomerates - agglomerates-dof relationship
 *   num_eig - number of eigenvectors for each agglomerate
 *
 *  TODO: Convert this to use tolerance instead of static number of eigenvectors.
 *
 */
Matrix * create_p_multi(Matrix * mat, Matrix * agglomerates, int num_eig) {
	Matrix * result = create_matrix(agglomerates->rows * num_eig,
			agglomerates->cols, agglomerates->nnz * num_eig);
	double * dense_sub_matrix;
	double * coarse;

	int size;
	int count = 0;
	double * d_inv;

	// Setup new P - copy sparsity pattern of agglomerates
	for (int i = 0; i < agglomerates->rows; ++i) {
		size = agglomerates->row_ptr[i+1] - agglomerates->row_ptr[i];
		for (int j = 0; j < num_eig; ++j) {
			result->row_ptr[i*num_eig+j] = count;
			memcpy(&result->col_index[count], &agglomerates->col_index[agglomerates->row_ptr[i]], size * sizeof(int));

			count += size;
		}
	}

	for (int i = 0; i < agglomerates->rows; ++i) {
		size = agglomerates->row_ptr[i + 1] - agglomerates->row_ptr[i];
		if(size >0) {
			dense_sub_matrix = local_dense(mat,
					&agglomerates->col_index[agglomerates->row_ptr[i]], 
					size); 
			coarse = coarse_dense(mat,
					&agglomerates->col_index[agglomerates->row_ptr[i]], 
					size); 
			d_inv = calloc(size, sizeof(double));

			for(int j =0; j< size; ++j) {
				d_inv[j] = 1/sqrt(coarse[j*size+j]);
			}
			for(int j = 0; j < size; ++j) {
				for (int k = 0; k < size; ++k) {
					coarse[j*size +k] *= d_inv[k]*d_inv[j];
				}
			}



			int info_c;


			double solution_c[size];

			/*
			   printf("Coarse\n");
			   print_dense(coarse, size,size);
			   printf("lap\n");
			   print_dense(dense_sub_matrix, size,size);
			   */

			info_c = LAPACKE_dsyev( LAPACK_ROW_MAJOR, 'V', 'U', 
					size, coarse, size, solution_c );
			if( info_c > 0 ) {
				printf( "The algorithm failed to compute eigenvalues.\n" );
				exit( 1 );
			}

			int info;
			double solution[size];
			/* Solve eigenproblem */
			info = LAPACKE_dsyev( LAPACK_ROW_MAJOR, 'V', 'U', 
					size, dense_sub_matrix, size, solution );
			/* Check for convergence */
			if( info > 0 ) {
				printf( "The algorithm failed to compute eigenvalues.\n" );
				exit( 1 );
			}

			int l;
			for (int j = 0; j < num_eig; ++j) {
				l = 0;
				int k = result->row_ptr[i*num_eig + j];
				while (k < result->row_ptr[i*num_eig + j+1]) {
					//result->values[k] =  coarse[l*size+size-1-j];
					result->values[k] =  d_inv[l]*coarse[l*size+size-1-j];
					++k;
					++l;

				}
			}
			/*
			   int l;
			   for (int j = 0; j < num_eig; ++j) {
			   l = 0;
			   int k = result->row_ptr[i*num_eig + j];
			   while (k < result->row_ptr[i*num_eig + j+1]) {
			   result->values[k] = dense_sub_matrix[l*size+j];
			   ++k;
			   ++l;

			   }
			   }
			   */
			free(dense_sub_matrix);
		}
	}
	return result;
}



double * local_dense(Matrix * mat, int * indices, int size) {
	int k;
	double * result;
	double * count;
	if (size < 1) {
		printf("no size local graph lapalacian!\n");
		exit(0);
	}

	result = calloc(size*size, sizeof(double));
	count = calloc(size, sizeof(double));


	//TODO: Fix this, like n^3 lol
	for (int i = 0; i < size; ++i) {
		k = mat->row_ptr[indices[i]];
		while (k < mat->row_ptr[indices[i] + 1 ] && mat->col_index[k] < indices[i]) {
			for (int j = 0; j < size; ++j) {
				if (mat->col_index[k] == indices[j]) {
					result[i * size + j] -= fabs(mat->values[k]);
					result[j * size + i] -= fabs(mat->values[k]);
					result[i * size + i] += fabs(mat->values[k]);
					result[j * size + j] += fabs(mat->values[k]);
				}
			}
			++k;
		}
	}
	free(count);
	return result;
}

		/*
		   int info;
		   double solution[size];
		// Solve eigenproblem 
		info = LAPACKE_dsyev( LAPACK_ROW_MAJOR, 'V', 'U', 
		size, dense_sub_matrix, size, solution );
		// Check for convergence 
		if( info > 0 ) {
		printf( "The algorithm failed to compute eigenvalues.\n" );
		exit( 1 );
		}
		*/



/*
 * Creates an aggregate matrix from an agglomeration matrix.
 * Input:
 *  mat - Matrix to create aggregtes from
 *
 * Output:
 *  Matrix containing the aggregate-vertex relationship.
 *
 * TODO: Make this simpler!
 */
Matrix * aggregate_multi(Matrix * mat, int num_eig) {
	if (!mat) {
		printf("Invalid Aggregate Paramater!\n");
		exit(0);
	}	
	int cnt = 0;
	int * count;
	count = calloc(mat->cols, sizeof(int));

	Matrix * result = create_matrix(mat->rows, mat->cols, mat->cols*num_eig);
	
	// Counts the number of edges in each agglomeration
	for (int i = 0; i < mat->nnz ; ++i) {
		if (count[mat->col_index[i]] > num_eig -1 ) {
			mat->values[i] = 0;
		}
		else {
			count[mat->col_index[i]]++;
		}
	}


	int k = 0;
	int rows = 0;
	int skip = 0;
	for (int i =0; i < mat->rows; ++i) {
		k = 0;
		if (i % num_eig == 0) {
			skip = 0;
			memset(count, 0, mat->cols * sizeof(int));
		}
		if (!skip) {
			for(int j = mat->row_ptr[i]; j < mat->row_ptr[i+1]; ++j) {
				if (mat->values[j] != 0 ){
					result->values[cnt] = mat->values[j];
					result->col_index[cnt] = mat->col_index[j];
					count[mat->col_index[j]]++;
					++cnt;
					++k;
				}
				else if (mat->values[j] == 0 && count[mat->col_index[j]] > 0 && count[mat->col_index[j]] < num_eig){
					//printf("Zero but needs to be included!\n");
					result->values[cnt] = mat->values[j];
					result->col_index[cnt] = mat->col_index[j];
					++cnt;
					++k;

				}
			}
		}
		if(k >= num_eig){
			rows++;
			result->row_ptr[rows] = cnt;
		}
		else if(k < num_eig && k > 0 && (i % num_eig) <= k ) {
			rows ++;
			result->row_ptr[rows] = cnt;
		}
		if (k < num_eig && i % num_eig == k - 1 && k > 0) 
			skip = 1;

	}
	result->nnz = cnt;
	result->rows = rows;

	free(count);
	return result;	
}
//
//
// SVD each row after aggregation.
void svd_aggs_multi(Matrix * mat, int num_eig) {

	for(int i = 0; i < mat->rows; i += num_eig) {
		int n = mat->row_ptr[i+1] - mat->row_ptr[i], m = num_eig, lda = n, ldu, ldvt = n, info;
		if(n < num_eig) 
			m = n;
		ldu = m;
		
		// Modified from Intel Example:
		int min = n < m ? n : m;
		double superb[min-1];
		double s[n], u[ldu*m], vt[ldvt*n];
		/* Compute SVD */
		info = LAPACKE_dgesvd( LAPACK_ROW_MAJOR, 'A', 'A', m, n, &mat->values[mat->row_ptr[i]], lda,
				s, u, ldu, vt, ldvt, superb );

		/* Check for convergence */
		if( info > 0 ) {
			printf( "The algorithm computing SVD failed to converge.\n" );
			exit( 1 );
		}

		memmove(&mat->values[mat->row_ptr[i]], vt, sizeof(double)*n*m);
		if ( n < num_eig) 
			i -= num_eig -1;
	}
}



/*
 * Creates an aggregate_vertex relationship matrix:
 *	creates edge-vertex relationship
 *	creates edge-edge relationship
 *	partitions edge-edge into agglomerations
 *	creates aggregates from agglomerations
 * Input:
 *  mat - Matrix to create aggregtes from
 *  size - size of each agglomerate
 *
 * Output:
 *  Matrix containing the aggregate-vertex relationship.
 *
 */

Matrix * get_agglomerate(Matrix * mat, int size) {
	// TODO: ARE THESE ALL NECESSARY?
	Matrix * agg_dof;	// agglomerate-vertex relation
	Matrix * agg_edge;	// Agglomerate-edge relation
	Matrix * adj;		// adjacency matrix of edge-edge
	Matrix * dof_dof;	// vertex-vertex relation
	Matrix * ev;		// 
	Matrix * ee;		// Adj matrix of mat
	Matrix * evt;		// Adj matrix of mat
	Matrix * aggregates;

	if(mat->rows <= 1) {
		aggregates= create_matrix(1,1,1);
		aggregates->values[0] = 1;
		return aggregates;
	}
	int * part;
	int agg_size;

	dof_dof = get_adj(mat);

	ev = edge_vertex(dof_dof);

	evt = transpose(ev);
	ee = sparse_mult_sparse(ev, evt);

	adj = get_adj(ee);
	agg_size = adj->rows/size;

	if (agg_size <= 1) {
		part = calloc(mat->rows, sizeof(int));
		agg_size = 1;
		}

	else {
	part = partition(adj, agg_size);
	}
	agg_edge = make_agg_edge(part, agg_size, adj->rows);
	agg_dof = sparse_mult_sparse(agg_edge, ev);
	

	// TODO: REmove, check if all vertices in an agglomerate!
	/*
	int * count = calloc(mat->rows, sizeof(int));
	for (int i = 0; i < agg_dof->nnz; ++i) {
		count[agg_dof->col_index[i]]++;
	}
	for (int i = 0; i < mat->rows; ++i) {
		if (count[i] < 1) {
			printf("Not vertices in agglomerate!\n");
			printf("Level Size: %d vertex : %d\n", mat->rows, i);
		}
	}
	free(count);
	*/

	if (part)
		free(part);

	destroy_matrix(adj);	
	destroy_matrix(agg_edge);
	destroy_matrix(dof_dof);
	destroy_matrix(ev);	
	destroy_matrix(ee);
	destroy_matrix(evt);

	return agg_dof;
}
