#!/usr/bin/bash

echo $@
M=5
TIMES=5
for i in `seq 1 30`;
do
  TOTAL=0
  ITER_TOTAL=0
  M=$(($i*5))
  for j in `seq 1 $TIMES`;
  do
    TEMP=$(./gmres $1 $2 $3 $4 $M -f adji )
    TIME=$(awk '/Time:/ {print $2}' <<<"$TEMP")
    ITER=$(awk '/Count:/ {print $2}' <<<"$TEMP")
    ITER_TOTAL=$(python -c "print(int($ITER) + int($ITER_TOTAL))")
    TOTAL=$(python -c "print(float($TIME) + float($TOTAL))")
  done
  ITER_TOTAL=$(python -c "print(int(int($ITER_TOTAL)/int($TIMES)))")
  TOTAL=$(python -c "print(round(float($TOTAL)/int($TIMES), 3))")
  echo $M $TOTAL $ITER_TOTAL
done
