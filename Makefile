CC=c99
OPTLEVEL=-O3
DEBUG=-g
CFLAGS=-fopenmp -lmetis -llapacke -lblas -lm -Wall $(OPTLEVEL)
PROGRAMS=gmres taskone fpcg_drive amge spec
SRCDIR=src
OBJDIR=obj
SRCS=gmres parser matrix taskone precondition fpcg fpcg_drive partition amge spec bst
OBJS=$(foreach I, $(SRCS), $(OBJDIR)/$I.o)
DIR= $(shell pwd)
UTIL=matrix.o parser.o precondition.o partition.o fpcg.o bst.o
LIBMETIS=metis-5.1.0/GKlib


all:	gmres taskone fpcg_drive amge spec

# Programs
$(PROGRAMS):	$(OBJS) 
	@$(CC) $(foreach I, $(UTIL), $(OBJDIR)/$I) -o $@ $(OBJDIR)/$@.o $(CFLAGS) 


# Object files
$(OBJDIR)/%.o:	$(SRCDIR)/%.c  $(SRCDIR)/%.h
	@mkdir -p $(DIR)/obj;
	@$(CC) -c -o $@ $< $(CFLAGS)

clean:
	rm -fR $(OBJDIR) $(PROGRAMS) 


# Build with debug information and O0 optimizations only.
#debug: clean debug_compile
debug:  clean debug_compile

debug_compile:	OPTLEVEL = -O0
debug_compile:	CFLAGS += $(DEBUG)
debug_compile:	CFLAGS += -DDEBUG=1
debug_compile:	all

.PHONY:	 clean all debug
