\documentclass[a4paper]{article}
\usepackage{amssymb}
\usepackage{amsmath}
\usepackage{amsthm}
\usepackage{hyperref}
\usepackage[]{algorithm2e}
\usepackage{siunitx}
\usepackage{graphicx}
\usepackage[table,x11names]{xcolor}
%\graphicspath{ {~/Documents/diagpre/report/} }


\title{AMGe Project \\Algebraic Multigrid}
\author{Stephan Gelever\footnote{Email: gelever@pdx.edu}}

\begin{document}
\maketitle

\begin{abstract}
	The goal of this project was to implement spectral AMG using my previous matrix library.  
\end{abstract}

\section{Introduction} 
This project is a continuation of the work I had done last term.  I created a matrix library in C that supports basic matrix and vector operations.  
There are several new components that I have added in order to support the implementation of AMGe.  These include partitioning and aggregation.  
When creating the GMRes portion of the project, I did not expect to build further upon the library. 
I have therefore refactored much of the code to make it easier to work with.  

In addition, I have implemented a smoother, the preconditioned conjugate gradient method and the flexible conjugate gradient method.

As of today, my implementation works with both two grid and multilevel.  I plan to add parallel support in the future.

The code found at my bitbucket 
repository\footnote{\hyperref[https://bitbucket.org/gelever/matrixlib/src]{https://bitbucket.org/gelever/matrixlib/src}}.

\section{New Components of Project} 
\subsection{Refactor}
In order to make the matrix library more user friendly, I have refactored much of the code.  There are now functions that take care of 
some of the more tedious tasks in the library.  

Prior to refactoring, it was necessary to manage the creation and destruction of matrices by hand. I have created methods that 
simply created a matrix or result based on arguments passed in by the user.  Destruction of data structures was also greatly simplified.  
Also, most functions used to require the user to pass in a pre-created data structure that the result was stored in.  Whenever possible, I have 
made the function return the new data structure instead of requiring the user to handle creation.  

I have not needed any new data structures for this project.  Although, it would be beneficial to add a vector data type to simply the code. I will 
work on this once the rest of the project is complete.

Overall, this refactoring has made the code much more clear and usable.

\subsection{Conjugate Gradient}
One of the lectures this term covered the Preconditioned Conjugate Gradient method and the Flexible Conjugate Gradient method.  These were similar enough to 
GMRes that I was able to implement them without much addition to the library.  Once AMGe is finalized, I plan to use it as a preconditioner for these function. 

\subsection{Smoothing}
In order to make multigrid work properly, a convergent smoother is needed.  I initially implemented a Jacobi smoother using the diagonal of the matrix.  
Afterwards, I folded the values of the off diagonal entries into the diagonal.  This provided a nice improvement in the convergence rate.  
The inverse is then easily calculated.

\subsection{Partitioning}
One of the most critical portions of this project was creating the interpolation operator \(P\).  AMGe is based on solving local eigenproblems and combining the eigenvectors to 
create \(P\).  In order to create these local eigenproblems, I used the partitioner METIS over the \(edge\text{-}edge\) relationship.  
To create this relationship, it was necessary to create several additional helper functions.

\subsection{Aggregation}
When the \(edge\text{-}edge\) partition was turned back into a \(dof\text{-}agglomerate\) relationship, some \(dofs\) were in multiple agglomerates.  
To solve this issue, I required an algorithm that decides which aggregate to place each \(dof\).
To keep things simple, I place the \(dof\) into the first aggregate that it encountered. 

\subsection{AMGe}
The primary goal of this project was to implement AMGe.  My implementation currently only supports v-cycles.  
It should not be difficult to expand to multiple cycle types.

\subsection{New Operations}
This project added several new functions to the previous matrix library.
\subsubsection{Preconditioning}
    \begin{itemize}
	    \item \textit{Weighted Jacobi Smooth} - Performs a user specified number
		    of rounds of smoothing. The user also provides the weight.

	    \item \textit{Invert Diagonal} - Inverts the diagonal on the input matrix. 
		    Walks through each entry and inverts it.
	    \item \textit{Precondition} - Folds the off diagonal entries into the diagonal.  Scales the values to keep them from growing too large.
	    \item \textit{Create \(M^{-1}\)} - Puts together the previous functions to create a \(M^{-1}\) matrix for the user.
    \end{itemize}
\subsubsection{Partitioning}
    \begin{itemize}
	    \item \textit{Get Adjacency Matrix} - Returns the adjacency matrix from the graph laplacian matrix.  This information was not kept
		    during parsing so I needed retrieve it afterwards.
	    \item \textit{Create \(edge\)-\(vertex\) relationship} -  Creates an \(edge\)-\(vertex\) matrix.  Walks through the input matrix 
		    and marks if and edge is connect to the current vertex.
	    \item \textit{Create \(agglomerate\)-\(edge\) relationship} - Creates and \(agglomerate\)-\(edge\) matrix.  First this counts the number of edges in each agglomerate.  Then, it walks through the result matrix and sets the correct number of entries in each agglomerate.   
	    \item \textit{Aggregate} - Removes vertices that are in multiple agglomerates and restricts then to just one aggregate.  
	    \item \textit{Partition} - Calls METIS to partition the \(edge\)-\(edge\) relationship.  METIS accepts the CSR format so this portion went smoothly.
	    \item \textit{Local Dense Matrices} - Creates a local matrix out of a single aggregate. Returns a dense matrix that can be used with LAPACKe.
	    \item \textit{Create Restriction Operator} - Using the previous functions, this creates the aggregates, solves the local graph laplacian eigenproblem, and then creates the restriction operator \(P^T\).
    \end{itemize}
\subsubsection{Conjugate Gradient}
    \begin{itemize}
	    \item \textit{Preconditioned Conjugate Gradient} - Runs the PCG algorithm.
	    \item \textit{Flexible Preconditioned Conjugate Gradient} - Runs the FPCG algorithm.
    \end{itemize}
\subsubsection{AMGe}
    \begin{itemize}
	    \item \textit{Setup AMGe} - Creates the solver hierarchy for AMGe.  
	    \item \textit{AMGe Iterations} - Runs the v-cycles in the AMGe algorithm until the residual is small enough or a maximum number of iterations has been reached.
    \end{itemize}


\section{AMGe Implementation}
The algorithm for AMGe follows closely the algorithm provided in class. 
There are two steps to solving the system: the setup phase and multigrid iteration phase.

\subsection{Setup Phase}
The setup phase creates the solver hierarchy.  Starting from the finest level, it creates the necessary components to run iterations.  Each level (except the coarsest) requires a prolongation operator, restriction operator, and a 
preconditioner.
Also at this time, memory is allocated for the current solution and the right hand side.

Only the restriction operator is explicitly created.  The prolongation operator is then simply the transpose of the restriction operator.  
This is done because in CSR format it is easier to work with row vectors instead of column vectors.
The setup process is recursively repeated for every level until a user specified coarse level size is reached, the max number of levels is reached, or the restriction operator no longer creates smaller matrices.

\subsubsection{Restriction Operator}
Computing the restriction operator takes several steps.  First the agglomerates are calculated.  The \(dof\)-\(dof\) relationship is created from the current level matrix.  From this, the \(edge\)-\(dof\) relationship
is formed.  To create the \(edge\)-\(edge\) relationship, we simply compute \((edge-dof)\times(edge-dof)^T\).  The adjacency matrix of this \(edge\)-\(edge\) relationship is then passed into METIS.  To determine the number of
parts to partition this relationship into, the user supplies an\(agglomerate-size\) parameter.  Then the number of parts is calculated by \(num(edges)/agglomerate-size\).  METIS returns a partition of the edges.  
From this partition, the \(agglomerate\)-\(edge\) relationship is formed.  Computing \(aggomerate\)-\(edge \times edge\)-\(vertex\) gives us a \(agglomerate\)-\(dof\) relationship.  

Using this \(agglomerate\)-\(dof\) relationship, it is now possible to calculate the local eigenproblems.  For each agglomerate, a dense submatrix is created:
\[A_e = \delta_e d_e {d_e}^T\]
Where \(\delta_e\) is the weight of the edge and \(d_e\) is a zero vector with non-zero entries 1 and -1 at positions \(i\) and \(j\).
Next, the local eigenproblem is solved: 
\[A_eq_k = \lambda_k q_k\]
The user specifies the number of eigenvectors that each local \(P_T\) contains.  Each local eigenproblem is solved from every agglomerate. 

Next, aggregation of the agglomerates is performed.  Each \(dof\) is restricted to be in only a single aggregate.  However, each local \(P_T\) may no longer be linearly independent.  SVD is performed to create
a set of linearly independent vectors.  These new \(P_A\) are now combined to create a global restriction matrix \(P\).

\subsubsection{Restriction Operator Algorithm}
\begin{algorithm}[H]
	\KwData{\\\(A\) - Input Matrix\\ \(num eig\) - Number of eigenvectors to include\\ }
	\KwResult{\(P^T\) - Restriction Operator}
	Agglomerate A\\
	Create \(P^T\) from A and agglomerates with \(num eig\) eigenvectors in each local P\\
	Aggregate \(P\)\\
	SVD \(P\)\\
	
	\For{i in 1,2,.. degrees}{
		\(P = (I-MA)P\)
		}
	\caption{Restriction Operator}
\end{algorithm}

\subsubsection{Coarse Level}
To calculate the coarse level, the finer level restriction and prolongation operators are used.
 \[ A_c = P^T A P\]
The coarsest level is kept as a dense matrix.  It is also LU factored.  This allows it to be solved faster during iterations.


\subsubsection{Operator Smoothing}
The user is able to specify how the prolongation operator P is smoothed.  \( P = (I-MA)P\) is repeated for however many degrees are specified.  
This is a relatively expensive operation as it requires a \(matrix \times matrix\) operation.


\subsection{Iteration}
The iterations follow the standard multigrid algorithm.  Only a v-cycle is currently supported.
First presmoothing is done for a user specified number of steps. Then, \(b_c\) is calculated by restricting the residual.  This is done until the coarsest level is reached.
Then, we use LAPACKE to solve directly on the coarsest level. Moving back up through the hierarchy, coarse grid correction is performed.  Finally, the postsmoothing is done for a user specified number of steps.

This cycle is repeated until the residual is below a certain tolerance or the max number of iterations has been reached.
\newline

\section{AMGe Algorithms}
\begin{algorithm}[H]
	\KwData{\\\(A\) - Input Matrix\\ \(coarseSize\) - size of coarsest size\\ }
	\KwResult{\(Solver\) - Solver hierarchy holding each level}
	Current Level - A = A\\
	Current Level - M = calculate M from A\\
	Current Level - \(\text{P}^{\text{T}}\)= calculate \(\text{P}^{\text{T}}\)from A\\
	Current Level - P = calculate P from \(\text{P}^{\text{T}}\)\\
	\While{Current Level - P \(>\) coarseSize }{
		Next Level - A = \(\text{P}^{\text{T}}\) \(\times\) A \(\times\) P\\
		Next Level - M = calculate \(M_c\) from  \(A_c\)\\
		Next Level - \(\text{P}^{\text{T}}\)= calculate \(\text{P}^{\text{T}}_c\) from \(A_c\)\\
		Next Level - P = calculate \(P_c\) from \(\text{P}^{\text{T}}_c\)  \\
		Current Level - Next Level = Next Level\\
		Current Level  = Next Level\\
		}
	\caption{Setup Solver}
\end{algorithm}

\begin{algorithm}[H]
	\KwData{\\\(Solver\) - Solver Hierarchy \\ \(b\) - right hand side\\ }
	\KwResult{\(x\) - approximate solution to the system}
	\While{\(numIterations <  maxIterations \&\& res > tol\) }{
		\While {Not at coarsest level}{
			Pre-smooth current level\\
		Calculate coarse level \(b\)\\
		Go to coarser level\\
		}
		Solve Coarsest Level\\
		\While {Not at finest level}{
			Coarse Grid Correction from coarser level\\
		Postsmooth current level\\
		Go to finer level\\
		}
		}
		Return Finest Level \(x\)\\
	\caption{Multilevel AMGe}
\end{algorithm}

\subsection{Issues with Implementing AMGe }
The goal was to create a fully functioning implementation of AMGe.  Right now, it is fully functioning for most problems and parameters.  By parameters, I mean 
the user control input (\textit{i.e} agglomerate size, smoothing steps, etc). However, for some parameters, my implementation fails to converge.  The residual
may spontaneously blowup after steadily decreasing.  I believe this may be an overflow issue somewhere. Also, there are some cases where I encounter segmentation faults.
These issues are not common.

\subsection{Optimizations}
To improve the performance of my implementation, there are several areas that need work.  Right now, my
\(matrix \times matrix\) operation is the slowest.  It would be beneficial to optimize it as it is the most 
time consuming portion of the setup phase.  Alternatively, I can build the \(edge\)-\(edge\) relationship without
using this operation. 

It would also be possible to parallelize some of the code.  For example, the local eigenproblems can be solved in parallel.

Also, for ease of use, it may be useful to create a GUI in Python.  Entering parameters through the command line can get tedious.  
\subsection{Study}

I performed a study of how altering the parameters affects the convergence rate. I used the same edgelists as my previous report.
This study will cover:
\begin{itemize}
	\item Smoothing Steps
	\item Agglomerate Size
	\item Prolongater Smoothing
	\item Multilevel Comparison
\end{itemize}

The tolerance is set to \(1\mathrm{e}{-07}\).
There are two metrics to look at: total time and number of iterations.
For these studies, I will be using the amazon\_h\_0 edgelist:
\begin{center}
	\begin{tabular}{|lll|}
		\hline
		 dofs & nnz & density\\
		\hline
		863&5705&0.77\%\\
		\hline
	\end{tabular}
\end{center}

\subsubsection{Smoothing Steps}
I will be looking at how the number of smoothing steps
affects the rate of convergence.  The aggregate size has been held constant to where the coarse size is 102. One degree of prolongator smoothing has been applied. 
To keep things simple, only two levels are used for this portion of the study.
Solver setup:
\begin{center}
	\begin{tabular}{|llll|}
		\hline
		Levels & dofs & nnz & density\\
		\hline
		0&863&5705&0.77\%\\
		1&102&1148&11.04\%\\
		\hline
	\end{tabular}

	\begin{tabular}{l}
		Grid Complexity: 1.118\\
		Operator Complexity: 1.201
	\end{tabular}
\end{center}


\begin{center}
	Results
	\includegraphics[width=\textwidth]{smoothsteps.png}

	\begin{tabular}{|l|ll|}
		\hline
		Steps&Seconds&Iterations\\
		\hline
		1&0.122&913\\
		2&0.101&563\\
		3&0.097&439\\
		4&0.098&370\\
		5&0.101&324\\
		10&0.112&211\\
		20&0.144&148\\
		\hline
	\end{tabular}
\end{center}

As seen in the table, the optimal number of smoothing steps is either 3 or 4.  Although more smoothing steps 
reduces the total number of iterations, it takes longer a longer time to converge.

\subsubsection{Agglomerate Size}
For this study I will be looking at how aggregate size affects convergence.  The number of smoothing steps will be
held constant at 4. One degree of smoothing will be applied to \(P\).  Only two levels will be used. In the tables, dofs, nnz, density, grid, and operator complexity will refer to the coarse level.
\begin{center}
	\includegraphics[width=\textwidth]{aggs.png}
	\begin{tabular}{|l|lllllll|}
		\hline
		Agg. Size& dofs & nnz& Density&Operator & Grid&Seconds&Iterations\\
		\hline
		4&369&7179&5.27&2.26&1.42&0.088&74\\
		6&265&4135&5.89&1.72&1.31&0.105&190\\
		8&213&3105&6.84&1.54&1.25&0.057&135\\
		10&180&2422&7.47&1.42&1.21&0.054&154\\
		20&102&1148&11.03&1.20&1.19&0.098&370\\
		\hline
	\end{tabular}
\end{center}

The data seems to support the idea that a smaller aggregate size is faster than the larger aggregate sizes.
The fastest size in terms of time was 10, even though it was not the least number of iterations. I do believe 
memory cache size plays a role in optimizing this parameter. 


\subsubsection{Prongator Smoothing}
For this study I will be looking at how smoothing \(P\) affects convergence.  The number of smoothing steps will be held constant at 1 and the agglomerate size will be 20.  
The number of nonzeros in each level will vary depending on how \(P\) is smoothed.
Solver setup:
\begin{center}
	\begin{tabular}{|ll|}
		\hline
		Levels & dofs \\
		\hline
		0&863\\
		1&102\\
		\hline
	\end{tabular}

	\begin{tabular}{l}
		Grid Complexity: 1.118\\
	\end{tabular}
\end{center}


\begin{center}
	Results

	\includegraphics[width=\textwidth]{smooth.png}

	\begin{tabular}{|l|ll|}
		\hline
		Degrees&Seconds&Iterations\\
		\hline
		0&0.173&1387\\
		1&0.123&913\\
		2&0.096&682\\
		3&0.098&663\\
		\hline
	\end{tabular}
\end{center}

The data shows that the effectiveness of smoothing \(P\) diminishes after the first few degrees.  The first smoothing greatly helps the convergence rate.
However, the difference between 2 and 3 degrees is minimal and probably not worth the setup time.

\subsubsection{Multilevel Comparison}
For this study I will be looking at additional levels affect the convergence rate.  The number of smoothing steps will be held constant at 1 and the agglomerate size will be 20.  
There will be 1 smoothing step and \(P\) will be smoothed by 1 degree.
The number of nonzeros in each level will vary depending on how \(P\) is smoothed.
Solver setup:

\begin{center}
	Results

	\includegraphics[width=\textwidth]{levels.png}

	\begin{tabular}{|l|lllll|}
		\hline
		Levels&Seconds&Iterations&Level Size &Grid Complexity&Operator Complexity\\
		\hline
		2&0.123&913& 102&1.118&1.201\\
		3&0.402&3720& 16&1.137&1.241\\
		4&0.401&3721& 2&1.139&1.242\\
		\hline
	\end{tabular}
\end{center}

The data shows that 2 grid is fastest by a significant amount.  Adding more levels adds more iterations
and time.  However, for these parameters, there is almost no difference between adding a third and fourth level.  

\end{document}
