\documentclass[a4paper]{article}
\usepackage{amssymb}
\usepackage{amsmath}
\usepackage{amsthm}
\usepackage{hyperref}
\usepackage[]{algorithm2e}
\usepackage{siunitx}
\usepackage{graphicx}
\usepackage{float}
\usepackage[table,x11names]{xcolor}
%\graphicspath{ {~/Documents/diagpre/report/} }


\title{AMGe Project \\Algebraic Multigrid}
\author{Stephan Gelever\footnote{Email: gelever@pdx.edu}}

\begin{document}
\maketitle

\begin{abstract}
	The goal of this project was to implement spectral AMG using my previous matrix library.  
\end{abstract}

\section{Introduction} 
This project is a continuation of the work I had done last term.  I created a matrix library in C that supports basic matrix and vector operations.  
There are several new components that I have added in order to support the implementation of AMGe.  These include partitioning and aggregation.  
When creating the GMRes portion of the project, I did not expect to build further upon the library. 
I have therefore refactored much of the code to make it easier to work with.  

In addition, I have implemented a smoother, the preconditioned conjugate gradient method and the flexible conjugate gradient method.

As of today, my implementation works with both two grid and multilevel.
Parallel computation has been included to solve the local eigenproblems.

The code found at my bitbucket 
repository\footnote{\hyperref[https://bitbucket.org/gelever/matrixlib/src]{https://bitbucket.org/gelever/matrixlib/src}}.

\section{New Components of Project} 
\subsection{Refactor}
In order to make the matrix library more user friendly, I have refactored much of the code.  There are now functions that take care of 
some of the more tedious tasks in the library.  

Prior to refactoring, it was necessary to manage the creation and destruction of matrices by hand. I have created methods that 
simply created a matrix or result based on arguments passed in by the user.  Destruction of data structures was also greatly simplified.  
Also, most functions used to require the user to pass in a pre-created data structure that the result was stored in.  Whenever possible, I have 
made the function return the new data structure instead of requiring the user to handle creation.  

I have not needed any new data structures for this project.  Although, it would be beneficial to add a vector data type to simply the code. I will 
work on this once the rest of the project is complete.

Overall, this refactoring has made the code much more clear and usable.

\subsection{Conjugate Gradient}
One of the lectures this term covered the Preconditioned Conjugate Gradient method and the Flexible Conjugate Gradient method.  These were similar enough to 
GMRes that I was able to implement them without much addition to the library.  Once AMGe is finalized, I plan to use it as a preconditioner for these function. 

\subsection{Smoothing}
In order to make multigrid work properly, a convergent smoother is needed.  I initially implemented a Jacobi smoother using the diagonal of the matrix.  
Afterwards, I folded the values of the off diagonal entries into the diagonal.  This provided a nice improvement in the convergence rate.  
The inverse is then easily calculated.

\subsection{Partitioning}
One of the most critical portions of this project was creating the interpolation operator \(P\).  AMGe is based on solving local eigenproblems and combining the eigenvectors to 
create \(P\).  In order to create these local eigenproblems, I used the partitioner METIS over the \(dof\text{-}dof\) relationship.  

\subsection{Aggregation}
Aggregating \(P^T\) after solving the local eigenproblems is accomplished by restricting each \(dof\) back to the same aggregate that was creating by the partition.

\subsection{AMGe}
The primary goal of this project was to implement AMGe.  
My implementation supports a user selected number of solves per level (i.e v-cycle is 1, w-cycle is 2).

\subsection{New Operations}
This project added several new functions to the previous matrix library.
\subsubsection{Preconditioning}
    \begin{itemize}
	    \item \textit{Weighted Jacobi Smooth} - Performs a user specified number
		    of rounds of smoothing. The user also provides the weight.

	    \item \textit{Invert Diagonal} - Inverts the diagonal on the input matrix. 
		    Walks through each entry and inverts it.
	    \item \textit{Precondition} - Folds the off diagonal entries into the diagonal.  Scales the values to keep them from growing too large.
	    \item \textit{Create \(M^{-1}\)} - Puts together the previous functions to create a \(M^{-1}\) matrix for the user.
    \end{itemize}
\subsubsection{Partitioning}
    \begin{itemize}
	    \item \textit{Get Adjacency Matrix} - Returns the adjacency matrix from the graph laplacian matrix.  This information was not kept
		    during parsing so I needed retrieve it afterwards.
	    \item \textit{Create \(aggregate\)-\(dof\) relationship} - Creates and \(aggregate\)-\(dof\) matrix.  First this counts the number of \(dofs\) in each aggregate.  
		    Then, it walks through the result matrix and sets the correct number of entries in each aggregate.
	    \item \textit{Aggregate} - Removes vertices that are in multiple agglomerates and restricts then to just one aggregate.  
	    \item \textit{Partition} - Calls METIS to partition a relationship.  METIS accepts the CSR format so this portion went smoothly.
	    \item \textit{Local Dense Matrices} - Creates a local matrix out of a single aggregate. Returns a dense matrix that can be used with LAPACKe.
	    \item \textit{Create Restriction Operator} - Using the previous functions, this creates the aggregates, solves the local graph laplacian eigenproblem, and then creates the restriction operator \(P^T\).
    \end{itemize}
\subsubsection{Conjugate Gradient}
    \begin{itemize}
	    \item \textit{Preconditioned Conjugate Gradient} - Runs the PCG algorithm.
	    \item \textit{Flexible Preconditioned Conjugate Gradient} - Runs the FPCG algorithm.
    \end{itemize}
\subsubsection{AMGe}
    \begin{itemize}
	    \item \textit{Setup AMGe} - Creates the solver hierarchy for AMGe.  
	    \item \textit{AMGe Iterations} - Cycles through the AMGe algorithm until the residual is small enough or a maximum number of iterations has been reached.
    \end{itemize}


\section{AMGe Implementation}
The algorithm for AMGe follows closely the algorithm provided in class. 
There are two steps to solving the system: the setup phase and multigrid iteration phase.

\subsection{Setup Phase}
The setup phase creates the solver hierarchy.  Starting from the finest level, it creates the necessary components to run iterations.  Each level (except the coarsest) requires a prolongation operator, restriction operator, and a 
preconditioner.
Also at this time, memory is allocated for the current solution and the right hand side.

Only the restriction operator is explicitly created.  The prolongation operator is then simply the transpose of the restriction operator.  
This is done because in CSR format it is easier to work with row vectors instead of column vectors.
The setup process is recursively repeated for every level until a user specified coarse level size is reached, the max number of levels is reached, or the restriction operator no longer creates smaller matrices.

\subsubsection{Restriction Operator}
Computing the restriction operator takes several steps.  
First the \(dofs\) are partitioned using metis.  
The \(aggregate\)-\(dof\) relationship is created using the partition.
From this, the \(agglomerate\)-\(dof\) relationship is formed by computing \(aggregate\)-\(dof\times dof\)-\(dof\).
Using this \(agglomerate\)-\(dof\) relationship, it is now possible to calculate the local eigenproblems.  
For each agglomerate, a dense submatrix is created:
\[A_e = \delta_e d_e {d_e}^T\]
Where \(\delta_e\) is the weight of the edge and \(d_e\) is a zero vector with non-zero entries 1 and -1 at positions \(i\) and \(j\).
Next, the local eigenproblem is solved: 
\[A_eq_k = \lambda_k q_k\]
The user specifies a \textit{theta} tolerance that determines how many vectors each local \(P_T\) contains.  
Each local eigenproblem is then solved in parallel.

Next, aggregation of the agglomerates is performed.  Each \(dof\) is restricted to be in only a single aggregate.  However, each local \(P_T\) may no longer be linearly independent.  
SVD is performed to create a set of linearly independent vectors.  
These new \(P_A\) are now combined to create a global restriction matrix \(P\).

\subsubsection{Restriction Operator Algorithm}
\begin{algorithm}[H]
	\KwData{\\\(A\) - Input Matrix\\ \(theta\) - spectral tolerance\\ \(aggSize\) - Aggregate size}
	\KwResult{\(P^T\) - Restriction Operator}
	Partition A into \((\text{Rows in A}) / aggSize\) parts\\
	Expand the \(aggregate\)-\(dof\) relation into \(agglomerate\)-\(dof\) relation\\
	\For{Each Agglomerate} {
		Create a local dense matrix as outlined above\\
		Solve the local eigenproblem\\
		Add largest eigenvectors within \(theta\) tolerance of largest eigenvalue to global \(P^T\)\\
		}
	Aggregate \(P^T\)\\
	SVD \(P^T\)\\
	\(P\) = \((P^T)^T\)\\
	\caption{Restriction Operator}
\end{algorithm}

\subsubsection{Coarse Level}
To calculate the coarse level, the finer level restriction and prolongation operators are used.
 \[ A_c = P^T A P\]
The coarsest level is kept as a dense matrix.  It is also LU factored.  This allows it to be solved faster during iterations.


\subsubsection{Operator Smoothing}
The user is able to specify how the prolongation operator P is smoothed.  \( P = (I-MA)P\) is repeated for however many degrees are specified.  
This can quickly become expensive as it increases the density of the coarser grid.


\subsection{Iteration}
The iterations follow the standard multigrid algorithm.  

A recursive approach is used to solve each level.
If the level is the coarsest in the solver, it is solved directly using LAPACKe.
Otherwise, presmoothing is done for a user specified number of steps. 
Then, \(b_c\) is calculated by restricting the residual.  
A call is made to solve the next coarsest level.
After the coarser level has been solved, coarse grid correction is performed.
Finally, the postsmoothing is done for a user specified number of steps.

This cycle is repeated until the residual is below a certain tolerance or the max number of iterations has been reached.
\newline

\section{AMGe Algorithms}
\begin{algorithm}[H]
	\KwData{\\\(A\) - Input Matrix\\ \(coarseSize\) - size of coarsest size\\ }
	\KwResult{\(Solver\) - Solver hierarchy holding each level}
	Current Level - A = A\\
	Current Level - M = calculate M from A\\
	Current Level - \(\text{P}^{\text{T}}\)= calculate \(\text{P}^{\text{T}}\)from A\\
	Current Level - P = calculate P from \(\text{P}^{\text{T}}\)\\
	\While{Current Level - P \(>\) coarseSize }{
		Next Level - A = \(\text{P}^{\text{T}}\) \(\times\) A \(\times\) P\\
		Next Level - M = calculate \(M_c\) from  \(A_c\)\\
		Next Level - \(\text{P}^{\text{T}}\)= calculate \(\text{P}^{\text{T}}_c\) from \(A_c\)\\
		Next Level - P = calculate \(P_c\) from \(\text{P}^{\text{T}}_c\)  \\
		Current Level - Next Level = Next Level\\
		Current Level  = Next Level\\
		}
	\caption{Setup Solver}
\end{algorithm}

\begin{algorithm}[H]
	\KwData{\\\(Solver\) - Solver Hierarchy \\ \(b\) - right hand side\\ }
	\KwResult{\(x\) - approximate solution to the system}
	\While{\(numIterations <  maxIterations\) and \(res > tol\) }{
		Solve Finest Level\\
		Calculate Residual\\
		numIterations = numIterations + 1\\
		}
		Return Finest Level \(x\)\\
	\caption{Iterate AMGe}
\end{algorithm}

\begin{algorithm}[H]
	\KwData{\\\(Level\) - Current Level \\ \(maxCycles\) - Number of cycles per level}
	\If{Current Level is Coarsest Level} {
		Directly Solve Current Level\\
		}
	\Else {
	\While{\(numCycles <  maxCycles\) }{
		Presmooth current level\\
		Solve next coarser level\\
		Coarse grid correction\\
		Postsmooth current level\\
		}
		}
	\caption{Solve Level}
\end{algorithm}

\subsection{Issues with Implementing AMGe }
The goal was to create a fully functioning implementation of AMGe.  
Right now, it is fully functioning for most problems and parameters.  
By parameters, I mean the user control input (\textit{i.e} agglomerate size, smoothing steps, etc). 
However, for some parameters, my implementation fails to converge.  
The residual may spontaneously blowup after steadily decreasing.  
This is sometimes remedied in the graph laplacian case by subtracting the average constant vector from each iteration.
These issues are not common.

\subsection{Optimizations}
For ease of use, it may be useful to create a GUI in Python.  Entering parameters through the command line can get tedious.  

\section{Study}

I performed a study of how altering the parameters affects the convergence rate. 

This study will cover:
\begin{itemize}
	\item Smoothing Steps
	\item Spectral Tolerance
	\item Aggregate Size
	\item Prolongater Smoothing
	\item Poisson Refinement
	\item Multilevel Comparison
\end{itemize}

For these studies, the residual tolerance is set to \(1\mathrm{e}{-07}\).

\subsection{Smoothing Steps}
I will be looking at how the number of smoothing steps affects the rate of convergence.  
In this section, I will be using the 3-D Poisson problem with 50 nodes.
The aggregate size has been held constant 30. 
The spectral tolerance has been set low at .01.
No smoothing has been applied to the interpolator. 
The solver will have 4 levels total.  

\begin{table}[H]
	\begin{center}
	\begin{tabular}{|l|ll|}
		\hline
		 level&dofs & nnz \\
		\hline
		1&125000&860000\\
		2&4166&63802\\
		3&138&1720\\
		4&4&16\\
		\hline
	\end{tabular}

	\begin{tabular}{l}
		Grid Complexity: 1.034\\
		Operator Complexity: 1.076
	\end{tabular}
\end{center}
	\caption{50 Node 3-D Poisson Problem: Setup}
\end{table}
\begin{table}[H]
\begin{center}
	\begin{tabular}{|l|ll|}
		\hline
		Steps&Seconds&Iterations\\
		\hline
		1 &3.165  &82\\
		2 &3.192  &56\\
		3 &3.800  &49\\
		4 &4.722  &46\\
		5 &5.709  &45\\
		10&9.355 &41\\
		20&15.684&36\\
		\hline
	\end{tabular}
\end{center}
	\caption{50 Node 3-D Poisson Problem: Smoothing Steps}
\end{table}
	\begin{figure}[H]
		\centering
		\includegraphics[width=\textwidth]{smoothing.png}
			\caption{Smoothing Steps vs Iterations}
	\end{figure}
	\begin{figure}[H]
		\centering
		\includegraphics[width=\textwidth]{smoothingsolve.png}
			\caption{Smoothing Steps vs Solve Time}
	\end{figure}

As seen in the table, the optimal number of smoothing steps is 2. After 2 steps, the benefit is greatly reduced.
Although more smoothing steps reduces the total number of iterations, it takes longer a longer time to reach a solution.

\subsection{Spectral Tolerance}
I will be looking at how the spectral tolerance affects both the solver setup and the rate of convergence.
In this section, I will be using the 3-D Poisson problem with 20 nodes.
The aggregate size will be held constant 30. 
Two smoothing steps will be applied to the interpolator. 
The solver will allow for a maximum of 4 levels.

\begin{table}[H]
\begin{center}
	\begin{tabular}{|l|ll|l|}
		\hline
		 spectral& oper & setup &  \\
		  tolerance& comp & time & iters  \\
		\hline
		.01 & 1.238 & 1.076 & 27\\ 
		.02 & 1.242 & 1.081 & 27\\ 
		.03 & 1.284 & 1.105 & 26\\ 
		.04 & 1.446 & 1.271 & 25\\ 
		.05 & 1.572 & 1.772 & 24\\ 
		.06 & 1.835 & 1.856 & 19\\ 
		.07 & 2.121 & 3.598 & 15\\ 
		.08 & 2.422 & 3.098 & 14\\ 
		.09 & 2.787 & 4.829 & 13\\ 
		.10 & 3.117 & 6.651 & 12\\ 
		\hline
	\end{tabular}
\end{center}
	\caption{20 Node 3-D Poisson Problem: Spectral Tolerance}
\end{table}
	\begin{figure}[H]
		\centering
		\includegraphics[width=\textwidth]{toliters.png}
			\caption{Spectral Tolerance vs Iterations}
	\end{figure}
	\begin{figure}[H]
		\centering
		\includegraphics[width=\textwidth]{tolcomp.png}
			\caption{Spectral Tolerance vs Operator Complexity}
	\end{figure}
	\begin{figure}[H]
		\centering
		\includegraphics[width=\textwidth]{tolsetup.png}
			\caption{Spectral Tolerance vs Setup Time}
	\end{figure}

I also looked at how the spectral tolerance affects the Graph Laplacian case.
In this section, I will be using the amazon\_h\_0 edgelist that was provided.
The aggregate size will be held constant 80. 
One smoothing steps will be applied to the interpolator. 
The solver will use only two levels.

\begin{table}[H]
	\begin{center}
		\begin{tabular}{|l|ll|l|}
			\hline
			spectral& oper & setup &  \\
			tolerance& comp & time & iters  \\
			\hline
			.01 & 1.105 & 0.088 & 295\\ 
			.02 & 1.193 & 0.091 & 158\\ 
			.03 & 1.226 & 0.097 & 115\\ 
			.04 & 1.295 & 0.098 & 79\\ 
			.05 & 1.395 & 0.099 & 62\\ 
			.06 & 1.426 & 0.105 & 57\\ 
			.07 & 1.480 & 0.105 & 44\\ 
			.08 & 1.520 & 0.108 & 44\\ 
			.09 & 1.598 & 0.112 & 37\\ 
			.10 & 1.662 & 0.112 & 31\\ 
			\hline
		\end{tabular}
	\end{center}
	\caption{amazon\_h0: Spectral Tolerance}
\end{table}
	\begin{figure}[H]
		\centering
		\includegraphics[width=\textwidth]{tolamazon.png}
			\caption{Spectral Tolerance vs Iterations}
	\end{figure}
	\begin{figure}[H]
		\centering
		\includegraphics[width=\textwidth]{tolamazoncomp.png}
			\caption{Spectral Tolerance vs Operator Complexity}
	\end{figure}
	\begin{figure}[H]
		\centering
		\includegraphics[width=\textwidth]{tolamazonsetup.png}
			\caption{Spectral Tolerance vs Setup Time}
	\end{figure}

As seen in the tables, there is trade off between setup time, operator complexity and iterations.
By increasing the spectral tolerance, the number of iterations required is decreased.  
However, this comes at a cost of increased memory use for each level and the longer setup time.

\subsection{Aggregate Size}
For this study I will be looking at how aggregate size affects convergence.  
I will be using the 3-D Poisson problem with 20 nodes.
The number of smoothing steps will be held constant at 2. 
One degree of smoothing will be applied to \(P\).  
The spectral tolerance has been set low at .01.

Only two levels will be used. 
In the following tables, the dofs and nnz will refer to the coarse level.
\begin{table}[H]
	\begin{center}
		%\includegraphics[width=\textwidth]{aggs.png}
		\begin{tabular}{|l|lll|ll|ll|}
			\hline
			 &      &    &        &Oper & Grid&Setup&         \\
			Agg. Size& dofs & nnz& density&Comp & Comp&Time &Iterations\\
			\hline
			5&1599&67005&2.62 &2.25&1.20&6.16&11\\
			10&800&28740&4.49 &1.54&1.10&2.92&17\\
			15&533&17599&6.20 &1.33&1.07&2.80&21\\
			20&400&12092&7.56 &1.23&1.05&2.07&22\\
			25&320&9092 &8.88 &1.17&1.04&1.91&27\\
			30&266&7656 &10.82&1.14&1.03&1.80&34\\
			35&228&4782 &9.20 &1.09&1.03&1.18&32\\
			40&200&3988 &9.97 &1.07&1.03&1.39&32\\
			45&177&3399 &10.85&1.06&1.02&1.40&35\\
			50&160&3038 &11.87&1.05&1.02&1.72&39\\
			\hline
		\end{tabular}
	\end{center}
		\caption{20 Node Poisson Problem: Aggregate Size}
\end{table}

	\begin{figure}[H]
		\centering
		\includegraphics[width=\textwidth]{aggsiters.png}
			\caption{Aggregate Size vs Iterations}
	\end{figure}
	\begin{figure}[H]
		\centering
		\includegraphics[width=\textwidth]{aggscomp.png}
			\caption{Aggregate Size vs Operation Complexity}
	\end{figure}
	\begin{figure}[H]
		\centering
		\includegraphics[width=\textwidth]{aggssetup.png}
			\caption{Aggregate Size vs Setup Time}
	\end{figure}

	The data shows that the aggregate size has an effect on the operator complexity, the setup time and number of iterations required.
	The smaller aggregate size require less iterations but at the cost of higher operator complexity and setup time.
	Memory cache size would most likely be a consideration when optimizing this parameter.


	\subsection{Prolongator Smoothing}
	For this study I will be looking at how smoothing the interpolator affects convergence.  
	I will be using the 3-D Poisson problem with 20 nodes.
	The number of smoothing steps will be held constant at 2 and the aggregate size will be 30.  
	The spectral tolerance has been set low at .01.
	The solver will allow for a maximum of 4 levels.

	\begin{table}[H]
		\begin{center}
			\begin{tabular}{|l|ll|l|}
				\hline
				smooth& oper & setup &  \\
				degrees& comp & time & iters  \\
				\hline
				0 & 1.071 & 1.943 & 53\\ 
				1 & 1.144 & 1.943 & 34\\ 
				2 & 1.238 & 1.973 & 27\\ 
				3 & 1.354 & 2.151 & 24\\ 
				4 & 1.479 & 2.319 & 22\\ 
				5 & 1.610 & 2.982 & 20\\ 
				6 & 1.744 & 3.701 & 19\\ 
				7 & 1.873 & 5.059 & 18\\ 
				8 & 1.979 & 6.253 & 17\\ 
				9 & 2.070 & 8.406 & 16\\ 
				10& 2.143 & 10.514& 16\\ 
				\hline
			\end{tabular}
		\end{center}
		\caption{20 Node 3-D Poisson Problem: Prolongator Smoothing}
	\end{table}
	\begin{figure}[H]
		\centering
			\includegraphics[width=\textwidth]{smoothiters.png}
			\caption{Smoothing Degrees vs Iterations}
	\end{figure}
	\begin{figure}[H]
		\centering
		\includegraphics[width=\textwidth]{smoothcomp.png}
			\caption{Smoothing Degrees vs Operator Complexity}
	\end{figure}
	\begin{figure}[H]
		\centering
			\includegraphics[width=\textwidth]{smoothsetup.png}
			\caption{Smoothing Degrees vs Setup Time}
	\end{figure}

The data shows that the effectiveness of smoothing \(P\) diminishes after the first few degrees.
The first two smoothing greatly helps the convergence rate without deteriorating the setup time.
Smoothing by more than two degrees improves the rate of convergence, but greatly increases both the
operator complexity and setup time.

\subsection{Poisson Refinement}
For this study I will be looking at how the solver performs on discrete Poisson problems.
I will be using 3-D Poisson problems.
The number of smoothing steps will be held constant at 2 and the aggregate size will be 30.  
The spectral tolerance has been set low at .01.
One degree of smoothing will be applied to \(P\).  
The solver will allow for a maximum of 8 levels.

\begin{table}[H]
	\begin{center}
		\begin{tabular}{|l|l||l|l||l|l||l|l|}
			\hline
			&     &     &          &     &     &     & \\
			nodes&iters&nodes&iters&nodes&iters&nodes&iters  \\
			\hline
			50& 36&  76& 41&  102& 40& 128& 43\\
			52& 35&  78& 42&  104& 40& 130& 43\\
			54& 36&  80& 42&  106& 40& 132& 43\\
			56& 38&  82& 42&  108& 42& 134& 43\\
			58& 37&  84& 42&  110& 41& 136& 43\\
			60& 38&  86& 42&  112& 41& 138& 43\\
			62& 37&  88& 43&  114& 42& 140& 44\\
			64& 38&  90& 43&  116& 41& 142& 44\\
			66& 39&  92& 41&  118& 42& 144& 44\\
			68& 40&  94& 40&  120& 42& 146& 43\\
			70& 40&  96& 42&  122& 41& 148& 44\\
			72& 40&  98& 39&  124& 42& 150& 44\\
			74& 41&  100& 41& 126& 41&    &   \\ 
			\hline
		\end{tabular}
	\end{center}
	\caption{Discrete 3-D Poisson: Refinement}
\end{table}
	\begin{figure}[H]
		\centering
		\includegraphics[width=\textwidth]{refine.png}
			\caption{Discrete Nodes vs Iterations}
	\end{figure}

The data shows that the number of iterations hovers around the same amount when additional nodes are introduced.

\subsection{Multilevel Comparison}
For this study I will be looking at additional levels affect the convergence rate.  
I will be using the 3-D Poisson problem with 20 nodes.
Agglomerate size will be set at 5 to allow for many levels.  
However, this will result in terrible operator complexity.
The spectral tolerance has been set low at .01.
There will be 2 smoothing steps and the interpolator will be smoothed by 1 degree.

\begin{table}[H]
	\begin{center}
		%\includegraphics[width=\textwidth]{levels.png}

		\begin{tabular}{|l|llllll|}
			\hline
			&Coarsest &Grid&Oper&Setup&Solve&Iterations\\
			Levels& Size &Comp& Comp  &Time &Time &Iterations\\
			\hline
			2&1599&1.200&2.250&7.478&1.604&11\\
			3&319&1.240&3.515&11.914&0.208&10\\
			4&63&1.248&3.589&13.266&0.255&10\\
			5&12&1.249&3.591&12.948&0.278&10\\
			6&2&1.249&3.591&13.377&0.395&10\\
			\hline
		\end{tabular}
	\end{center}
	\caption{20 Node 3-D Poisson Problem: Multilevel}
\end{table}

The data shows that the number of levels is a balance between setup time and solve time.  
When the coarsest grid is large, it is faster to setup but suffers when solving.
On the other hand, solving on a smaller coarse grid is faster but takes longer to setup.

\end{document}
