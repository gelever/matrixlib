\documentclass[a4paper]{article}
\usepackage{amssymb}
\usepackage{amsmath}
\usepackage{amsthm}
\usepackage{hyperref}
\usepackage[]{algorithm2e}
\usepackage{siunitx}
\usepackage[table,x11names]{xcolor}


\title{GMRES Project \\Applied Linear Algebra}
\author{Stephan Gelever\footnote{Email: gelever@pdx.edu}}

\begin{document}
\maketitle

\begin{abstract}
This project was tasked to create a set of tools for operating on sparse matrices in compressed sparse row format.
The library includes function for parsing and basic operations.  With these tools, I implemented the Generalized Minimal Residual (GMRES) algorithm.
This paper is an overview of the sparse matrix tool set, implementation of GMRES, and a study on how the maximum iteration value affects convergence.
\end{abstract}

\section{Introduction} 
This project is implemented in C.  Initially, I had chosen Python for it's ease of use.  However, after implementing a basic set of tools,
testing showed that my code was prohibitively slow.  I believe I was initializing objects too often and as a result the program ran slowly.  
Instead of trying to optimize the Python code, I rewrote everything in C.  This gave me control over how and when memory was allocated.

The code and results of the study can be found at my bitbucket 
repository\footnote{\hyperref[https://bitbucket.org/gelever/gmres-c]{https://bitbucket.org/gelever/gmres-c}}.

\section{Sparse Matrix Library} 
\subsection{Parsing}
This project required reading sparse matrix data from file.  The matrices used were stored in compressed sparse row (CSR) format.
My parser is able to read four different data file formats: entries, edge list, dense vectors, and Harwell Boeing. The entries format
consists of a list of all non-zero entries of the matrix.  The edge list format is a list of edges of a graph.  This is further converted
into the appropriate graph Laplacian.  The dense vector format is simply all the entries of a single vector.  The Harwell Boing format was
used as a means obtain large test data. 

\subsection{Data Structures}
There are two data structures that help package similar data.  The Matrix struct consists of three dynamic arrays that hold the spare matrix
information. Matrix entries are represented by double precision floats.  Column values and row indices are stored as integers. The number
of non-zeros, rows and columns are also stored.  A result struct is used to hold return data from GMRES.

\subsection{Operations}
This tool kit provides functions for several basic matrix operations.
\subsubsection{Sparse}
    \begin{itemize}
      \item \textit{Matrix * Matrix} - multiplies the non-zero elements of each row of the left hand side by non-zeros in 
        all columns of the right hand side. The result is kept in the provided result matrix. 
        The result is also stored directly in CSR format 
      \item \textit{Matrix * Vector} - multiplies the non-zero elements of each row of the matrix by the dense vector.
        The result is stored in the provided dense vector.
      \item \textit{Transpose} - transposes the given matrix.  The algorithm walks through each row to check if a non-zero
        exists for the current column.  An array is used to keep track of the last column position of each row in order to avoid walking
        through the sub-array each time.  This operation is not in place.  Instead, it stores the result
        in a new matrix.
      \item \textit{Symmetry Test} - checks is the matrix is symmetric. First, two random vectors \textbf{v} and \textbf{w},
        are generated.  Then the products \(\text{\textbf{v}}^T\text{A}\text{\textbf{w}}\) and 
        \(\text{\textbf{w}}^T\text{A}\text{\textbf{v}}\) are computed.  The operation return whether or not the two products are equal.
      \item \textit{Print Values} - prints the matrix in CSR format.
      \item \textit{Print Dense Version} - prints the values of the matrix, including zeros, in dense matrix representation.  Works 
        best on small matrices where each row can fit on the terminal screen. 
    \end{itemize}
\subsubsection{Dense}
    \begin{itemize}
      \item \textit{Matrix * Vector} - multiplies the dense matrix by a dense vector.  Computes the dot product of each row by the vector.
        The result is stored in a new dense vector.
      \item \textit{Matrix Transposed * Vector} - multiplies the transposed dense matrix by a dense vector.  For GMRES, some matrices
        are stored transposed for ease of access to the column vectors.  Instead of transposing to recover the original matrix, this operation
        multiplies with the transpose in mind.  The result is stored in a new dense vector.
      \item \textit{Solve Upper Triangular System} - solves the system of equations of an upper triangular matrix.  The algorithm
        walks through each row computes the value of each new \(x_i\). The input is actually the upper left hand corner sub-matrix of a larger
        matrix. Therefore, the size of both the sub-matrix and the matrix must be provided.
    \end{itemize}
\subsubsection{Vector}
    \begin{itemize}
      \item \textit{Scalar * Vector} - multiplies a scalar through a vector. This operation is not in place.  The result is stored in a 
        provided dense vector.
      \item \textit{Add two Vectors} - adds two vectors together.  Computes the entry-wise addition of the two vectors.  
      \item \textit{Subtract two Vectors} - subtracts two vectors together.  Computes the entry-wise subtraction of the two vectors.  
      \item \textit{Inner Product} - computes the inner product of two vectors.  The algorithm walks through both arrays and
        sums the products of each entry.
      \item \textit{Euclidean Norm} - computes the \(\ell_2\) norm of a vector.  This operation returns the square root of the inner product
        of the vector with itself.
    \end{itemize}


\section{Task One}
Task one of the project called for the creation of a test driver that performed several matrix operations.  First, a matrix \(A\) is read
in from file.  Several formats must be supported.  My driver supports selecting the appropriate format by using the cli input flag \({-}f\) 
and then specifying the format.

Next, the driver must calculate \(B=A^T\).  This is done by using the \(transpose\) function my library to store the transpose of 
\(A\) into \(B\). Then, the product \(C=AB\) is computed.  The library function \(sparse\_mult\_sparse\) takes care of this calculation.

Finally, it is necessary to check if \(C\) is symmetric.  The function \(is\_symmetric\) performs this task. The results of the symmetry 
test are displayed to the user.
\section{Task Two}
\subsection{GMRES Implementation}
The algorithm for GMRES with restart follows closely the algorithm provided in class. 
It uses an iterative approach to approximate solutions to a system of equations.
The Gram-Schmidt process has been inlined into the algorithm and saves the previous orthonormal vectors after each iteration as \(Q,S\).

Some programming difficulties were encountered when implementing GMRES.  An overflow error occurred an incorrect 
buffer was used in calculating \(\tilde{\text{\textbf{p}}}_m\). Careful debugging solved the bug.
Slow performance was fixed by switching from Python to C.  


Note: In the algorithm, the customary variable \(R\) in \(QR\) has been replaced by \(S\) to avoid confusion with 
the residual vector \(\text{\textbf{r}}\).
\newline

\subsection{GMRES Algorithm}
\begin{algorithm}[H]
  \KwData{\\\(A\) - Input Matrix\\ \(b\) - right hand side\\ \(x_0\) - initial guess\\ \(\epsilon\) - tolerance\\ \(m_{max}\) maximum iterations}
  \KwResult{\(x\) - approximate solution to the system}
  \While{Solution Not Found}{
    \(\text{\textbf{r}}_0 = \text{\textbf{b}} - A\text{\textbf{x}}_0\)\\
    \(\text{\textbf{p}}_1 = \frac{1}{\|\text{\textbf{r}}_0\|}\text{\textbf{r}}_0\)\\
    \(\text{\textbf{b}}_1 = A\text{\textbf{p}}_1\)\\
    \(\text{\textbf{t}}_1 = \frac{\text{\textbf{b}}_1^T\text{\textbf{r}}}{\text{\textbf{b}}_1^T\text{\textbf{b}}_1}\)\\
    \(\text{\textbf{x}}_1 = \text{\textbf{x}}_0 + t_1\text{\textbf{p}}_1\)\\
    \(\text{\textbf{r}}_1 = \text{\textbf{r}}_0 - t_1\text{\textbf{b}}_1\)\\
    \(m=1\)\\
    Calculate QR for Gram-Schmidt:\\
    \(\text{\textbf{q}}_1 = \frac{1}{\|\text{\textbf{b}}_1\|}\text{\textbf{b}}_1\)\\
    \(\text{\textbf{s}}_1 = \text{\textbf{b}}_1^T\text{\textbf{q}}_1\)\\
    \While{\(m<m_{\text{max}}\) and \(\|\text{\textbf{r}}_m\|>\epsilon\) }{
      \(\beta_i = \text{\textbf{p}}_i^T \text{\textbf{r}}_{m-1} \) for \(i=1,\dots,m-1\)\\
      \(\tilde{\text{\textbf{p}}}_m = \text{\textbf{r}}_{m-1} - \beta_1\text{\textbf{p}}_1 - \dots - \beta_{m-1}\text{\textbf{p}}_{m-1}\)\\
      \(\text{\textbf{p}}_m = \frac{1}{\|\tilde{\text{\textbf{p}}}_m\|}\tilde{\text{\textbf{p}}}_m\)\\
      \(\text{\textbf{b}} _m = A\text{\textbf{p}}_m\) \\
      Form \(P=[ \text{\textbf{p}}_1,\text{\textbf{p}}_2,\dots, \text{\textbf{p}}_m] \) \\
      Form \(B=[ \text{\textbf{b}}_1,\text{\textbf{b}}_2,\dots, \text{\textbf{b}}_m] \) \\
      Next iteration of Gram-Schmidt:\\
      \(\text{\textbf{s}}_{m,1:m-1} = \text{\textbf{q}}_i^T\text{\textbf{b}}_m\) for \(i=1,\dots,m-1\)\\
      \(\tilde{\text{\textbf{q}}}_m = \text{\textbf{b}}_m - \text{\textbf{s}}_{m,1}\text{\textbf{q}}_1 - \dots 
      - \text{\textbf{s}}_{m,m-1}\text{\textbf{q}}_{m-1} \)\\
      \(\text{\textbf{q}}_m = \frac{1}{\|\tilde{\text{\textbf{q}}}_m\|}\tilde{\text{\textbf{q}}}_m\)\\
      \(\text{\textbf{s}}_{m,m} = \text{\textbf{q}}_m^T\text{\textbf{b}}_m\)\\
      Form \(Q=[ \text{\textbf{q}}_1,\text{\textbf{q}}_2,\dots, \text{\textbf{q}}_m] \) \\
      Form \(S=[ \text{\textbf{s}}_1,\text{\textbf{s}}_2,\dots, \text{\textbf{s}}_m] \) \\
      Solve \(S\text{\textbf{t}} = Q^T\text{\textbf{r}}_{m-1}\)\\
      \(\text{\textbf{x}}_m = \text{\textbf{x}}_{m-1} + P\text{\textbf{t}}\)\\
      \(\text{\textbf{r}}_m = \text{\textbf{r}}_{m-1} - B\text{\textbf{t}}\)\\
    }
    \If{  \(\|r\| < \epsilon\)}{
      Solution Found\\
    }
  }
  \caption{Implemented GMRES with Restart}
\end{algorithm}
\subsection{Study on \(m_{max}\)}
Task two called for a study on the effects of varying \(m_{max} = 5,10,15,\dots\). I wrote a small bash script \(runTaskTwo.sh\) that called
my GMRES program with different \(m_{max}\) values.  \(m_{max}\) was varied from 5 to 150 by increments of 5 to study 
the number of  iterations and time to converge.
A zero vector was used as an initial guess.  The tolerance was set to \num{1e-6}.  
A random right hand side orthogonal to the constant 1 vector was generated for each edge list.
The times and iterations for each \(m\) were averaged over 5 runs.

\subsubsection{Edge Lists}
Several edge lists were provided for testing:
\begin{center}
\begin{tabular}{|c|cc|}
  \hline
Edge List& Edges & Nodes\\
  \hline
  amazon\_h\_0.edgelist&4842&863\\
  amazon\_h\_1.edgelist&3820&665\\
  amazon\_m\_0.edgelist&2954&670\\
  amazon\_m\_1.edgelist&4882&862\\
  amazon\_m\_2.edgelist&3624&765\\
  \hline
\end{tabular}
\end{center}



\subsubsection{Results}
\noindent
\begin{center}
%\begin{minipage}{\textwidth}
\begin{minipage}{0,5\textwidth}
  \center
\begin{tabular}{|l|ll|}
  \hline
  \(m_{max}\)&Seconds&Iterations\\
  \hline
  5& 0.855& 20939\\
  10&0.548&10076\\
  15&0.452&6529\\
  20&0.420&4913\\
  25&0.398&3867\\
  30&0.388&3167\\
  35&0.394&2793\\
  40&0.385&2408\\
  45&0.378&2133\\
  50&0.388&1986\\
  55&0.379&1782\\
  60&0.345&1488\\
  \rowcolor{lightgray}65&0.338&1352\\
  70&0.365&1335\\
  75&0.389&1329\\
  80&0.442&1350\\
  85&0.450&1298\\
  90&0.446&1168\\
  95&0.407&1008\\
  100&0.369&857\\
  105&0.374&809\\
  110&0.397&802\\
  115&0.382&729\\
  120&0.422&726\\
  125&0.432&729\\
  130&0.443&723\\
  135&0.500&771\\
  140&0.579&819\\
  145&0.601&830\\
  150&0.617&815\\
  \hline
\end{tabular}
\medskip

amazon\_h\_0 Results
\end{minipage}
\noindent
\begin{minipage}{0,3\textwidth}
  \center
\begin{tabular}{|l|ll|}
  \hline
  \(m_{max}\)&Seconds&Iterations\\
  \hline
 5 &0.585&18604\\
 10&0.382&9174 \\
 15&0.309&5829 \\
 20&0.282&4323 \\
 25&0.258&3288 \\
 30&0.251&2692 \\
 35&0.260&2421 \\
 40&0.248&2040 \\
 \rowcolor{lightgray}45&0.229&1703 \\
 50&0.246&1660 \\
 55&0.246&1528 \\
 60&0.244&1392 \\
 65&0.248&1322 \\
 70&0.253&1251 \\
 75&0.267&1218 \\
 80&0.266&1142 \\
 85&0.266&1067 \\
 90&0.249&937  \\
 95&0.271&944  \\
 100& 0.261&855\\
105 & 0.285&886\\
110 & 0.312&888\\
115 & 0.323&875\\
120 & 0.333&834\\
125 & 0.334&820\\
130 & 0.356&801\\
135 & 0.386&820\\
140 & 0.409&837\\
145 & 0.425&840\\
150 & 0.437&833\\
  \hline
\end{tabular}
\medskip

amazon\_h\_1 Results
\end{minipage}

\noindent
\begin{minipage}{0,5\textwidth}
  \center
\begin{tabular}{|l|ll|}
  \hline
  \(m_{max}\)&Seconds&Iterations\\
  \hline
  5&0.896&29230 \\
 10&0.597&14653 \\
 15&0.393&7505  \\
 20&0.359&5529  \\
 25&0.315&4034  \\
 30&0.302&3244  \\
 35&0.292&2728  \\
 40&0.280&2310  \\
 45&0.276&2054  \\
 50&0.275&1851  \\
 55&0.271&1680  \\
 60&0.261&1480  \\
 65&0.250&1324  \\
 70&0.266&1304  \\
 75&0.270&1237  \\
 80&0.282&1207  \\
 85&0.293&1175  \\
 90&0.300&1132  \\
 95&0.306&1079  \\
100&0.284&947   \\
105&0.274&831   \\
\rowcolor{lightgray}110&0.244&730   \\
115&0.270&751   \\
120&0.282&751   \\
125&0.309&777   \\
130&0.324&761   \\
135&0.330&753   \\
140&0.358&774   \\
145&0.374&746   \\
150&0.406&783   \\
  \hline
\end{tabular}
\medskip

amazon\_m\_0 Results
\end{minipage}
\begin{minipage}{0,3\textwidth}
  \center
\begin{tabular}{|l|ll|}
  \hline
  \(m_{max}\)&Seconds&Iterations\\
  \hline
 5 &2.938&71684 \\
 10&1.620&29969 \\
 15&1.140&16557 \\
 20&0.915&10832 \\
 25&0.532&5217  \\
 30&0.663&5461  \\
 35&0.666&4756  \\
 40&0.639&4040  \\
 45&0.590&3358  \\
 50&0.596&3064  \\
 55&0.551&2612  \\
 60&0.531&2332  \\
 65&0.529&2090  \\
 70&0.536&1941  \\
 75&0.539&1820  \\
 80&0.482&1500  \\
 85&0.452&1340  \\
 \rowcolor{lightgray}90&0.421&1156  \\
 95&0.452&1134  \\
100&0.453&1063  \\
105&0.455&987   \\
110&0.439&941   \\
115&0.492&987   \\
120&0.563&1041  \\
125&0.592&1035  \\
130&0.604&1015  \\
135&0.596&936   \\
140&0.599&921   \\
145&0.639&935   \\
150&0.678&944   \\
  \hline
\end{tabular}
\medskip

amazon\_m\_1 Results
\end{minipage}
\noindent
\begin{minipage}{0,5\textwidth}
  \center
\begin{tabular}{|l|ll|}
  \hline
  \(m_{max}\)&Seconds&Iterations\\
  \hline
  5&1.830&52045 \\
 10&0.818&17435 \\
 15&0.539&8973  \\
 20&0.431&5808  \\
 25&0.508&5658  \\
 30&0.523&4893  \\
 35&0.500&4052  \\
 40&0.439&3165  \\
 45&0.426&2740  \\
 50&0.426&2488  \\
 55&0.435&2343  \\
 60&0.437&2153  \\
 65&0.397&1816  \\
\rowcolor{lightgray} 70&0.349&1506  \\
 75&0.376&1491  \\
 80&0.399&1479  \\
 85&0.409&1392  \\
 90&0.402&1272  \\
 95&0.408&1207  \\
100&0.415&1117  \\
105&0.396&1016  \\
110&0.384&927   \\
115&0.422&935   \\
120&0.418&915   \\
125&0.406&830   \\
130&0.458&876   \\
135&0.490&902   \\
140&0.533&920   \\
145&0.571&939   \\
150&0.601&947   \\
  \hline
\end{tabular}
\medskip

amazon\_m\_2 Results
\end{minipage}
\end{center}

\subsubsection{Result Analysis}
Results from the study range from 45 to 110 as the optimum number for max iterations. However, there are several local minima per result.
For almost all results, a local minima exists at either \(m_{max}= 30\) or \(m_{max}= 60\).
In practice, it may be beneficial to select a smaller \(m_{max}\) that has similar timing.  With a large enough matrix, 
there may not be enough memory for a large \(m_{max}\). In addition, the right hand side vectors were randomly generated to create a 
vector that summed to one.  This may not be a realistic application.

The number of iterations tends to decrease as \(m_{max}\) increases.  However, this is not always strictly the case.  After \(m_{max}\) starts
to get large, the number of iterations fluctuates.  If \(m_{max}\) is set high enough, the algorithm will provide a solution without restart.
With the edge lists in the study, this happened with \(m_{max}\) between 300 and 400.
\end{document}
